// ==UserScript==
// @name         StaffTools
// @namespace    https://violentmonkey.github.io/
// @version      0.10.3.5
// @updateURL    https://git.raptorsne.st/Raptor/staffscript/raw/branch/master/stafftools.user.js
// @description  does cool stuff
// @author       BTN
// @contributor  ChillMeister
// @contributor  Kaccha
// @contributor  Raptor
// @contributor  Ryko
// @match        https://broadcasthe.net/*
// @require      https://greasemonkey.github.io/gm4-polyfill/gm4-polyfill.js
// @require      https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/dialog-polyfill/0.4.3/dialog-polyfill.min.js
// @require      https://cdn.rawgit.com/alertifyjs/alertify.js/v1.0.11/dist/js/alertify.js
// @resource     userscript_styles https://git.raptorsne.st/Raptor/staffscript/raw/branch/master/css/styles.css
// @resource     bootstrap_iso https://git.raptorsne.st/Raptor/staffscript/raw/branch/master/css/bootstrap-iso.css
// @resource     dialog_polyfill https://cdnjs.cloudflare.com/ajax/libs/dialog-polyfill/0.4.3/dialog-polyfill.min.css
// @resource     options_dialog https://git.raptorsne.st/Raptor/staffscript/raw/branch/master/html/options.html
// @icon         https://broadcasthe.net/favicon.ico
// @grant        GM_info
// @grant        GM_setValue
// @grant        GM_getValue
// @grant        GM_deleteValue
// @grant        GM_xmlhttpRequest
// @grant        GM_addStyle
// @grant        GM_getResourceText
// @grant        unsafeWindow
// @connect      api.thetvdb.com
// @connect      api.tvmaze.com
// @connect      broadcasthe.net
// @connect		 git.raptorsne.st
// @connect      localhost
// @run-at       document-end
// ==/UserScript==

const exportFunction = a => a;
'use strict';
(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';
class MediaInfo {
    constructor(log) {
        var regex_section = /^((?:general|video|audio|text|menu)\s*(?:\#(\d+)?)*)$/i;
        var lines = log.split(/\r\n|\n|\r/);
        var header = undefined;
        for(var i = 0; i < lines.length; i++) {
            console.log(lines[i]);
            if(lines[i].length <= 1) {
                header = undefined;
                continue;
            }
            lines[i] = lines[i].trim();
            var match = lines[i].match(regex_section);
            if(match) {
                header = match[1];
                this[match[1]] = {};
            } else if(header) {
                var sp = lines[i].replace(/\s*:\s*/, '\x01').split('\x01');
                sp[0] = (sp[0] || '').trim();
                sp[1] = (sp[1] || '').trim();
                this[header][sp[0]] = sp[1];
            }
        }
        console.log(this);

        if(!this['General']) throw 'Invalid/no MediaInfo.';
    }

    static getAllMediaInfos(log) {
        var logs = log.split(/General[^\S\n]*(?:\#\d+?)*\r?\n/g).slice(1);
        var mediaInfos = [];
        for(var i = 0; i < logs.length; i++) {
            mediaInfos.push(new MediaInfo('General\n'+logs[i]));
        }
        if(mediaInfos.length == 0) {
            throw 'No valid MediaInfos found.';
        }
        console.log(mediaInfos);
        return mediaInfos;
    }

    getFilename() {
        if(!this['General']['Complete name']) throw 'No filename provided in MediaInfo.';
        return this['General']['Complete name'].match(/(?:.*\/)?(.*)/)[1];
    }

    getContainer() {
        if(!this['General']['Complete name']) throw 'No filename provided in MediaInfo.';
        var container = this['General']['Complete name'].match(/(?:.*\/)?.*\.(.*)/)[1].toUpperCase();
        if(container == 'IFO') return 'VOB';
        return container;
    }

    getVideoCodec() {
        if(!this['Video']) throw 'No video section in MediaInfo.';
        var video = this['Video'];
        var codec = video['Format'];
        var writingLibrary = video['Writing library'];
        var writingApplication = this['General']['Writing application'];
        var bitDepth = video['Bit depth'];
        var formatVersion = video['Format version'];
        var codecId = video['Codec ID'];
        var codecIdHint = video['Codec ID/Hint'];
        if(codec == 'AVC') {
            if(writingLibrary) {
                if(writingLibrary.indexOf('x264') > -1) return (bitDepth && bitDepth == '10 bits' ? 'x264-Hi10P' : 'x264');
            }
            return 'H.264';
        } else if (codec == 'HEVC' || codec == 'hvc1' || codec == 'hev1') {
            return (writingLibrary && writingLibrary.indexOf('x265') > -1 ? 'x265' : 'H.265');
        } else if (codec == 'MPEG-4 Visual') {
            if(writingLibrary) {
                if(/XviD/i.test(writingLibrary)) return 'Xvid';
                else if(/DivX/i.test(writingLibrary)) return 'DivX';
            }
            if(codecId) {
                if(/XviD/i.test(codecId)) return 'Xvid';
                else if(/DivX|DX50/i.test(codecId)) return 'DivX';
            }
            if(codecIdHint) {
                if(/XviD/i.test(codecIdHint)) return 'Xvid';
                else if(/DivX/i.test(codecIdHint)) return 'DivX';
            }
        } else if (codec == 'MPEG Video') {
            if(formatVersion) {
                if(formatVersion == 'Version 1') return 'MPEG';
                else if(formatVersion == 'Version 2') return 'MPEG2';
            }
        } else if (codec == 'VC-1') {
            return 'VC-1';
        } else if (codec == 'VP9') {
            return 'VP9'
        }
        throw 'Unknown/unsupported video codec.';
    }

    getVideoResolution() {
        if(!this['Video']) throw 'No video section in MediaInfo.';
        var video = this['Video'];
        var height = video['Height'];
        height = parseInt(height.split(' ').join('').match(/([0-9]+pixels)/)[1]);
        var scanType = this.isInterlaced();
        if(height > 1080) {
            return '2160p';
        } else if(height > 720) {
            return (scanType == 'Interlaced' ? '1080i' : '1080p');
        } else if(height > 576) {
            return '720p';
        } else if(height == 576) {
            return (scanType == 'Interlaced' ? '576i' : '576p');
        } else {
            return 'SD';
        }
    }

    isInterlaced() {
        if(!this['Video']) throw 'No video section in MediaInfo.';
        var video = this['Video'];
        var scanType = video['Scan type'];
        var storeMethod = video['Scan type, store method'];
        if (scanType == 'Interlaced' || scanType == 'MBAFF' || storeMethod == 'Interleaved fields') return 'Interlaced';
        return 'Progressive';
    }

    getVideoStandard() {
        if(!this['Video']) throw 'No video section in MediaInfo.';
        return this['Video']['Standard'];
    }

    getNumberOfAudioTracks() {
        var keys = Object.keys(this);
        var num = 0;
        for(var i = 0; i < keys.length; i++) {
            if(keys[i].indexOf('Audio') > -1) num++;
        }
        return num;
    }

    getPrimaryAudioCodec() {
        if(!this['Audio'] && !this['Audio #1']) throw 'No audio section in MediaInfo.';
        var audio = this['Audio'] || this['Audio #1'];
        var codec = audio['Format'];
        var profile = audio['Format profile'];
        var channels = (audio['Channel(s)_Original'] || audio['Channel(s)'] || audio['Channel count']).match(/([0-9]) channels?/);
        if(channels) channels = parseInt(channels[1]);
        else channels = 0;

        if(codec == 'MPEG Audio') {
            if(profile && profile == 'Layer 3') return 'MP3';
            else if(profile && profile == 'Layer 2') return 'MP2';
        }

        var channelMap = {
            1: '1.0',
            2: '2.0',
            3: '2.1',
            6: '5.1',
            8: '7.1'
        };

        if(channelMap[channels]) {
            if(codec.indexOf('TrueHD') > -1) {
                return 'TrueHD' + channelMap[channels];
            } else if(codec == 'PCM') {
                return 'LPCM' + channelMap[channels];
            } else if(codec == 'FLAC') {
                return 'FLAC' + channelMap[channels];
            } else if(codec == 'DTS') {
                if(profile && profile == 'MA / Core') return 'DTS-HD.MA' + channelMap[channels];
                else return 'DTS' + channelMap[channels];
            } else if(codec == 'E-AC-3') {
                return 'DDP' + channelMap[channels];
            } else if(codec == 'AC-3') {
                return 'DD' + channelMap[channels];
            } else if(codec == 'AAC') {
                return 'AAC' + channelMap[channels];
            } else if(codec == 'Opus') {
                return 'Opus' + channelMap[channels];
            }
        }
        throw 'Unknown/unsupported audio codec.';
    }

    getNumberOfSubTracks() {
        var keys = Object.keys(this);
        var num = 0;
        for(var i = 0; i < keys.length; i++) {
            if(keys[i].indexOf('Text') > -1) num++;
        }
        return num;
    }
}

module.exports = MediaInfo;

},{}],2:[function(require,module,exports){
'use strict';
class Preferences {
    constructor() {
        if (GM_setValue !== undefined && GM_getValue !== undefined && GM_deleteValue !== undefined) {
            this.useGM = true;
            return;
        }
        // check if local storage supported
        try {
            this.useLocalStorage = (localStorage !== 'undefined');
        } catch (e) {
            // error accessing local storage (user may have blocked access)
            console.error(e);
            this.useLocalStorage = false;
        }
    }

    setItem(key, value) {
        if (this.useGM) {
            GM_setValue(key, String(value));
        } else if (this.useLocalStorage) {
            try {
                localStorage.setItem(key, value);
            } catch (e) {
                // error modifying local storage (out of space?)
                console.error(e);
            }
        }
    }

    getItem(key, defaultValue) {
        if (this.useGM) {
            return GM_getValue(key, defaultValue);
        } else if (this.useLocalStorage) {
            try {
                var result = localStorage.getItem(key);
                return (result !== null ? result : defaultValue);
            } catch (e) {
                // error reading from local storage
                console.error(e);
                return defaultValue;
            }
        } else {
            return defaultValue;
        }
    }

    removeItem(key) {
        if (this.useGM) {
            GM_deleteValue(key);
        } else if (this.useLocalStorage) {
            try {
                localStorage.removeItem(key);
            } catch (e) {
                // error modifying local storage
                console.error(e);
            }
        }
    }

    clear() {
        if (this.useGM) {
            // todo
        }
        if (this.useLocalStorage) {
            try {
                localStorage.clear();
            } catch (e) {
                // error modifying local storage
                console.error(e);
            }
        }
    }
}

class NamedPreferences {
    constructor(namespace, preferenceManager) {
        this.manager = preferenceManager;
        this.prefix = 'nerfed-preferences/' + namespace;
    }

    setItem(key, value) {
        this.manager.setItem(this.prefix + '/' + key, value);
    }

    setItems(obj) {
        var that = this;
        Object.keys(obj).forEach(function (key) {
            that.manager.setItem(that.prefix + '/' + key, obj[key]);
        });
    }

    getItem(key, defaultValue) {
        return this.manager.getItem(this.prefix + '/' + key, defaultValue);
    }

    removeItem(key) {
        this.manager.removeItem(this.prefix + '/' + key);
    }
}

var instance = new Preferences();
var namedInstances = [];
module.exports.getInstance = function (namespace) {
    if (!namedInstances[namespace]) {
        namedInstances[namespace] = new NamedPreferences(namespace, instance);
    }
    return namedInstances[namespace];
};

},{}],3:[function(require,module,exports){
'use strict';
var Promise = require('bluebird');

function GM_XHR() {
    this.type = null;
    this.url = null;
    this.async = null;
    this.username = null;
    this.password = null;
    this.status = null;
    this.headers = {};
    this.readyState = null;

    this.abort = function () {
        this.readyState = 0;
    };

    this.getAllResponseHeaders = function (name) {
        if (this.readyState != 4) {
            return "";
        }
        return this.responseHeaders;
    };

    this.getResponseHeader = function (name) {
        var regexp = new RegExp('^' + name + ': (.*)$', 'im');
        var match = regexp.exec(this.responseHeaders);
        if (match) {
            return match[1];
        }
        return '';
    };

    this.open = function (type, url, async, username, password) {
        this.type = type ? type : null;
        this.url = url ? url : null;
        this.async = async ? async : null;
        this.username = username ? username : null;
        this.password = password ? password : null;
        this.readyState = 1;
    };

    this.setRequestHeader = function (name, value) {
        this.headers[name] = value;
    };

    this.send = function (data) {
        this.data = data;
        var that = this;
        // http://wiki.greasespot.net/GM_xmlhttpRequest
        GM_xmlhttpRequest({
            method:  this.type,
            url:     this.url,
            headers: this.headers,
            data:    this.data,
            onload:  function (rsp) {
                // Populate wrapper object with returned data
                // including the Greasemonkey specific "responseHeaders"
                for (var k in rsp) {
                    that[k] = rsp[k];
                }
                // now we call onreadystatechange
                if (that.onload) {
                    that.onload();
                } else {
                    that.onreadystatechange();
                }
            },
            onerror: function (rsp) {
                for (var k in rsp) {
                    that[k] = rsp[k];
                }
                if (that.onerror) {
                    that.onerror();
                } else {
                    that.onreadystatechange();
                }
            }
        });
    };
}

class TVDBv2 {

    constructor() {
        this.token = '';
        this.languages = {};
    }

    refreshToken() {
        var that = this;
        return Promise.resolve($.ajax({
            url:        'https://api.thetvdb.com/refresh_token',
            xhr:        function () {
                return new GM_XHR();
            },
            type:       'GET',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', 'Bearer ' + that.token);
            }
        })).then(function (data) {
            that.token = data.token;
            return data.token;
        });
    }

    login(apikey, username, userkey) {
        var that = this;
        return Promise.resolve($.ajax({
            url:        'https://api.thetvdb.com/login',
            xhr:        function () {
                return new GM_XHR();
            },
            type:       'POST',
            data:       JSON.stringify({
                apikey:   apikey,
                username: username,
                userkey:  userkey
            }),
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Content-Type', 'application/json');
                xhr.setRequestHeader('Accept', 'application/json');
            }
        })).then(function (data) {
            that.token = data.token;
            console.log('JWT token: ' + that.token);
            return true;
        }).then(function () {
            return Promise.resolve($.ajax({
                url:        'https://api.thetvdb.com/languages',
                xhr:        function () {
                    return new GM_XHR();
                },
                type:       'GET',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'Bearer ' + that.token);
                }
            })).then(function (data) {
                that.languages = {};
                data = data.data;
                for(var i = 0; i < data.length; i++) {
                    that.languages[data[i]['id']] = data[i]['abbreviation'];
                }
                return true;
            });
        });
    }

    getSeries(seriesId, languageId) {
        var that = this;
        function f(languageId) {
            return Promise.resolve($.ajax({
                url:        'https://api.thetvdb.com/series/' + seriesId,
                xhr:        function () {
                    return new GM_XHR();
                },
                type:       'GET',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'Bearer ' + that.token);
                    if(languageId) {
                        xhr.setRequestHeader('Accept-Language', that.languages[languageId]);
                    }
                }
            }));
        }
        var englishLanguage = f(7);
        if(languageId && languageId != 7) {
            return Promise.all([englishLanguage, f(languageId)]).then(function(data) {
                var english = data[0].data;
                var otherLang = data[1].data;
                var keys = Object.keys(otherLang);
                for (var i = 0; i < keys.length; i++) {
                    var key = keys[i];
                    if (otherLang[key] && !english[key]) {
                        english[key] = otherLang[key];
                    }
                }
                if(!english['seriesName']) {
                    throw 'Needs new language ID';
                }
                return english;
            })
        } else {
            return englishLanguage.then(function (data) {
                if(!data.data['seriesName']) {
                    throw 'Needs new language ID';
                }
                return data.data;
            });
        }
    }

    getSeriesEpisode(seriesId, season, episode, languageId) {
        var that = this;
        function f(languageId) {
            return Promise.resolve($.ajax({
                url:        'https://api.thetvdb.com/series/' + seriesId + '/episodes/query?airedSeason=' + season + '&airedEpisode=' + episode,
                xhr:        function () {
                    return new GM_XHR();
                },
                type:       'GET',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'Bearer ' + that.token);
                    if(languageId) {
                        xhr.setRequestHeader('Accept-Language', that.languages[languageId]);
                    }
                }
            })).then(function (data) {
                return that.getEpisodeById(data.data[0].id);
            });
        }

        return f(7).catch(function (err) {
            if(languageId) {
                return f(languageId);
            } else {
                throw err;
            }
        });
    }

    getSeriesSeason(seriesId, season, languageId) {
        var that = this;
        function f(languageId) {
            return Promise.resolve($.ajax({
                url:        'https://api.thetvdb.com/series/' + seriesId + '/episodes/query?airedSeason=' + season,
                xhr:        function () {
                    return new GM_XHR();
                },
                type:       'GET',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'Bearer ' + that.token);
                    if(languageId) {
                        xhr.setRequestHeader('Accept-Language', that.languages[languageId]);
                    }
                }
            })).then(function (data) {
                if (data.data.length > 100) {
                    throw 'Use site autofill for season packs with >100 episodes.'
                }
                var episodes = data.data.map(function (d) {
                    return that.getEpisodeById(d.id);
                });
                return Promise.all(episodes);
            });
        }

        return f(7).catch(function (err) {
            if(languageId) {
                return f(languageId);
            } else {
                throw err;
            }
        });
    }

    getSeriesImages(seriesId, keyType, resolution, subKey, languageId) {
        var that = this;
        var url = 'https://api.thetvdb.com/series/' + seriesId + '/images/query?keyType=' + keyType;
        if (resolution) {
            url += '&resolution=' + resolution;
        }
        if (subKey) {
            url += '&subKey=' + subKey;
        }

        function f(languageId) {
            return Promise.resolve($.ajax({
                url:        url,
                xhr:        function () {
                    return new GM_XHR();
                },
                type:       'GET',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'Bearer ' + that.token);
                    if(languageId) {
                        xhr.setRequestHeader('Accept-Language', that.languages[languageId]);
                    }
                }
            })).then(function (data) {
                return data.data;
            });
        }

        return f(7).catch(function (err) {
            if(languageId) {
                return f(languageId);
            } else {
                throw err;
            }
        });
    }

    getSeriesPoster(seriesId, defaultValue, languageId) {
        var that = this;
        return that.getSeriesImages(seriesId, 'poster', undefined, undefined, languageId)
            .then(function (data) {
                return 'http://thetvdb.com/banners/' + data[0].fileName;
            }).catch(function (err) {
                console.log(err);
                return defaultValue;
            });
    }

    getSeasonPoster(seriesId, season, defaultValue, languageId) {
        var that = this;
        return that.getSeriesImages(seriesId, 'season', undefined, season, languageId)
            .then(function (data) {
                return 'http://thetvdb.com/banners/' + data[0].fileName;
            }).catch(function (err) {
                console.log(err);
                return that.getSeriesPoster(seriesId, defaultValue);
            });
    }

    getEpisodeById(episodeId, languageId) {
        var that = this;
        function f(languageId) {
            return Promise.resolve($.ajax({
                url:        'https://api.thetvdb.com/episodes/' + episodeId,
                xhr:        function () {
                    return new GM_XHR();
                },
                type:       'GET',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'Bearer ' + that.token);
                    if(languageId) {
                        xhr.setRequestHeader('Accept-Language', that.languages[languageId]);
                    }
                }
            })).then(function (data) {
                return data.data;
            });
        }

        return f(7).catch(function (err) {
            if(languageId) {
                return f(languageId);
            } else {
                throw err;
            }
        });
    }

    getSeriesSummaryById(seriesId, languageId) {
        var that = this;
        function f(languageId) {
            return Promise.resolve($.ajax({
                url:        'https://api.thetvdb.com/series/' + seriesId + '/episodes/summary',
                xhr:        function () {
                    return new GM_XHR();
                },
                type:       'GET',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'Bearer ' + that.token);
                    if(languageId) {
                        xhr.setRequestHeader('Accept-Language', that.languages[languageId]);
                    }
                }
            })).then(function (data) {
                return data.data;
            });
        }

        return f(7).catch(function (err) {
            if(languageId) {
                return f(languageId);
            } else {
                throw err;
            }
        });
    }
}

module.exports = TVDBv2;

},{"bluebird":7}],4:[function(require,module,exports){
'use strict';
var Promise = require('bluebird');

function GM_XHR() {
    this.type = null;
    this.url = null;
    this.async = null;
    this.username = null;
    this.password = null;
    this.status = null;
    this.headers = {};
    this.readyState = null;

    this.abort = function () {
        this.readyState = 0;
    };

    this.getAllResponseHeaders = function (name) {
        if (this.readyState != 4) {
            return "";
        }
        return this.responseHeaders;
    };

    this.getResponseHeader = function (name) {
        var regexp = new RegExp('^' + name + ': (.*)$', 'im');
        var match = regexp.exec(this.responseHeaders);
        if (match) {
            return match[1];
        }
        return '';
    };

    this.open = function (type, url, async, username, password) {
        this.type = type ? type : null;
        this.url = url ? url : null;
        this.async = async ? async : null;
        this.username = username ? username : null;
        this.password = password ? password : null;
        this.readyState = 1;
    };

    this.setRequestHeader = function (name, value) {
        this.headers[name] = value;
    };

    this.send = function (data) {
        this.data = data;
        var that = this;
        // http://wiki.greasespot.net/GM_xmlhttpRequest
        GM_xmlhttpRequest({
            method:  this.type,
            url:     this.url,
            headers: this.headers,
            data:    this.data,
            onload:  function (rsp) {
                // Populate wrapper object with returned data
                // including the Greasemonkey specific "responseHeaders"
                for (var k in rsp) {
                    that[k] = rsp[k];
                }
                // now we call onreadystatechange
                if (that.onload) {
                    that.onload();
                } else {
                    that.onreadystatechange();
                }
            },
            onerror: function (rsp) {
                for (var k in rsp) {
                    that[k] = rsp[k];
                }
                if (that.onerror) {
                    that.onerror();
                } else {
                    that.onreadystatechange();
                }
            }
        });
    };
}

class TVMaze {

    getSeriesByTVDBId(tvdbSeriesId) {
        return Promise.resolve($.ajax({
            url:  'http://api.tvmaze.com/lookup/shows?thetvdb=' + tvdbSeriesId,
            xhr:  function () {
                return new GM_XHR();
            },
            type: 'GET'
        })).then(function (data) {
            return data;
        });
    }

    getSeriesByIMDBId(imdbSeriesId) {
        return Promise.resolve($.ajax({
            url:  'http://api.tvmaze.com/lookup/shows?imdb=' + imdbSeriesId,
            xhr:  function () {
                return new GM_XHR();
            },
            type: 'GET'
        })).then(function (data) {
            return data;
        });
    }

    getSeriesByTVRageId(tvrageSeriesId) {
        return Promise.resolve($.ajax({
            url:  'http://api.tvmaze.com/lookup/shows?tvrage=' + tvrageSeriesId,
            xhr:  function () {
                return new GM_XHR();
            },
            type: 'GET'
        })).then(function (data) {
            return data;
        });
    }

    getSeries(seriesId) {
        return Promise.resolve($.ajax({
            url:  'http://api.tvmaze.com/shows/' + seriesId,
            xhr:  function () {
                return new GM_XHR();
            },
            type: 'GET'
        })).then(function (data) {
            return data;
        });
    }

    getSeasons(seriesId) {
        return Promise.resolve($.ajax({
            url:  'http://api.tvmaze.com/shows/' + seriesId + '/seasons',
            xhr:  function () {
                return new GM_XHR();
            },
            type: 'GET'
        })).then(function (data) {
            return data;
        });
    }

    getEpisode(seriesId, season, episode) {
        return Promise.resolve($.ajax({
            url:  'http://api.tvmaze.com/shows/' + seriesId + '/episodebynumber?season=' + season + '&number=' + episode,
            xhr:  function () {
                return new GM_XHR();
            },
            type: 'GET'
        })).then(function (data) {
            return data;
        });
    }

    getEpisodes(seriesId, specials) {
        return Promise.resolve($.ajax({
            url:  'http://api.tvmaze.com/shows/' + seriesId + '/episodes?specials=' + (!!specials ? 1 : 0),
            xhr:  function () {
                return new GM_XHR();
            },
            type: 'GET'
        })).then(function (data) {
            return data;
        });
    }

    getEpisodesWithSpecials(seriesId) {
        return getEpisodes(seriesId, true);
    }
}

module.exports = TVMaze;

},{"bluebird":7}],5:[function(require,module,exports){
'use strict';
class Utils {
    static getLongestCommonSubsequence(a, b) {
        var m = a.length, n = b.length,
            C = [], i, j;
        for (i = 0; i <= m; i++) C.push([0]);
        for (j = 0; j < n; j++) C[0].push(0);
        for (i = 0; i < m; i++)
            for (j = 0; j < n; j++)
                C[i+1][j+1] = a[i] === b[j] ? C[i][j]+1 : Math.max(C[i+1][j], C[i][j+1]);
        return (function bt(i, j) {
            if (i*j === 0) { return ""; }
            if (a[i-1] === b[j-1]) { return bt(i-1, j-1) + a[i-1]; }
            return (C[i][j-1] > C[i-1][j]) ? bt(i, j-1) : bt(i-1, j);
        }(m, n));
    }

    static getHTMLDiff(original, modified) {
        var lcs = this.getLongestCommonSubsequence(original, modified);
        var originalOutput = '';
        var modifiedOutput = '';
        var i = 0;
        var j = 0;
        var k = 0;
        var addedBuffer = '';
        var deletedBuffer = '';
        while(i < lcs.length) {
            addedBuffer = '';
            deletedBuffer = '';
            var commonChar = lcs.charAt(i++);
            while(k < modified.length) {
                var modifiedChar = modified.charAt(k++);
                if(modifiedChar == commonChar) {
                    break;
                }
                addedBuffer += modifiedChar;
            }
            if(addedBuffer.length > 0) {
                modifiedOutput += '<div class="str-added">' + addedBuffer + '</div>';
            }
            while(j < original.length) {
                var originalChar = original.charAt(j++);
                if(originalChar == commonChar) {
                    break;
                }
                deletedBuffer += originalChar;
            }
            if(deletedBuffer.length > 0) {
                originalOutput += '<div class="str-removed">' + deletedBuffer + '</div>';
            }
            originalOutput += commonChar;
            modifiedOutput += commonChar;
        }
        addedBuffer = '';
        deletedBuffer = '';
        while(k < modified.length) {
            addedBuffer += modified.charAt(k++);
        }
        if(addedBuffer.length > 0) {
            modifiedOutput += '<div class="str-added">' + addedBuffer + '</div>';
        }
        while(j < original.length) {
            deletedBuffer += original.charAt(j++);
        }
        if(deletedBuffer.length > 0) {
            originalOutput += '<div class="str-removed">' + deletedBuffer + '</div>';
        }
        return [originalOutput, modifiedOutput];
    }

    static escapeHTML(str) {
        var tagsToReplace = {
            '&': '&amp;',
            '<': '&lt;',
            '>': '&gt;'
        };
        return str.replace(/[&<>]/g, function(tag) {
            return tagsToReplace[tag] || tag;
        });
    }

    static cleanReleaseName(str) {
        return str.replace(/\s/g, '').replace(String.fromCharCode(187), '');
    }
}

module.exports = Utils;

},{}],6:[function(require,module,exports){
'use strict';
class Latinise {
    static latinise(str) {
        return str.replace(/[^A-Za-z0-9\[\] ]/g, function(x) { return Latinise.latin_map[x] || x; });
    }
    static isLatin(str) {
        return str == this.latinise(str);
    }
}

Latinise.latin_map = {
'Á': 'A', 
'Ă': 'A', 
'Ắ': 'A', 
'Ặ': 'A', 
'Ằ': 'A', 
'Ẳ': 'A', 
'Ẵ': 'A', 
'Ǎ': 'A', 
'Â': 'A', 
'Ấ': 'A', 
'Ậ': 'A', 
'Ầ': 'A', 
'Ẩ': 'A', 
'Ẫ': 'A', 
'Ä': 'A', 
'Ǟ': 'A', 
'Ȧ': 'A', 
'Ǡ': 'A', 
'Ạ': 'A', 
'Ȁ': 'A', 
'À': 'A', 
'Ả': 'A', 
'Ȃ': 'A', 
'Ā': 'A', 
'Ą': 'A', 
'Å': 'A', 
'Ǻ': 'A', 
'Ḁ': 'A', 
'Ⱥ': 'A', 
'Ã': 'A', 
'Ꜳ': 'AA', 
'Æ': 'AE', 
'Ǽ': 'AE', 
'Ǣ': 'AE', 
'Ꜵ': 'AO', 
'Ꜷ': 'AU', 
'Ꜹ': 'AV', 
'Ꜻ': 'AV', 
'Ꜽ': 'AY', 
'Ḃ': 'B', 
'Ḅ': 'B', 
'Ɓ': 'B', 
'Ḇ': 'B', 
'Ƀ': 'B', 
'Ƃ': 'B', 
'Ć': 'C', 
'Č': 'C', 
'Ç': 'C', 
'Ḉ': 'C', 
'Ĉ': 'C', 
'Ċ': 'C', 
'Ƈ': 'C', 
'Ȼ': 'C', 
'Ď': 'D', 
'Ḑ': 'D', 
'Ḓ': 'D', 
'Ḋ': 'D', 
'Ḍ': 'D', 
'Ɗ': 'D', 
'Ḏ': 'D', 
'ǲ': 'D', 
'ǅ': 'D', 
'Đ': 'D', 
'Ƌ': 'D', 
'Ǳ': 'DZ', 
'Ǆ': 'DZ', 
'É': 'E', 
'Ĕ': 'E', 
'Ě': 'E', 
'Ȩ': 'E', 
'Ḝ': 'E', 
'Ê': 'E', 
'Ế': 'E', 
'Ệ': 'E', 
'Ề': 'E', 
'Ể': 'E', 
'Ễ': 'E', 
'Ḙ': 'E', 
'Ë': 'E', 
'Ė': 'E', 
'Ẹ': 'E', 
'Ȅ': 'E', 
'È': 'E', 
'Ẻ': 'E', 
'Ȇ': 'E', 
'Ē': 'E', 
'Ḗ': 'E', 
'Ḕ': 'E', 
'Ę': 'E', 
'Ɇ': 'E', 
'Ẽ': 'E', 
'Ḛ': 'E', 
'Ꝫ': 'ET', 
'Ḟ': 'F', 
'Ƒ': 'F', 
'Ǵ': 'G', 
'Ğ': 'G', 
'Ǧ': 'G', 
'Ģ': 'G', 
'Ĝ': 'G', 
'Ġ': 'G', 
'Ɠ': 'G', 
'Ḡ': 'G', 
'Ǥ': 'G', 
'Ḫ': 'H', 
'Ȟ': 'H', 
'Ḩ': 'H', 
'Ĥ': 'H', 
'Ⱨ': 'H', 
'Ḧ': 'H', 
'Ḣ': 'H', 
'Ḥ': 'H', 
'Ħ': 'H', 
'Í': 'I', 
'Ĭ': 'I', 
'Ǐ': 'I', 
'Î': 'I', 
'Ï': 'I', 
'Ḯ': 'I', 
'İ': 'I', 
'Ị': 'I', 
'Ȉ': 'I', 
'Ì': 'I', 
'Ỉ': 'I', 
'Ȋ': 'I', 
'Ī': 'I', 
'Į': 'I', 
'Ɨ': 'I', 
'Ĩ': 'I', 
'Ḭ': 'I', 
'Ꝺ': 'D', 
'Ꝼ': 'F', 
'Ᵹ': 'G', 
'Ꞃ': 'R', 
'Ꞅ': 'S', 
'Ꞇ': 'T', 
'Ꝭ': 'IS', 
'Ĵ': 'J', 
'Ɉ': 'J', 
'Ḱ': 'K', 
'Ǩ': 'K', 
'Ķ': 'K', 
'Ⱪ': 'K', 
'Ꝃ': 'K', 
'Ḳ': 'K', 
'Ƙ': 'K', 
'Ḵ': 'K', 
'Ꝁ': 'K', 
'Ꝅ': 'K', 
'Ĺ': 'L', 
'Ƚ': 'L', 
'Ľ': 'L', 
'Ļ': 'L', 
'Ḽ': 'L', 
'Ḷ': 'L', 
'Ḹ': 'L', 
'Ⱡ': 'L', 
'Ꝉ': 'L', 
'Ḻ': 'L', 
'Ŀ': 'L', 
'Ɫ': 'L', 
'ǈ': 'L', 
'Ł': 'L', 
'Ǉ': 'LJ', 
'Ḿ': 'M', 
'Ṁ': 'M', 
'Ṃ': 'M', 
'Ɱ': 'M', 
'Ń': 'N', 
'Ň': 'N', 
'Ņ': 'N', 
'Ṋ': 'N', 
'Ṅ': 'N', 
'Ṇ': 'N', 
'Ǹ': 'N', 
'Ɲ': 'N', 
'Ṉ': 'N', 
'Ƞ': 'N', 
'ǋ': 'N', 
'Ñ': 'N', 
'Ǌ': 'NJ', 
'Ó': 'O', 
'Ŏ': 'O', 
'Ǒ': 'O', 
'Ô': 'O', 
'Ố': 'O', 
'Ộ': 'O', 
'Ồ': 'O', 
'Ổ': 'O', 
'Ỗ': 'O', 
'Ö': 'O', 
'Ȫ': 'O', 
'Ȯ': 'O', 
'Ȱ': 'O', 
'Ọ': 'O', 
'Ő': 'O', 
'Ȍ': 'O', 
'Ò': 'O', 
'Ỏ': 'O', 
'Ơ': 'O', 
'Ớ': 'O', 
'Ợ': 'O', 
'Ờ': 'O', 
'Ở': 'O', 
'Ỡ': 'O', 
'Ȏ': 'O', 
'Ꝋ': 'O', 
'Ꝍ': 'O', 
'Ō': 'O', 
'Ṓ': 'O', 
'Ṑ': 'O', 
'Ɵ': 'O', 
'Ǫ': 'O', 
'Ǭ': 'O', 
'Ø': 'O', 
'Ǿ': 'O', 
'Õ': 'O', 
'Ṍ': 'O', 
'Ṏ': 'O', 
'Ȭ': 'O', 
'Ƣ': 'OI', 
'Ꝏ': 'OO', 
'Ɛ': 'E', 
'Ɔ': 'O', 
'Ȣ': 'OU', 
'Ṕ': 'P', 
'Ṗ': 'P', 
'Ꝓ': 'P', 
'Ƥ': 'P', 
'Ꝕ': 'P', 
'Ᵽ': 'P', 
'Ꝑ': 'P', 
'Ꝙ': 'Q', 
'Ꝗ': 'Q', 
'Ŕ': 'R', 
'Ř': 'R', 
'Ŗ': 'R', 
'Ṙ': 'R', 
'Ṛ': 'R', 
'Ṝ': 'R', 
'Ȑ': 'R', 
'Ȓ': 'R', 
'Ṟ': 'R', 
'Ɍ': 'R', 
'Ɽ': 'R', 
'Ꜿ': 'C', 
'Ǝ': 'E', 
'Ś': 'S', 
'Ṥ': 'S', 
'Š': 'S', 
'Ṧ': 'S', 
'Ş': 'S', 
'Ŝ': 'S', 
'Ș': 'S', 
'Ṡ': 'S', 
'Ṣ': 'S', 
'Ṩ': 'S', 
'ẞ': 'SS', 
'Ť': 'T', 
'Ţ': 'T', 
'Ṱ': 'T', 
'Ț': 'T', 
'Ⱦ': 'T', 
'Ṫ': 'T', 
'Ṭ': 'T', 
'Ƭ': 'T', 
'Ṯ': 'T', 
'Ʈ': 'T', 
'Ŧ': 'T', 
'Ɐ': 'A', 
'Ꞁ': 'L', 
'Ɯ': 'M', 
'Ʌ': 'V', 
'Ꜩ': 'TZ', 
'Ú': 'U', 
'Ŭ': 'U', 
'Ǔ': 'U', 
'Û': 'U', 
'Ṷ': 'U', 
'Ü': 'U', 
'Ǘ': 'U', 
'Ǚ': 'U', 
'Ǜ': 'U', 
'Ǖ': 'U', 
'Ṳ': 'U', 
'Ụ': 'U', 
'Ű': 'U', 
'Ȕ': 'U', 
'Ù': 'U', 
'Ủ': 'U', 
'Ư': 'U', 
'Ứ': 'U', 
'Ự': 'U', 
'Ừ': 'U', 
'Ử': 'U', 
'Ữ': 'U', 
'Ȗ': 'U', 
'Ū': 'U', 
'Ṻ': 'U', 
'Ų': 'U', 
'Ů': 'U', 
'Ũ': 'U', 
'Ṹ': 'U', 
'Ṵ': 'U', 
'Ꝟ': 'V', 
'Ṿ': 'V', 
'Ʋ': 'V', 
'Ṽ': 'V', 
'Ꝡ': 'VY', 
'Ẃ': 'W', 
'Ŵ': 'W', 
'Ẅ': 'W', 
'Ẇ': 'W', 
'Ẉ': 'W', 
'Ẁ': 'W', 
'Ⱳ': 'W', 
'Ẍ': 'X', 
'Ẋ': 'X', 
'Ý': 'Y', 
'Ŷ': 'Y', 
'Ÿ': 'Y', 
'Ẏ': 'Y', 
'Ỵ': 'Y', 
'Ỳ': 'Y', 
'Ƴ': 'Y', 
'Ỷ': 'Y', 
'Ỿ': 'Y', 
'Ȳ': 'Y', 
'Ɏ': 'Y', 
'Ỹ': 'Y', 
'Ź': 'Z', 
'Ž': 'Z', 
'Ẑ': 'Z', 
'Ⱬ': 'Z', 
'Ż': 'Z', 
'Ẓ': 'Z', 
'Ȥ': 'Z', 
'Ẕ': 'Z', 
'Ƶ': 'Z', 
'Ĳ': 'IJ', 
'Œ': 'OE', 
'ᴀ': 'A', 
'ᴁ': 'AE', 
'ʙ': 'B', 
'ᴃ': 'B', 
'ᴄ': 'C', 
'ᴅ': 'D', 
'ᴇ': 'E', 
'ꜰ': 'F', 
'ɢ': 'G', 
'ʛ': 'G', 
'ʜ': 'H', 
'ɪ': 'I', 
'ʁ': 'R', 
'ᴊ': 'J', 
'ᴋ': 'K', 
'ʟ': 'L', 
'ᴌ': 'L', 
'ᴍ': 'M', 
'ɴ': 'N', 
'ᴏ': 'O', 
'ɶ': 'OE', 
'ᴐ': 'O', 
'ᴕ': 'OU', 
'ᴘ': 'P', 
'ʀ': 'R', 
'ᴎ': 'N', 
'ᴙ': 'R', 
'ꜱ': 'S', 
'ᴛ': 'T', 
'ⱻ': 'E', 
'ᴚ': 'R', 
'ᴜ': 'U', 
'ᴠ': 'V', 
'ᴡ': 'W', 
'ʏ': 'Y', 
'ᴢ': 'Z', 
'á': 'a', 
'ă': 'a', 
'ắ': 'a', 
'ặ': 'a', 
'ằ': 'a', 
'ẳ': 'a', 
'ẵ': 'a', 
'ǎ': 'a', 
'â': 'a', 
'ấ': 'a', 
'ậ': 'a', 
'ầ': 'a', 
'ẩ': 'a', 
'ẫ': 'a', 
'ä': 'a', 
'ǟ': 'a', 
'ȧ': 'a', 
'ǡ': 'a', 
'ạ': 'a', 
'ȁ': 'a', 
'à': 'a', 
'ả': 'a', 
'ȃ': 'a', 
'ā': 'a', 
'ą': 'a', 
'ᶏ': 'a', 
'ẚ': 'a', 
'å': 'a', 
'ǻ': 'a', 
'ḁ': 'a', 
'ⱥ': 'a', 
'ã': 'a', 
'ꜳ': 'aa', 
'æ': 'ae', 
'ǽ': 'ae', 
'ǣ': 'ae', 
'ꜵ': 'ao', 
'ꜷ': 'au', 
'ꜹ': 'av', 
'ꜻ': 'av', 
'ꜽ': 'ay', 
'ḃ': 'b', 
'ḅ': 'b', 
'ɓ': 'b', 
'ḇ': 'b', 
'ᵬ': 'b', 
'ᶀ': 'b', 
'ƀ': 'b', 
'ƃ': 'b', 
'ɵ': 'o', 
'ć': 'c', 
'č': 'c', 
'ç': 'c', 
'ḉ': 'c', 
'ĉ': 'c', 
'ɕ': 'c', 
'ċ': 'c', 
'ƈ': 'c', 
'ȼ': 'c', 
'ď': 'd', 
'ḑ': 'd', 
'ḓ': 'd', 
'ȡ': 'd', 
'ḋ': 'd', 
'ḍ': 'd', 
'ɗ': 'd', 
'ᶑ': 'd', 
'ḏ': 'd', 
'ᵭ': 'd', 
'ᶁ': 'd', 
'đ': 'd', 
'ɖ': 'd', 
'ƌ': 'd', 
'ı': 'i', 
'ȷ': 'j', 
'ɟ': 'j', 
'ʄ': 'j', 
'ǳ': 'dz', 
'ǆ': 'dz', 
'é': 'e', 
'ĕ': 'e', 
'ě': 'e', 
'ȩ': 'e', 
'ḝ': 'e', 
'ê': 'e', 
'ế': 'e', 
'ệ': 'e', 
'ề': 'e', 
'ể': 'e', 
'ễ': 'e', 
'ḙ': 'e', 
'ë': 'e', 
'ė': 'e', 
'ẹ': 'e', 
'ȅ': 'e', 
'è': 'e', 
'ẻ': 'e', 
'ȇ': 'e', 
'ē': 'e', 
'ḗ': 'e', 
'ḕ': 'e', 
'ⱸ': 'e', 
'ę': 'e', 
'ᶒ': 'e', 
'ɇ': 'e', 
'ẽ': 'e', 
'ḛ': 'e', 
'ꝫ': 'et', 
'ḟ': 'f', 
'ƒ': 'f', 
'ᵮ': 'f', 
'ᶂ': 'f', 
'ǵ': 'g', 
'ğ': 'g', 
'ǧ': 'g', 
'ģ': 'g', 
'ĝ': 'g', 
'ġ': 'g', 
'ɠ': 'g', 
'ḡ': 'g', 
'ᶃ': 'g', 
'ǥ': 'g', 
'ḫ': 'h', 
'ȟ': 'h', 
'ḩ': 'h', 
'ĥ': 'h', 
'ⱨ': 'h', 
'ḧ': 'h', 
'ḣ': 'h', 
'ḥ': 'h', 
'ɦ': 'h', 
'ẖ': 'h', 
'ħ': 'h', 
'ƕ': 'hv', 
'í': 'i', 
'ĭ': 'i', 
'ǐ': 'i', 
'î': 'i', 
'ï': 'i', 
'ḯ': 'i', 
'ị': 'i', 
'ȉ': 'i', 
'ì': 'i', 
'ỉ': 'i', 
'ȋ': 'i', 
'ī': 'i', 
'į': 'i', 
'ᶖ': 'i', 
'ɨ': 'i', 
'ĩ': 'i', 
'ḭ': 'i', 
'ꝺ': 'd', 
'ꝼ': 'f', 
'ᵹ': 'g', 
'ꞃ': 'r', 
'ꞅ': 's', 
'ꞇ': 't', 
'ꝭ': 'is', 
'ǰ': 'j', 
'ĵ': 'j', 
'ʝ': 'j', 
'ɉ': 'j', 
'ḱ': 'k', 
'ǩ': 'k', 
'ķ': 'k', 
'ⱪ': 'k', 
'ꝃ': 'k', 
'ḳ': 'k', 
'ƙ': 'k', 
'ḵ': 'k', 
'ᶄ': 'k', 
'ꝁ': 'k', 
'ꝅ': 'k', 
'ĺ': 'l', 
'ƚ': 'l', 
'ɬ': 'l', 
'ľ': 'l', 
'ļ': 'l', 
'ḽ': 'l', 
'ȴ': 'l', 
'ḷ': 'l', 
'ḹ': 'l', 
'ⱡ': 'l', 
'ꝉ': 'l', 
'ḻ': 'l', 
'ŀ': 'l', 
'ɫ': 'l', 
'ᶅ': 'l', 
'ɭ': 'l', 
'ł': 'l', 
'ǉ': 'lj', 
'ſ': 's', 
'ẜ': 's', 
'ẛ': 's', 
'ẝ': 's', 
'ḿ': 'm', 
'ṁ': 'm', 
'ṃ': 'm', 
'ɱ': 'm', 
'ᵯ': 'm', 
'ᶆ': 'm', 
'ń': 'n', 
'ň': 'n', 
'ņ': 'n', 
'ṋ': 'n', 
'ȵ': 'n', 
'ṅ': 'n', 
'ṇ': 'n', 
'ǹ': 'n', 
'ɲ': 'n', 
'ṉ': 'n', 
'ƞ': 'n', 
'ᵰ': 'n', 
'ᶇ': 'n', 
'ɳ': 'n', 
'ñ': 'n', 
'ǌ': 'nj', 
'ó': 'o', 
'ŏ': 'o', 
'ǒ': 'o', 
'ô': 'o', 
'ố': 'o', 
'ộ': 'o', 
'ồ': 'o', 
'ổ': 'o', 
'ỗ': 'o', 
'ö': 'o', 
'ȫ': 'o', 
'ȯ': 'o', 
'ȱ': 'o', 
'ọ': 'o', 
'ő': 'o', 
'ȍ': 'o', 
'ò': 'o', 
'ỏ': 'o', 
'ơ': 'o', 
'ớ': 'o', 
'ợ': 'o', 
'ờ': 'o', 
'ở': 'o', 
'ỡ': 'o', 
'ȏ': 'o', 
'ꝋ': 'o', 
'ꝍ': 'o', 
'ⱺ': 'o', 
'ō': 'o', 
'ṓ': 'o', 
'ṑ': 'o', 
'ǫ': 'o', 
'ǭ': 'o', 
'ø': 'o', 
'ǿ': 'o', 
'õ': 'o', 
'ṍ': 'o', 
'ṏ': 'o', 
'ȭ': 'o', 
'ƣ': 'oi', 
'ꝏ': 'oo', 
'ɛ': 'e', 
'ᶓ': 'e', 
'ɔ': 'o', 
'ᶗ': 'o', 
'ȣ': 'ou', 
'ṕ': 'p', 
'ṗ': 'p', 
'ꝓ': 'p', 
'ƥ': 'p', 
'ᵱ': 'p', 
'ᶈ': 'p', 
'ꝕ': 'p', 
'ᵽ': 'p', 
'ꝑ': 'p', 
'ꝙ': 'q', 
'ʠ': 'q', 
'ɋ': 'q', 
'ꝗ': 'q', 
'ŕ': 'r', 
'ř': 'r', 
'ŗ': 'r', 
'ṙ': 'r', 
'ṛ': 'r', 
'ṝ': 'r', 
'ȑ': 'r', 
'ɾ': 'r', 
'ᵳ': 'r', 
'ȓ': 'r', 
'ṟ': 'r', 
'ɼ': 'r', 
'ᵲ': 'r', 
'ᶉ': 'r', 
'ɍ': 'r', 
'ɽ': 'r', 
'ↄ': 'c', 
'ꜿ': 'c', 
'ɘ': 'e', 
'ɿ': 'r', 
'ś': 's', 
'ṥ': 's', 
'š': 's', 
'ṧ': 's', 
'ş': 's', 
'ŝ': 's', 
'ș': 's', 
'ṡ': 's', 
'ṣ': 's', 
'ṩ': 's', 
'ʂ': 's', 
'ᵴ': 's', 
'ᶊ': 's', 
'ȿ': 's', 
'ɡ': 'g', 
'ß': 'ss', 
'ᴑ': 'o', 
'ᴓ': 'o', 
'ᴝ': 'u', 
'ť': 't', 
'ţ': 't', 
'ṱ': 't', 
'ț': 't', 
'ȶ': 't', 
'ẗ': 't', 
'ⱦ': 't', 
'ṫ': 't', 
'ṭ': 't', 
'ƭ': 't', 
'ṯ': 't', 
'ᵵ': 't', 
'ƫ': 't', 
'ʈ': 't', 
'ŧ': 't', 
'ᵺ': 'th', 
'ɐ': 'a', 
'ᴂ': 'ae', 
'ǝ': 'e', 
'ᵷ': 'g', 
'ɥ': 'h', 
'ʮ': 'h', 
'ʯ': 'h', 
'ᴉ': 'i', 
'ʞ': 'k', 
'ꞁ': 'l', 
'ɯ': 'm', 
'ɰ': 'm', 
'ᴔ': 'oe', 
'ɹ': 'r', 
'ɻ': 'r', 
'ɺ': 'r', 
'ⱹ': 'r', 
'ʇ': 't', 
'ʌ': 'v', 
'ʍ': 'w', 
'ʎ': 'y', 
'ꜩ': 'tz', 
'ú': 'u', 
'ŭ': 'u', 
'ǔ': 'u', 
'û': 'u', 
'ṷ': 'u', 
'ü': 'u', 
'ǘ': 'u', 
'ǚ': 'u', 
'ǜ': 'u', 
'ǖ': 'u', 
'ṳ': 'u', 
'ụ': 'u', 
'ű': 'u', 
'ȕ': 'u', 
'ù': 'u', 
'ủ': 'u', 
'ư': 'u', 
'ứ': 'u', 
'ự': 'u', 
'ừ': 'u', 
'ử': 'u', 
'ữ': 'u', 
'ȗ': 'u', 
'ū': 'u', 
'ṻ': 'u', 
'ų': 'u', 
'ᶙ': 'u', 
'ů': 'u', 
'ũ': 'u', 
'ṹ': 'u', 
'ṵ': 'u', 
'ᵫ': 'ue', 
'ꝸ': 'um', 
'ⱴ': 'v', 
'ꝟ': 'v', 
'ṿ': 'v', 
'ʋ': 'v', 
'ᶌ': 'v', 
'ⱱ': 'v', 
'ṽ': 'v', 
'ꝡ': 'vy', 
'ẃ': 'w', 
'ŵ': 'w', 
'ẅ': 'w', 
'ẇ': 'w', 
'ẉ': 'w', 
'ẁ': 'w', 
'ⱳ': 'w', 
'ẘ': 'w', 
'ẍ': 'x', 
'ẋ': 'x', 
'ᶍ': 'x', 
'ý': 'y', 
'ŷ': 'y', 
'ÿ': 'y', 
'ẏ': 'y', 
'ỵ': 'y', 
'ỳ': 'y', 
'ƴ': 'y', 
'ỷ': 'y', 
'ỿ': 'y', 
'ȳ': 'y', 
'ẙ': 'y', 
'ɏ': 'y', 
'ỹ': 'y', 
'ź': 'z', 
'ž': 'z', 
'ẑ': 'z', 
'ʑ': 'z', 
'ⱬ': 'z', 
'ż': 'z', 
'ẓ': 'z', 
'ȥ': 'z', 
'ẕ': 'z', 
'ᵶ': 'z', 
'ᶎ': 'z', 
'ʐ': 'z', 
'ƶ': 'z', 
'ɀ': 'z', 
'ﬀ': 'ff', 
'ﬃ': 'ffi', 
'ﬄ': 'ffl', 
'ﬁ': 'fi', 
'ﬂ': 'fl', 
'ĳ': 'ij', 
'œ': 'oe', 
'ﬆ': 'st', 
'ₐ': 'a', 
'ₑ': 'e', 
'ᵢ': 'i', 
'ⱼ': 'j', 
'ₒ': 'o', 
'ᵣ': 'r', 
'ᵤ': 'u', 
'ᵥ': 'v', 
'ₓ': 'x' 
};

module.exports = Latinise;
},{}],7:[function(require,module,exports){
(function (process,global){
/* @preserve
 * The MIT License (MIT)
 * 
 * Copyright (c) 2013-2015 Petka Antonov
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 */
/**
 * bluebird build version 3.4.0
 * Features enabled: core, race, call_get, generators, map, nodeify, promisify, props, reduce, settle, some, using, timers, filter, any, each
*/
!function(e){if("object"==typeof exports&&"undefined"!=typeof module)module.exports=e();else if("function"==typeof define&&define.amd)define([],e);else{var f;"undefined"!=typeof window?f=window:"undefined"!=typeof global?f=global:"undefined"!=typeof self&&(f=self),f.Promise=e()}}(function(){var define,module,exports;return (function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof _dereq_=="function"&&_dereq_;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof _dereq_=="function"&&_dereq_;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(_dereq_,module,exports){
"use strict";
module.exports = function(Promise) {
var SomePromiseArray = Promise._SomePromiseArray;
function any(promises) {
    var ret = new SomePromiseArray(promises);
    var promise = ret.promise();
    ret.setHowMany(1);
    ret.setUnwrap();
    ret.init();
    return promise;
}

Promise.any = function (promises) {
    return any(promises);
};

Promise.prototype.any = function () {
    return any(this);
};

};

},{}],2:[function(_dereq_,module,exports){
"use strict";
var firstLineError;
try {throw new Error(); } catch (e) {firstLineError = e;}
var schedule = _dereq_("./schedule");
var Queue = _dereq_("./queue");
var util = _dereq_("./util");

function Async() {
    this._customScheduler = false;
    this._isTickUsed = false;
    this._lateQueue = new Queue(16);
    this._normalQueue = new Queue(16);
    this._haveDrainedQueues = false;
    this._trampolineEnabled = true;
    var self = this;
    this.drainQueues = function () {
        self._drainQueues();
    };
    this._schedule = schedule;
}

Async.prototype.setScheduler = function(fn) {
    var prev = this._schedule;
    this._schedule = fn;
    this._customScheduler = true;
    return prev;
};

Async.prototype.hasCustomScheduler = function() {
    return this._customScheduler;
};

Async.prototype.enableTrampoline = function() {
    this._trampolineEnabled = true;
};

Async.prototype.disableTrampolineIfNecessary = function() {
    if (util.hasDevTools) {
        this._trampolineEnabled = false;
    }
};

Async.prototype.haveItemsQueued = function () {
    return this._isTickUsed || this._haveDrainedQueues;
};


Async.prototype.fatalError = function(e, isNode) {
    if (isNode) {
        process.stderr.write("Fatal " + (e instanceof Error ? e.stack : e) +
            "\n");
        process.exit(2);
    } else {
        this.throwLater(e);
    }
};

Async.prototype.throwLater = function(fn, arg) {
    if (arguments.length === 1) {
        arg = fn;
        fn = function () { throw arg; };
    }
    if (typeof setTimeout !== "undefined") {
        setTimeout(function() {
            fn(arg);
        }, 0);
    } else try {
        this._schedule(function() {
            fn(arg);
        });
    } catch (e) {
        throw new Error("No async scheduler available\u000a\u000a    See http://goo.gl/MqrFmX\u000a");
    }
};

function AsyncInvokeLater(fn, receiver, arg) {
    this._lateQueue.push(fn, receiver, arg);
    this._queueTick();
}

function AsyncInvoke(fn, receiver, arg) {
    this._normalQueue.push(fn, receiver, arg);
    this._queueTick();
}

function AsyncSettlePromises(promise) {
    this._normalQueue._pushOne(promise);
    this._queueTick();
}

if (!util.hasDevTools) {
    Async.prototype.invokeLater = AsyncInvokeLater;
    Async.prototype.invoke = AsyncInvoke;
    Async.prototype.settlePromises = AsyncSettlePromises;
} else {
    Async.prototype.invokeLater = function (fn, receiver, arg) {
        if (this._trampolineEnabled) {
            AsyncInvokeLater.call(this, fn, receiver, arg);
        } else {
            this._schedule(function() {
                setTimeout(function() {
                    fn.call(receiver, arg);
                }, 100);
            });
        }
    };

    Async.prototype.invoke = function (fn, receiver, arg) {
        if (this._trampolineEnabled) {
            AsyncInvoke.call(this, fn, receiver, arg);
        } else {
            this._schedule(function() {
                fn.call(receiver, arg);
            });
        }
    };

    Async.prototype.settlePromises = function(promise) {
        if (this._trampolineEnabled) {
            AsyncSettlePromises.call(this, promise);
        } else {
            this._schedule(function() {
                promise._settlePromises();
            });
        }
    };
}

Async.prototype.invokeFirst = function (fn, receiver, arg) {
    this._normalQueue.unshift(fn, receiver, arg);
    this._queueTick();
};

Async.prototype._drainQueue = function(queue) {
    while (queue.length() > 0) {
        var fn = queue.shift();
        if (typeof fn !== "function") {
            fn._settlePromises();
            continue;
        }
        var receiver = queue.shift();
        var arg = queue.shift();
        fn.call(receiver, arg);
    }
};

Async.prototype._drainQueues = function () {
    this._drainQueue(this._normalQueue);
    this._reset();
    this._haveDrainedQueues = true;
    this._drainQueue(this._lateQueue);
};

Async.prototype._queueTick = function () {
    if (!this._isTickUsed) {
        this._isTickUsed = true;
        this._schedule(this.drainQueues);
    }
};

Async.prototype._reset = function () {
    this._isTickUsed = false;
};

module.exports = Async;
module.exports.firstLineError = firstLineError;

},{"./queue":26,"./schedule":29,"./util":36}],3:[function(_dereq_,module,exports){
"use strict";
module.exports = function(Promise, INTERNAL, tryConvertToPromise, debug) {
var calledBind = false;
var rejectThis = function(_, e) {
    this._reject(e);
};

var targetRejected = function(e, context) {
    context.promiseRejectionQueued = true;
    context.bindingPromise._then(rejectThis, rejectThis, null, this, e);
};

var bindingResolved = function(thisArg, context) {
    if (((this._bitField & 50397184) === 0)) {
        this._resolveCallback(context.target);
    }
};

var bindingRejected = function(e, context) {
    if (!context.promiseRejectionQueued) this._reject(e);
};

Promise.prototype.bind = function (thisArg) {
    if (!calledBind) {
        calledBind = true;
        Promise.prototype._propagateFrom = debug.propagateFromFunction();
        Promise.prototype._boundValue = debug.boundValueFunction();
    }
    var maybePromise = tryConvertToPromise(thisArg);
    var ret = new Promise(INTERNAL);
    ret._propagateFrom(this, 1);
    var target = this._target();
    ret._setBoundTo(maybePromise);
    if (maybePromise instanceof Promise) {
        var context = {
            promiseRejectionQueued: false,
            promise: ret,
            target: target,
            bindingPromise: maybePromise
        };
        target._then(INTERNAL, targetRejected, undefined, ret, context);
        maybePromise._then(
            bindingResolved, bindingRejected, undefined, ret, context);
        ret._setOnCancel(maybePromise);
    } else {
        ret._resolveCallback(target);
    }
    return ret;
};

Promise.prototype._setBoundTo = function (obj) {
    if (obj !== undefined) {
        this._bitField = this._bitField | 2097152;
        this._boundTo = obj;
    } else {
        this._bitField = this._bitField & (~2097152);
    }
};

Promise.prototype._isBound = function () {
    return (this._bitField & 2097152) === 2097152;
};

Promise.bind = function (thisArg, value) {
    return Promise.resolve(value).bind(thisArg);
};
};

},{}],4:[function(_dereq_,module,exports){
"use strict";
var old;
if (typeof Promise !== "undefined") old = Promise;
function noConflict() {
    try { if (Promise === bluebird) Promise = old; }
    catch (e) {}
    return bluebird;
}
var bluebird = _dereq_("./promise")();
bluebird.noConflict = noConflict;
module.exports = bluebird;

},{"./promise":22}],5:[function(_dereq_,module,exports){
"use strict";
var cr = Object.create;
if (cr) {
    var callerCache = cr(null);
    var getterCache = cr(null);
    callerCache[" size"] = getterCache[" size"] = 0;
}

module.exports = function(Promise) {
var util = _dereq_("./util");
var canEvaluate = util.canEvaluate;
var isIdentifier = util.isIdentifier;

var getMethodCaller;
var getGetter;
if (!true) {
var makeMethodCaller = function (methodName) {
    return new Function("ensureMethod", "                                    \n\
        return function(obj) {                                               \n\
            'use strict'                                                     \n\
            var len = this.length;                                           \n\
            ensureMethod(obj, 'methodName');                                 \n\
            switch(len) {                                                    \n\
                case 1: return obj.methodName(this[0]);                      \n\
                case 2: return obj.methodName(this[0], this[1]);             \n\
                case 3: return obj.methodName(this[0], this[1], this[2]);    \n\
                case 0: return obj.methodName();                             \n\
                default:                                                     \n\
                    return obj.methodName.apply(obj, this);                  \n\
            }                                                                \n\
        };                                                                   \n\
        ".replace(/methodName/g, methodName))(ensureMethod);
};

var makeGetter = function (propertyName) {
    return new Function("obj", "                                             \n\
        'use strict';                                                        \n\
        return obj.propertyName;                                             \n\
        ".replace("propertyName", propertyName));
};

var getCompiled = function(name, compiler, cache) {
    var ret = cache[name];
    if (typeof ret !== "function") {
        if (!isIdentifier(name)) {
            return null;
        }
        ret = compiler(name);
        cache[name] = ret;
        cache[" size"]++;
        if (cache[" size"] > 512) {
            var keys = Object.keys(cache);
            for (var i = 0; i < 256; ++i) delete cache[keys[i]];
            cache[" size"] = keys.length - 256;
        }
    }
    return ret;
};

getMethodCaller = function(name) {
    return getCompiled(name, makeMethodCaller, callerCache);
};

getGetter = function(name) {
    return getCompiled(name, makeGetter, getterCache);
};
}

function ensureMethod(obj, methodName) {
    var fn;
    if (obj != null) fn = obj[methodName];
    if (typeof fn !== "function") {
        var message = "Object " + util.classString(obj) + " has no method '" +
            util.toString(methodName) + "'";
        throw new Promise.TypeError(message);
    }
    return fn;
}

function caller(obj) {
    var methodName = this.pop();
    var fn = ensureMethod(obj, methodName);
    return fn.apply(obj, this);
}
Promise.prototype.call = function (methodName) {
    var args = [].slice.call(arguments, 1);;
    if (!true) {
        if (canEvaluate) {
            var maybeCaller = getMethodCaller(methodName);
            if (maybeCaller !== null) {
                return this._then(
                    maybeCaller, undefined, undefined, args, undefined);
            }
        }
    }
    args.push(methodName);
    return this._then(caller, undefined, undefined, args, undefined);
};

function namedGetter(obj) {
    return obj[this];
}
function indexedGetter(obj) {
    var index = +this;
    if (index < 0) index = Math.max(0, index + obj.length);
    return obj[index];
}
Promise.prototype.get = function (propertyName) {
    var isIndex = (typeof propertyName === "number");
    var getter;
    if (!isIndex) {
        if (canEvaluate) {
            var maybeGetter = getGetter(propertyName);
            getter = maybeGetter !== null ? maybeGetter : namedGetter;
        } else {
            getter = namedGetter;
        }
    } else {
        getter = indexedGetter;
    }
    return this._then(getter, undefined, undefined, propertyName, undefined);
};
};

},{"./util":36}],6:[function(_dereq_,module,exports){
"use strict";
module.exports = function(Promise, PromiseArray, apiRejection, debug) {
var util = _dereq_("./util");
var tryCatch = util.tryCatch;
var errorObj = util.errorObj;
var async = Promise._async;

Promise.prototype["break"] = Promise.prototype.cancel = function() {
    if (!debug.cancellation()) return this._warn("cancellation is disabled");

    var promise = this;
    var child = promise;
    while (promise.isCancellable()) {
        if (!promise._cancelBy(child)) {
            if (child._isFollowing()) {
                child._followee().cancel();
            } else {
                child._cancelBranched();
            }
            break;
        }

        var parent = promise._cancellationParent;
        if (parent == null || !parent.isCancellable()) {
            if (promise._isFollowing()) {
                promise._followee().cancel();
            } else {
                promise._cancelBranched();
            }
            break;
        } else {
            if (promise._isFollowing()) promise._followee().cancel();
            child = promise;
            promise = parent;
        }
    }
};

Promise.prototype._branchHasCancelled = function() {
    this._branchesRemainingToCancel--;
};

Promise.prototype._enoughBranchesHaveCancelled = function() {
    return this._branchesRemainingToCancel === undefined ||
           this._branchesRemainingToCancel <= 0;
};

Promise.prototype._cancelBy = function(canceller) {
    if (canceller === this) {
        this._branchesRemainingToCancel = 0;
        this._invokeOnCancel();
        return true;
    } else {
        this._branchHasCancelled();
        if (this._enoughBranchesHaveCancelled()) {
            this._invokeOnCancel();
            return true;
        }
    }
    return false;
};

Promise.prototype._cancelBranched = function() {
    if (this._enoughBranchesHaveCancelled()) {
        this._cancel();
    }
};

Promise.prototype._cancel = function() {
    if (!this.isCancellable()) return;

    this._setCancelled();
    async.invoke(this._cancelPromises, this, undefined);
};

Promise.prototype._cancelPromises = function() {
    if (this._length() > 0) this._settlePromises();
};

Promise.prototype._unsetOnCancel = function() {
    this._onCancelField = undefined;
};

Promise.prototype.isCancellable = function() {
    return this.isPending() && !this.isCancelled();
};

Promise.prototype._doInvokeOnCancel = function(onCancelCallback, internalOnly) {
    if (util.isArray(onCancelCallback)) {
        for (var i = 0; i < onCancelCallback.length; ++i) {
            this._doInvokeOnCancel(onCancelCallback[i], internalOnly);
        }
    } else if (onCancelCallback !== undefined) {
        if (typeof onCancelCallback === "function") {
            if (!internalOnly) {
                var e = tryCatch(onCancelCallback).call(this._boundValue());
                if (e === errorObj) {
                    this._attachExtraTrace(e.e);
                    async.throwLater(e.e);
                }
            }
        } else {
            onCancelCallback._resultCancelled(this);
        }
    }
};

Promise.prototype._invokeOnCancel = function() {
    var onCancelCallback = this._onCancel();
    this._unsetOnCancel();
    async.invoke(this._doInvokeOnCancel, this, onCancelCallback);
};

Promise.prototype._invokeInternalOnCancel = function() {
    if (this.isCancellable()) {
        this._doInvokeOnCancel(this._onCancel(), true);
        this._unsetOnCancel();
    }
};

Promise.prototype._resultCancelled = function() {
    this.cancel();
};

};

},{"./util":36}],7:[function(_dereq_,module,exports){
"use strict";
module.exports = function(NEXT_FILTER) {
var util = _dereq_("./util");
var getKeys = _dereq_("./es5").keys;
var tryCatch = util.tryCatch;
var errorObj = util.errorObj;

function catchFilter(instances, cb, promise) {
    return function(e) {
        var boundTo = promise._boundValue();
        predicateLoop: for (var i = 0; i < instances.length; ++i) {
            var item = instances[i];

            if (item === Error ||
                (item != null && item.prototype instanceof Error)) {
                if (e instanceof item) {
                    return tryCatch(cb).call(boundTo, e);
                }
            } else if (typeof item === "function") {
                var matchesPredicate = tryCatch(item).call(boundTo, e);
                if (matchesPredicate === errorObj) {
                    return matchesPredicate;
                } else if (matchesPredicate) {
                    return tryCatch(cb).call(boundTo, e);
                }
            } else if (util.isObject(e)) {
                var keys = getKeys(item);
                for (var j = 0; j < keys.length; ++j) {
                    var key = keys[j];
                    if (item[key] != e[key]) {
                        continue predicateLoop;
                    }
                }
                return tryCatch(cb).call(boundTo, e);
            }
        }
        return NEXT_FILTER;
    };
}

return catchFilter;
};

},{"./es5":13,"./util":36}],8:[function(_dereq_,module,exports){
"use strict";
module.exports = function(Promise) {
var longStackTraces = false;
var contextStack = [];

Promise.prototype._promiseCreated = function() {};
Promise.prototype._pushContext = function() {};
Promise.prototype._popContext = function() {return null;};
Promise._peekContext = Promise.prototype._peekContext = function() {};

function Context() {
    this._trace = new Context.CapturedTrace(peekContext());
}
Context.prototype._pushContext = function () {
    if (this._trace !== undefined) {
        this._trace._promiseCreated = null;
        contextStack.push(this._trace);
    }
};

Context.prototype._popContext = function () {
    if (this._trace !== undefined) {
        var trace = contextStack.pop();
        var ret = trace._promiseCreated;
        trace._promiseCreated = null;
        return ret;
    }
    return null;
};

function createContext() {
    if (longStackTraces) return new Context();
}

function peekContext() {
    var lastIndex = contextStack.length - 1;
    if (lastIndex >= 0) {
        return contextStack[lastIndex];
    }
    return undefined;
}
Context.CapturedTrace = null;
Context.create = createContext;
Context.deactivateLongStackTraces = function() {};
Context.activateLongStackTraces = function() {
    var Promise_pushContext = Promise.prototype._pushContext;
    var Promise_popContext = Promise.prototype._popContext;
    var Promise_PeekContext = Promise._peekContext;
    var Promise_peekContext = Promise.prototype._peekContext;
    var Promise_promiseCreated = Promise.prototype._promiseCreated;
    Context.deactivateLongStackTraces = function() {
        Promise.prototype._pushContext = Promise_pushContext;
        Promise.prototype._popContext = Promise_popContext;
        Promise._peekContext = Promise_PeekContext;
        Promise.prototype._peekContext = Promise_peekContext;
        Promise.prototype._promiseCreated = Promise_promiseCreated;
        longStackTraces = false;
    };
    longStackTraces = true;
    Promise.prototype._pushContext = Context.prototype._pushContext;
    Promise.prototype._popContext = Context.prototype._popContext;
    Promise._peekContext = Promise.prototype._peekContext = peekContext;
    Promise.prototype._promiseCreated = function() {
        var ctx = this._peekContext();
        if (ctx && ctx._promiseCreated == null) ctx._promiseCreated = this;
    };
};
return Context;
};

},{}],9:[function(_dereq_,module,exports){
"use strict";
module.exports = function(Promise, Context) {
var getDomain = Promise._getDomain;
var async = Promise._async;
var Warning = _dereq_("./errors").Warning;
var util = _dereq_("./util");
var canAttachTrace = util.canAttachTrace;
var unhandledRejectionHandled;
var possiblyUnhandledRejection;
var bluebirdFramePattern =
    /[\\\/]bluebird[\\\/]js[\\\/](release|debug|instrumented)/;
var stackFramePattern = null;
var formatStack = null;
var indentStackFrames = false;
var printWarning;
var debugging = !!(util.env("BLUEBIRD_DEBUG") != 0 &&
                        (true ||
                         util.env("BLUEBIRD_DEBUG") ||
                         util.env("NODE_ENV") === "development"));

var warnings = !!(util.env("BLUEBIRD_WARNINGS") != 0 &&
    (debugging || util.env("BLUEBIRD_WARNINGS")));

var longStackTraces = !!(util.env("BLUEBIRD_LONG_STACK_TRACES") != 0 &&
    (debugging || util.env("BLUEBIRD_LONG_STACK_TRACES")));

var wForgottenReturn = util.env("BLUEBIRD_W_FORGOTTEN_RETURN") != 0 &&
    (warnings || !!util.env("BLUEBIRD_W_FORGOTTEN_RETURN"));

Promise.prototype.suppressUnhandledRejections = function() {
    var target = this._target();
    target._bitField = ((target._bitField & (~1048576)) |
                      524288);
};

Promise.prototype._ensurePossibleRejectionHandled = function () {
    if ((this._bitField & 524288) !== 0) return;
    this._setRejectionIsUnhandled();
    async.invokeLater(this._notifyUnhandledRejection, this, undefined);
};

Promise.prototype._notifyUnhandledRejectionIsHandled = function () {
    fireRejectionEvent("rejectionHandled",
                                  unhandledRejectionHandled, undefined, this);
};

Promise.prototype._setReturnedNonUndefined = function() {
    this._bitField = this._bitField | 268435456;
};

Promise.prototype._returnedNonUndefined = function() {
    return (this._bitField & 268435456) !== 0;
};

Promise.prototype._notifyUnhandledRejection = function () {
    if (this._isRejectionUnhandled()) {
        var reason = this._settledValue();
        this._setUnhandledRejectionIsNotified();
        fireRejectionEvent("unhandledRejection",
                                      possiblyUnhandledRejection, reason, this);
    }
};

Promise.prototype._setUnhandledRejectionIsNotified = function () {
    this._bitField = this._bitField | 262144;
};

Promise.prototype._unsetUnhandledRejectionIsNotified = function () {
    this._bitField = this._bitField & (~262144);
};

Promise.prototype._isUnhandledRejectionNotified = function () {
    return (this._bitField & 262144) > 0;
};

Promise.prototype._setRejectionIsUnhandled = function () {
    this._bitField = this._bitField | 1048576;
};

Promise.prototype._unsetRejectionIsUnhandled = function () {
    this._bitField = this._bitField & (~1048576);
    if (this._isUnhandledRejectionNotified()) {
        this._unsetUnhandledRejectionIsNotified();
        this._notifyUnhandledRejectionIsHandled();
    }
};

Promise.prototype._isRejectionUnhandled = function () {
    return (this._bitField & 1048576) > 0;
};

Promise.prototype._warn = function(message, shouldUseOwnTrace, promise) {
    return warn(message, shouldUseOwnTrace, promise || this);
};

Promise.onPossiblyUnhandledRejection = function (fn) {
    var domain = getDomain();
    possiblyUnhandledRejection =
        typeof fn === "function" ? (domain === null ? fn : domain.bind(fn))
                                 : undefined;
};

Promise.onUnhandledRejectionHandled = function (fn) {
    var domain = getDomain();
    unhandledRejectionHandled =
        typeof fn === "function" ? (domain === null ? fn : domain.bind(fn))
                                 : undefined;
};

var disableLongStackTraces = function() {};
Promise.longStackTraces = function () {
    if (async.haveItemsQueued() && !config.longStackTraces) {
        throw new Error("cannot enable long stack traces after promises have been created\u000a\u000a    See http://goo.gl/MqrFmX\u000a");
    }
    if (!config.longStackTraces && longStackTracesIsSupported()) {
        var Promise_captureStackTrace = Promise.prototype._captureStackTrace;
        var Promise_attachExtraTrace = Promise.prototype._attachExtraTrace;
        config.longStackTraces = true;
        disableLongStackTraces = function() {
            if (async.haveItemsQueued() && !config.longStackTraces) {
                throw new Error("cannot enable long stack traces after promises have been created\u000a\u000a    See http://goo.gl/MqrFmX\u000a");
            }
            Promise.prototype._captureStackTrace = Promise_captureStackTrace;
            Promise.prototype._attachExtraTrace = Promise_attachExtraTrace;
            Context.deactivateLongStackTraces();
            async.enableTrampoline();
            config.longStackTraces = false;
        };
        Promise.prototype._captureStackTrace = longStackTracesCaptureStackTrace;
        Promise.prototype._attachExtraTrace = longStackTracesAttachExtraTrace;
        Context.activateLongStackTraces();
        async.disableTrampolineIfNecessary();
    }
};

Promise.hasLongStackTraces = function () {
    return config.longStackTraces && longStackTracesIsSupported();
};

var fireDomEvent = (function() {
    try {
        var event = document.createEvent("CustomEvent");
        event.initCustomEvent("testingtheevent", false, true, {});
        util.global.dispatchEvent(event);
        return function(name, event) {
            var domEvent = document.createEvent("CustomEvent");
            domEvent.initCustomEvent(name.toLowerCase(), false, true, event);
            return !util.global.dispatchEvent(domEvent);
        };
    } catch (e) {}
    return function() {
        return false;
    };
})();

var fireGlobalEvent = (function() {
    if (util.isNode) {
        return function() {
            return process.emit.apply(process, arguments);
        };
    } else {
        if (!util.global) {
            return function() {
                return false;
            };
        }
        return function(name) {
            var methodName = "on" + name.toLowerCase();
            var method = util.global[methodName];
            if (!method) return false;
            method.apply(util.global, [].slice.call(arguments, 1));
            return true;
        };
    }
})();

function generatePromiseLifecycleEventObject(name, promise) {
    return {promise: promise};
}

var eventToObjectGenerator = {
    promiseCreated: generatePromiseLifecycleEventObject,
    promiseFulfilled: generatePromiseLifecycleEventObject,
    promiseRejected: generatePromiseLifecycleEventObject,
    promiseResolved: generatePromiseLifecycleEventObject,
    promiseCancelled: generatePromiseLifecycleEventObject,
    promiseChained: function(name, promise, child) {
        return {promise: promise, child: child};
    },
    warning: function(name, warning) {
        return {warning: warning};
    },
    unhandledRejection: function (name, reason, promise) {
        return {reason: reason, promise: promise};
    },
    rejectionHandled: generatePromiseLifecycleEventObject
};

var activeFireEvent = function (name) {
    var globalEventFired = false;
    try {
        globalEventFired = fireGlobalEvent.apply(null, arguments);
    } catch (e) {
        async.throwLater(e);
        globalEventFired = true;
    }

    var domEventFired = false;
    try {
        domEventFired = fireDomEvent(name,
                    eventToObjectGenerator[name].apply(null, arguments));
    } catch (e) {
        async.throwLater(e);
        domEventFired = true;
    }

    return domEventFired || globalEventFired;
};

Promise.config = function(opts) {
    opts = Object(opts);
    if ("longStackTraces" in opts) {
        if (opts.longStackTraces) {
            Promise.longStackTraces();
        } else if (!opts.longStackTraces && Promise.hasLongStackTraces()) {
            disableLongStackTraces();
        }
    }
    if ("warnings" in opts) {
        var warningsOption = opts.warnings;
        config.warnings = !!warningsOption;
        wForgottenReturn = config.warnings;

        if (util.isObject(warningsOption)) {
            if ("wForgottenReturn" in warningsOption) {
                wForgottenReturn = !!warningsOption.wForgottenReturn;
            }
        }
    }
    if ("cancellation" in opts && opts.cancellation && !config.cancellation) {
        if (async.haveItemsQueued()) {
            throw new Error(
                "cannot enable cancellation after promises are in use");
        }
        Promise.prototype._clearCancellationData =
            cancellationClearCancellationData;
        Promise.prototype._propagateFrom = cancellationPropagateFrom;
        Promise.prototype._onCancel = cancellationOnCancel;
        Promise.prototype._setOnCancel = cancellationSetOnCancel;
        Promise.prototype._attachCancellationCallback =
            cancellationAttachCancellationCallback;
        Promise.prototype._execute = cancellationExecute;
        propagateFromFunction = cancellationPropagateFrom;
        config.cancellation = true;
    }
    if ("monitoring" in opts) {
        if (opts.monitoring && !config.monitoring) {
            config.monitoring = true;
            Promise.prototype._fireEvent = activeFireEvent;
        } else if (!opts.monitoring && config.monitoring) {
            config.monitoring = false;
            Promise.prototype._fireEvent = defaultFireEvent;
        }
    }
};

function defaultFireEvent() { return false; }

Promise.prototype._fireEvent = defaultFireEvent;
Promise.prototype._execute = function(executor, resolve, reject) {
    try {
        executor(resolve, reject);
    } catch (e) {
        return e;
    }
};
Promise.prototype._onCancel = function () {};
Promise.prototype._setOnCancel = function (handler) { ; };
Promise.prototype._attachCancellationCallback = function(onCancel) {
    ;
};
Promise.prototype._captureStackTrace = function () {};
Promise.prototype._attachExtraTrace = function () {};
Promise.prototype._clearCancellationData = function() {};
Promise.prototype._propagateFrom = function (parent, flags) {
    ;
    ;
};

function cancellationExecute(executor, resolve, reject) {
    var promise = this;
    try {
        executor(resolve, reject, function(onCancel) {
            if (typeof onCancel !== "function") {
                throw new TypeError("onCancel must be a function, got: " +
                                    util.toString(onCancel));
            }
            promise._attachCancellationCallback(onCancel);
        });
    } catch (e) {
        return e;
    }
}

function cancellationAttachCancellationCallback(onCancel) {
    if (!this.isCancellable()) return this;

    var previousOnCancel = this._onCancel();
    if (previousOnCancel !== undefined) {
        if (util.isArray(previousOnCancel)) {
            previousOnCancel.push(onCancel);
        } else {
            this._setOnCancel([previousOnCancel, onCancel]);
        }
    } else {
        this._setOnCancel(onCancel);
    }
}

function cancellationOnCancel() {
    return this._onCancelField;
}

function cancellationSetOnCancel(onCancel) {
    this._onCancelField = onCancel;
}

function cancellationClearCancellationData() {
    this._cancellationParent = undefined;
    this._onCancelField = undefined;
}

function cancellationPropagateFrom(parent, flags) {
    if ((flags & 1) !== 0) {
        this._cancellationParent = parent;
        var branchesRemainingToCancel = parent._branchesRemainingToCancel;
        if (branchesRemainingToCancel === undefined) {
            branchesRemainingToCancel = 0;
        }
        parent._branchesRemainingToCancel = branchesRemainingToCancel + 1;
    }
    if ((flags & 2) !== 0 && parent._isBound()) {
        this._setBoundTo(parent._boundTo);
    }
}

function bindingPropagateFrom(parent, flags) {
    if ((flags & 2) !== 0 && parent._isBound()) {
        this._setBoundTo(parent._boundTo);
    }
}
var propagateFromFunction = bindingPropagateFrom;

function boundValueFunction() {
    var ret = this._boundTo;
    if (ret !== undefined) {
        if (ret instanceof Promise) {
            if (ret.isFulfilled()) {
                return ret.value();
            } else {
                return undefined;
            }
        }
    }
    return ret;
}

function longStackTracesCaptureStackTrace() {
    this._trace = new CapturedTrace(this._peekContext());
}

function longStackTracesAttachExtraTrace(error, ignoreSelf) {
    if (canAttachTrace(error)) {
        var trace = this._trace;
        if (trace !== undefined) {
            if (ignoreSelf) trace = trace._parent;
        }
        if (trace !== undefined) {
            trace.attachExtraTrace(error);
        } else if (!error.__stackCleaned__) {
            var parsed = parseStackAndMessage(error);
            util.notEnumerableProp(error, "stack",
                parsed.message + "\n" + parsed.stack.join("\n"));
            util.notEnumerableProp(error, "__stackCleaned__", true);
        }
    }
}

function checkForgottenReturns(returnValue, promiseCreated, name, promise,
                               parent) {
    if (returnValue === undefined && promiseCreated !== null &&
        wForgottenReturn) {
        if (parent !== undefined && parent._returnedNonUndefined()) return;
        if ((promise._bitField & 65535) === 0) return;

        if (name) name = name + " ";
        var msg = "a promise was created in a " + name +
            "handler but was not returned from it";
        promise._warn(msg, true, promiseCreated);
    }
}

function deprecated(name, replacement) {
    var message = name +
        " is deprecated and will be removed in a future version.";
    if (replacement) message += " Use " + replacement + " instead.";
    return warn(message);
}

function warn(message, shouldUseOwnTrace, promise) {
    if (!config.warnings) return;
    var warning = new Warning(message);
    var ctx;
    if (shouldUseOwnTrace) {
        promise._attachExtraTrace(warning);
    } else if (config.longStackTraces && (ctx = Promise._peekContext())) {
        ctx.attachExtraTrace(warning);
    } else {
        var parsed = parseStackAndMessage(warning);
        warning.stack = parsed.message + "\n" + parsed.stack.join("\n");
    }

    if (!activeFireEvent("warning", warning)) {
        formatAndLogError(warning, "", true);
    }
}

function reconstructStack(message, stacks) {
    for (var i = 0; i < stacks.length - 1; ++i) {
        stacks[i].push("From previous event:");
        stacks[i] = stacks[i].join("\n");
    }
    if (i < stacks.length) {
        stacks[i] = stacks[i].join("\n");
    }
    return message + "\n" + stacks.join("\n");
}

function removeDuplicateOrEmptyJumps(stacks) {
    for (var i = 0; i < stacks.length; ++i) {
        if (stacks[i].length === 0 ||
            ((i + 1 < stacks.length) && stacks[i][0] === stacks[i+1][0])) {
            stacks.splice(i, 1);
            i--;
        }
    }
}

function removeCommonRoots(stacks) {
    var current = stacks[0];
    for (var i = 1; i < stacks.length; ++i) {
        var prev = stacks[i];
        var currentLastIndex = current.length - 1;
        var currentLastLine = current[currentLastIndex];
        var commonRootMeetPoint = -1;

        for (var j = prev.length - 1; j >= 0; --j) {
            if (prev[j] === currentLastLine) {
                commonRootMeetPoint = j;
                break;
            }
        }

        for (var j = commonRootMeetPoint; j >= 0; --j) {
            var line = prev[j];
            if (current[currentLastIndex] === line) {
                current.pop();
                currentLastIndex--;
            } else {
                break;
            }
        }
        current = prev;
    }
}

function cleanStack(stack) {
    var ret = [];
    for (var i = 0; i < stack.length; ++i) {
        var line = stack[i];
        var isTraceLine = "    (No stack trace)" === line ||
            stackFramePattern.test(line);
        var isInternalFrame = isTraceLine && shouldIgnore(line);
        if (isTraceLine && !isInternalFrame) {
            if (indentStackFrames && line.charAt(0) !== " ") {
                line = "    " + line;
            }
            ret.push(line);
        }
    }
    return ret;
}

function stackFramesAsArray(error) {
    var stack = error.stack.replace(/\s+$/g, "").split("\n");
    for (var i = 0; i < stack.length; ++i) {
        var line = stack[i];
        if ("    (No stack trace)" === line || stackFramePattern.test(line)) {
            break;
        }
    }
    if (i > 0) {
        stack = stack.slice(i);
    }
    return stack;
}

function parseStackAndMessage(error) {
    var stack = error.stack;
    var message = error.toString();
    stack = typeof stack === "string" && stack.length > 0
                ? stackFramesAsArray(error) : ["    (No stack trace)"];
    return {
        message: message,
        stack: cleanStack(stack)
    };
}

function formatAndLogError(error, title, isSoft) {
    if (typeof console !== "undefined") {
        var message;
        if (util.isObject(error)) {
            var stack = error.stack;
            message = title + formatStack(stack, error);
        } else {
            message = title + String(error);
        }
        if (typeof printWarning === "function") {
            printWarning(message, isSoft);
        } else if (typeof console.log === "function" ||
            typeof console.log === "object") {
            console.log(message);
        }
    }
}

function fireRejectionEvent(name, localHandler, reason, promise) {
    var localEventFired = false;
    try {
        if (typeof localHandler === "function") {
            localEventFired = true;
            if (name === "rejectionHandled") {
                localHandler(promise);
            } else {
                localHandler(reason, promise);
            }
        }
    } catch (e) {
        async.throwLater(e);
    }

    if (name === "unhandledRejection") {
        if (!activeFireEvent(name, reason, promise) && !localEventFired) {
            formatAndLogError(reason, "Unhandled rejection ");
        }
    } else {
        activeFireEvent(name, promise);
    }
}

function formatNonError(obj) {
    var str;
    if (typeof obj === "function") {
        str = "[function " +
            (obj.name || "anonymous") +
            "]";
    } else {
        str = obj && typeof obj.toString === "function"
            ? obj.toString() : util.toString(obj);
        var ruselessToString = /\[object [a-zA-Z0-9$_]+\]/;
        if (ruselessToString.test(str)) {
            try {
                var newStr = JSON.stringify(obj);
                str = newStr;
            }
            catch(e) {

            }
        }
        if (str.length === 0) {
            str = "(empty array)";
        }
    }
    return ("(<" + snip(str) + ">, no stack trace)");
}

function snip(str) {
    var maxChars = 41;
    if (str.length < maxChars) {
        return str;
    }
    return str.substr(0, maxChars - 3) + "...";
}

function longStackTracesIsSupported() {
    return typeof captureStackTrace === "function";
}

var shouldIgnore = function() { return false; };
var parseLineInfoRegex = /[\/<\(]([^:\/]+):(\d+):(?:\d+)\)?\s*$/;
function parseLineInfo(line) {
    var matches = line.match(parseLineInfoRegex);
    if (matches) {
        return {
            fileName: matches[1],
            line: parseInt(matches[2], 10)
        };
    }
}

function setBounds(firstLineError, lastLineError) {
    if (!longStackTracesIsSupported()) return;
    var firstStackLines = firstLineError.stack.split("\n");
    var lastStackLines = lastLineError.stack.split("\n");
    var firstIndex = -1;
    var lastIndex = -1;
    var firstFileName;
    var lastFileName;
    for (var i = 0; i < firstStackLines.length; ++i) {
        var result = parseLineInfo(firstStackLines[i]);
        if (result) {
            firstFileName = result.fileName;
            firstIndex = result.line;
            break;
        }
    }
    for (var i = 0; i < lastStackLines.length; ++i) {
        var result = parseLineInfo(lastStackLines[i]);
        if (result) {
            lastFileName = result.fileName;
            lastIndex = result.line;
            break;
        }
    }
    if (firstIndex < 0 || lastIndex < 0 || !firstFileName || !lastFileName ||
        firstFileName !== lastFileName || firstIndex >= lastIndex) {
        return;
    }

    shouldIgnore = function(line) {
        if (bluebirdFramePattern.test(line)) return true;
        var info = parseLineInfo(line);
        if (info) {
            if (info.fileName === firstFileName &&
                (firstIndex <= info.line && info.line <= lastIndex)) {
                return true;
            }
        }
        return false;
    };
}

function CapturedTrace(parent) {
    this._parent = parent;
    this._promisesCreated = 0;
    var length = this._length = 1 + (parent === undefined ? 0 : parent._length);
    captureStackTrace(this, CapturedTrace);
    if (length > 32) this.uncycle();
}
util.inherits(CapturedTrace, Error);
Context.CapturedTrace = CapturedTrace;

CapturedTrace.prototype.uncycle = function() {
    var length = this._length;
    if (length < 2) return;
    var nodes = [];
    var stackToIndex = {};

    for (var i = 0, node = this; node !== undefined; ++i) {
        nodes.push(node);
        node = node._parent;
    }
    length = this._length = i;
    for (var i = length - 1; i >= 0; --i) {
        var stack = nodes[i].stack;
        if (stackToIndex[stack] === undefined) {
            stackToIndex[stack] = i;
        }
    }
    for (var i = 0; i < length; ++i) {
        var currentStack = nodes[i].stack;
        var index = stackToIndex[currentStack];
        if (index !== undefined && index !== i) {
            if (index > 0) {
                nodes[index - 1]._parent = undefined;
                nodes[index - 1]._length = 1;
            }
            nodes[i]._parent = undefined;
            nodes[i]._length = 1;
            var cycleEdgeNode = i > 0 ? nodes[i - 1] : this;

            if (index < length - 1) {
                cycleEdgeNode._parent = nodes[index + 1];
                cycleEdgeNode._parent.uncycle();
                cycleEdgeNode._length =
                    cycleEdgeNode._parent._length + 1;
            } else {
                cycleEdgeNode._parent = undefined;
                cycleEdgeNode._length = 1;
            }
            var currentChildLength = cycleEdgeNode._length + 1;
            for (var j = i - 2; j >= 0; --j) {
                nodes[j]._length = currentChildLength;
                currentChildLength++;
            }
            return;
        }
    }
};

CapturedTrace.prototype.attachExtraTrace = function(error) {
    if (error.__stackCleaned__) return;
    this.uncycle();
    var parsed = parseStackAndMessage(error);
    var message = parsed.message;
    var stacks = [parsed.stack];

    var trace = this;
    while (trace !== undefined) {
        stacks.push(cleanStack(trace.stack.split("\n")));
        trace = trace._parent;
    }
    removeCommonRoots(stacks);
    removeDuplicateOrEmptyJumps(stacks);
    util.notEnumerableProp(error, "stack", reconstructStack(message, stacks));
    util.notEnumerableProp(error, "__stackCleaned__", true);
};

var captureStackTrace = (function stackDetection() {
    var v8stackFramePattern = /^\s*at\s*/;
    var v8stackFormatter = function(stack, error) {
        if (typeof stack === "string") return stack;

        if (error.name !== undefined &&
            error.message !== undefined) {
            return error.toString();
        }
        return formatNonError(error);
    };

    if (typeof Error.stackTraceLimit === "number" &&
        typeof Error.captureStackTrace === "function") {
        Error.stackTraceLimit += 6;
        stackFramePattern = v8stackFramePattern;
        formatStack = v8stackFormatter;
        var captureStackTrace = Error.captureStackTrace;

        shouldIgnore = function(line) {
            return bluebirdFramePattern.test(line);
        };
        return function(receiver, ignoreUntil) {
            Error.stackTraceLimit += 6;
            captureStackTrace(receiver, ignoreUntil);
            Error.stackTraceLimit -= 6;
        };
    }
    var err = new Error();

    if (typeof err.stack === "string" &&
        err.stack.split("\n")[0].indexOf("stackDetection@") >= 0) {
        stackFramePattern = /@/;
        formatStack = v8stackFormatter;
        indentStackFrames = true;
        return function captureStackTrace(o) {
            o.stack = new Error().stack;
        };
    }

    var hasStackAfterThrow;
    try { throw new Error(); }
    catch(e) {
        hasStackAfterThrow = ("stack" in e);
    }
    if (!("stack" in err) && hasStackAfterThrow &&
        typeof Error.stackTraceLimit === "number") {
        stackFramePattern = v8stackFramePattern;
        formatStack = v8stackFormatter;
        return function captureStackTrace(o) {
            Error.stackTraceLimit += 6;
            try { throw new Error(); }
            catch(e) { o.stack = e.stack; }
            Error.stackTraceLimit -= 6;
        };
    }

    formatStack = function(stack, error) {
        if (typeof stack === "string") return stack;

        if ((typeof error === "object" ||
            typeof error === "function") &&
            error.name !== undefined &&
            error.message !== undefined) {
            return error.toString();
        }
        return formatNonError(error);
    };

    return null;

})([]);

if (typeof console !== "undefined" && typeof console.warn !== "undefined") {
    printWarning = function (message) {
        console.warn(message);
    };
    if (util.isNode && process.stderr.isTTY) {
        printWarning = function(message, isSoft) {
            var color = isSoft ? "\u001b[33m" : "\u001b[31m";
            console.warn(color + message + "\u001b[0m\n");
        };
    } else if (!util.isNode && typeof (new Error().stack) === "string") {
        printWarning = function(message, isSoft) {
            console.warn("%c" + message,
                        isSoft ? "color: darkorange" : "color: red");
        };
    }
}

var config = {
    warnings: warnings,
    longStackTraces: false,
    cancellation: false,
    monitoring: false
};

if (longStackTraces) Promise.longStackTraces();

return {
    longStackTraces: function() {
        return config.longStackTraces;
    },
    warnings: function() {
        return config.warnings;
    },
    cancellation: function() {
        return config.cancellation;
    },
    monitoring: function() {
        return config.monitoring;
    },
    propagateFromFunction: function() {
        return propagateFromFunction;
    },
    boundValueFunction: function() {
        return boundValueFunction;
    },
    checkForgottenReturns: checkForgottenReturns,
    setBounds: setBounds,
    warn: warn,
    deprecated: deprecated,
    CapturedTrace: CapturedTrace,
    fireDomEvent: fireDomEvent,
    fireGlobalEvent: fireGlobalEvent
};
};

},{"./errors":12,"./util":36}],10:[function(_dereq_,module,exports){
"use strict";
module.exports = function(Promise) {
function returner() {
    return this.value;
}
function thrower() {
    throw this.reason;
}

Promise.prototype["return"] =
Promise.prototype.thenReturn = function (value) {
    if (value instanceof Promise) value.suppressUnhandledRejections();
    return this._then(
        returner, undefined, undefined, {value: value}, undefined);
};

Promise.prototype["throw"] =
Promise.prototype.thenThrow = function (reason) {
    return this._then(
        thrower, undefined, undefined, {reason: reason}, undefined);
};

Promise.prototype.catchThrow = function (reason) {
    if (arguments.length <= 1) {
        return this._then(
            undefined, thrower, undefined, {reason: reason}, undefined);
    } else {
        var _reason = arguments[1];
        var handler = function() {throw _reason;};
        return this.caught(reason, handler);
    }
};

Promise.prototype.catchReturn = function (value) {
    if (arguments.length <= 1) {
        if (value instanceof Promise) value.suppressUnhandledRejections();
        return this._then(
            undefined, returner, undefined, {value: value}, undefined);
    } else {
        var _value = arguments[1];
        if (_value instanceof Promise) _value.suppressUnhandledRejections();
        var handler = function() {return _value;};
        return this.caught(value, handler);
    }
};
};

},{}],11:[function(_dereq_,module,exports){
"use strict";
module.exports = function(Promise, INTERNAL) {
var PromiseReduce = Promise.reduce;
var PromiseAll = Promise.all;

function promiseAllThis() {
    return PromiseAll(this);
}

function PromiseMapSeries(promises, fn) {
    return PromiseReduce(promises, fn, INTERNAL, INTERNAL);
}

Promise.prototype.each = function (fn) {
    return this.mapSeries(fn)
            ._then(promiseAllThis, undefined, undefined, this, undefined);
};

Promise.prototype.mapSeries = function (fn) {
    return PromiseReduce(this, fn, INTERNAL, INTERNAL);
};

Promise.each = function (promises, fn) {
    return PromiseMapSeries(promises, fn)
            ._then(promiseAllThis, undefined, undefined, promises, undefined);
};

Promise.mapSeries = PromiseMapSeries;
};

},{}],12:[function(_dereq_,module,exports){
"use strict";
var es5 = _dereq_("./es5");
var Objectfreeze = es5.freeze;
var util = _dereq_("./util");
var inherits = util.inherits;
var notEnumerableProp = util.notEnumerableProp;

function subError(nameProperty, defaultMessage) {
    function SubError(message) {
        if (!(this instanceof SubError)) return new SubError(message);
        notEnumerableProp(this, "message",
            typeof message === "string" ? message : defaultMessage);
        notEnumerableProp(this, "name", nameProperty);
        if (Error.captureStackTrace) {
            Error.captureStackTrace(this, this.constructor);
        } else {
            Error.call(this);
        }
    }
    inherits(SubError, Error);
    return SubError;
}

var _TypeError, _RangeError;
var Warning = subError("Warning", "warning");
var CancellationError = subError("CancellationError", "cancellation error");
var TimeoutError = subError("TimeoutError", "timeout error");
var AggregateError = subError("AggregateError", "aggregate error");
try {
    _TypeError = TypeError;
    _RangeError = RangeError;
} catch(e) {
    _TypeError = subError("TypeError", "type error");
    _RangeError = subError("RangeError", "range error");
}

var methods = ("join pop push shift unshift slice filter forEach some " +
    "every map indexOf lastIndexOf reduce reduceRight sort reverse").split(" ");

for (var i = 0; i < methods.length; ++i) {
    if (typeof Array.prototype[methods[i]] === "function") {
        AggregateError.prototype[methods[i]] = Array.prototype[methods[i]];
    }
}

es5.defineProperty(AggregateError.prototype, "length", {
    value: 0,
    configurable: false,
    writable: true,
    enumerable: true
});
AggregateError.prototype["isOperational"] = true;
var level = 0;
AggregateError.prototype.toString = function() {
    var indent = Array(level * 4 + 1).join(" ");
    var ret = "\n" + indent + "AggregateError of:" + "\n";
    level++;
    indent = Array(level * 4 + 1).join(" ");
    for (var i = 0; i < this.length; ++i) {
        var str = this[i] === this ? "[Circular AggregateError]" : this[i] + "";
        var lines = str.split("\n");
        for (var j = 0; j < lines.length; ++j) {
            lines[j] = indent + lines[j];
        }
        str = lines.join("\n");
        ret += str + "\n";
    }
    level--;
    return ret;
};

function OperationalError(message) {
    if (!(this instanceof OperationalError))
        return new OperationalError(message);
    notEnumerableProp(this, "name", "OperationalError");
    notEnumerableProp(this, "message", message);
    this.cause = message;
    this["isOperational"] = true;

    if (message instanceof Error) {
        notEnumerableProp(this, "message", message.message);
        notEnumerableProp(this, "stack", message.stack);
    } else if (Error.captureStackTrace) {
        Error.captureStackTrace(this, this.constructor);
    }

}
inherits(OperationalError, Error);

var errorTypes = Error["__BluebirdErrorTypes__"];
if (!errorTypes) {
    errorTypes = Objectfreeze({
        CancellationError: CancellationError,
        TimeoutError: TimeoutError,
        OperationalError: OperationalError,
        RejectionError: OperationalError,
        AggregateError: AggregateError
    });
    es5.defineProperty(Error, "__BluebirdErrorTypes__", {
        value: errorTypes,
        writable: false,
        enumerable: false,
        configurable: false
    });
}

module.exports = {
    Error: Error,
    TypeError: _TypeError,
    RangeError: _RangeError,
    CancellationError: errorTypes.CancellationError,
    OperationalError: errorTypes.OperationalError,
    TimeoutError: errorTypes.TimeoutError,
    AggregateError: errorTypes.AggregateError,
    Warning: Warning
};

},{"./es5":13,"./util":36}],13:[function(_dereq_,module,exports){
var isES5 = (function(){
    "use strict";
    return this === undefined;
})();

if (isES5) {
    module.exports = {
        freeze: Object.freeze,
        defineProperty: Object.defineProperty,
        getDescriptor: Object.getOwnPropertyDescriptor,
        keys: Object.keys,
        names: Object.getOwnPropertyNames,
        getPrototypeOf: Object.getPrototypeOf,
        isArray: Array.isArray,
        isES5: isES5,
        propertyIsWritable: function(obj, prop) {
            var descriptor = Object.getOwnPropertyDescriptor(obj, prop);
            return !!(!descriptor || descriptor.writable || descriptor.set);
        }
    };
} else {
    var has = {}.hasOwnProperty;
    var str = {}.toString;
    var proto = {}.constructor.prototype;

    var ObjectKeys = function (o) {
        var ret = [];
        for (var key in o) {
            if (has.call(o, key)) {
                ret.push(key);
            }
        }
        return ret;
    };

    var ObjectGetDescriptor = function(o, key) {
        return {value: o[key]};
    };

    var ObjectDefineProperty = function (o, key, desc) {
        o[key] = desc.value;
        return o;
    };

    var ObjectFreeze = function (obj) {
        return obj;
    };

    var ObjectGetPrototypeOf = function (obj) {
        try {
            return Object(obj).constructor.prototype;
        }
        catch (e) {
            return proto;
        }
    };

    var ArrayIsArray = function (obj) {
        try {
            return str.call(obj) === "[object Array]";
        }
        catch(e) {
            return false;
        }
    };

    module.exports = {
        isArray: ArrayIsArray,
        keys: ObjectKeys,
        names: ObjectKeys,
        defineProperty: ObjectDefineProperty,
        getDescriptor: ObjectGetDescriptor,
        freeze: ObjectFreeze,
        getPrototypeOf: ObjectGetPrototypeOf,
        isES5: isES5,
        propertyIsWritable: function() {
            return true;
        }
    };
}

},{}],14:[function(_dereq_,module,exports){
"use strict";
module.exports = function(Promise, INTERNAL) {
var PromiseMap = Promise.map;

Promise.prototype.filter = function (fn, options) {
    return PromiseMap(this, fn, options, INTERNAL);
};

Promise.filter = function (promises, fn, options) {
    return PromiseMap(promises, fn, options, INTERNAL);
};
};

},{}],15:[function(_dereq_,module,exports){
"use strict";
module.exports = function(Promise, tryConvertToPromise) {
var util = _dereq_("./util");
var CancellationError = Promise.CancellationError;
var errorObj = util.errorObj;

function PassThroughHandlerContext(promise, type, handler) {
    this.promise = promise;
    this.type = type;
    this.handler = handler;
    this.called = false;
    this.cancelPromise = null;
}

PassThroughHandlerContext.prototype.isFinallyHandler = function() {
    return this.type === 0;
};

function FinallyHandlerCancelReaction(finallyHandler) {
    this.finallyHandler = finallyHandler;
}

FinallyHandlerCancelReaction.prototype._resultCancelled = function() {
    checkCancel(this.finallyHandler);
};

function checkCancel(ctx, reason) {
    if (ctx.cancelPromise != null) {
        if (arguments.length > 1) {
            ctx.cancelPromise._reject(reason);
        } else {
            ctx.cancelPromise._cancel();
        }
        ctx.cancelPromise = null;
        return true;
    }
    return false;
}

function succeed() {
    return finallyHandler.call(this, this.promise._target()._settledValue());
}
function fail(reason) {
    if (checkCancel(this, reason)) return;
    errorObj.e = reason;
    return errorObj;
}
function finallyHandler(reasonOrValue) {
    var promise = this.promise;
    var handler = this.handler;

    if (!this.called) {
        this.called = true;
        var ret = this.isFinallyHandler()
            ? handler.call(promise._boundValue())
            : handler.call(promise._boundValue(), reasonOrValue);
        if (ret !== undefined) {
            promise._setReturnedNonUndefined();
            var maybePromise = tryConvertToPromise(ret, promise);
            if (maybePromise instanceof Promise) {
                if (this.cancelPromise != null) {
                    if (maybePromise.isCancelled()) {
                        var reason =
                            new CancellationError("late cancellation observer");
                        promise._attachExtraTrace(reason);
                        errorObj.e = reason;
                        return errorObj;
                    } else if (maybePromise.isPending()) {
                        maybePromise._attachCancellationCallback(
                            new FinallyHandlerCancelReaction(this));
                    }
                }
                return maybePromise._then(
                    succeed, fail, undefined, this, undefined);
            }
        }
    }

    if (promise.isRejected()) {
        checkCancel(this);
        errorObj.e = reasonOrValue;
        return errorObj;
    } else {
        checkCancel(this);
        return reasonOrValue;
    }
}

Promise.prototype._passThrough = function(handler, type, success, fail) {
    if (typeof handler !== "function") return this.then();
    return this._then(success,
                      fail,
                      undefined,
                      new PassThroughHandlerContext(this, type, handler),
                      undefined);
};

Promise.prototype.lastly =
Promise.prototype["finally"] = function (handler) {
    return this._passThrough(handler,
                             0,
                             finallyHandler,
                             finallyHandler);
};

Promise.prototype.tap = function (handler) {
    return this._passThrough(handler, 1, finallyHandler);
};

return PassThroughHandlerContext;
};

},{"./util":36}],16:[function(_dereq_,module,exports){
"use strict";
module.exports = function(Promise,
                          apiRejection,
                          INTERNAL,
                          tryConvertToPromise,
                          Proxyable,
                          debug) {
var errors = _dereq_("./errors");
var TypeError = errors.TypeError;
var util = _dereq_("./util");
var errorObj = util.errorObj;
var tryCatch = util.tryCatch;
var yieldHandlers = [];

function promiseFromYieldHandler(value, yieldHandlers, traceParent) {
    for (var i = 0; i < yieldHandlers.length; ++i) {
        traceParent._pushContext();
        var result = tryCatch(yieldHandlers[i])(value);
        traceParent._popContext();
        if (result === errorObj) {
            traceParent._pushContext();
            var ret = Promise.reject(errorObj.e);
            traceParent._popContext();
            return ret;
        }
        var maybePromise = tryConvertToPromise(result, traceParent);
        if (maybePromise instanceof Promise) return maybePromise;
    }
    return null;
}

function PromiseSpawn(generatorFunction, receiver, yieldHandler, stack) {
    if (debug.cancellation()) {
        var internal = new Promise(INTERNAL);
        var _finallyPromise = this._finallyPromise = new Promise(INTERNAL);
        this._promise = internal.lastly(function() {
            return _finallyPromise;
        });
        internal._captureStackTrace();
        internal._setOnCancel(this);
    } else {
        var promise = this._promise = new Promise(INTERNAL);
        promise._captureStackTrace();
    }
    this._stack = stack;
    this._generatorFunction = generatorFunction;
    this._receiver = receiver;
    this._generator = undefined;
    this._yieldHandlers = typeof yieldHandler === "function"
        ? [yieldHandler].concat(yieldHandlers)
        : yieldHandlers;
    this._yieldedPromise = null;
    this._cancellationPhase = false;
}
util.inherits(PromiseSpawn, Proxyable);

PromiseSpawn.prototype._isResolved = function() {
    return this._promise === null;
};

PromiseSpawn.prototype._cleanup = function() {
    this._promise = this._generator = null;
    if (debug.cancellation() && this._finallyPromise !== null) {
        this._finallyPromise._fulfill();
        this._finallyPromise = null;
    }
};

PromiseSpawn.prototype._promiseCancelled = function() {
    if (this._isResolved()) return;
    var implementsReturn = typeof this._generator["return"] !== "undefined";

    var result;
    if (!implementsReturn) {
        var reason = new Promise.CancellationError(
            "generator .return() sentinel");
        Promise.coroutine.returnSentinel = reason;
        this._promise._attachExtraTrace(reason);
        this._promise._pushContext();
        result = tryCatch(this._generator["throw"]).call(this._generator,
                                                         reason);
        this._promise._popContext();
    } else {
        this._promise._pushContext();
        result = tryCatch(this._generator["return"]).call(this._generator,
                                                          undefined);
        this._promise._popContext();
    }
    this._cancellationPhase = true;
    this._yieldedPromise = null;
    this._continue(result);
};

PromiseSpawn.prototype._promiseFulfilled = function(value) {
    this._yieldedPromise = null;
    this._promise._pushContext();
    var result = tryCatch(this._generator.next).call(this._generator, value);
    this._promise._popContext();
    this._continue(result);
};

PromiseSpawn.prototype._promiseRejected = function(reason) {
    this._yieldedPromise = null;
    this._promise._attachExtraTrace(reason);
    this._promise._pushContext();
    var result = tryCatch(this._generator["throw"])
        .call(this._generator, reason);
    this._promise._popContext();
    this._continue(result);
};

PromiseSpawn.prototype._resultCancelled = function() {
    if (this._yieldedPromise instanceof Promise) {
        var promise = this._yieldedPromise;
        this._yieldedPromise = null;
        promise.cancel();
    }
};

PromiseSpawn.prototype.promise = function () {
    return this._promise;
};

PromiseSpawn.prototype._run = function () {
    this._generator = this._generatorFunction.call(this._receiver);
    this._receiver =
        this._generatorFunction = undefined;
    this._promiseFulfilled(undefined);
};

PromiseSpawn.prototype._continue = function (result) {
    var promise = this._promise;
    if (result === errorObj) {
        this._cleanup();
        if (this._cancellationPhase) {
            return promise.cancel();
        } else {
            return promise._rejectCallback(result.e, false);
        }
    }

    var value = result.value;
    if (result.done === true) {
        this._cleanup();
        if (this._cancellationPhase) {
            return promise.cancel();
        } else {
            return promise._resolveCallback(value);
        }
    } else {
        var maybePromise = tryConvertToPromise(value, this._promise);
        if (!(maybePromise instanceof Promise)) {
            maybePromise =
                promiseFromYieldHandler(maybePromise,
                                        this._yieldHandlers,
                                        this._promise);
            if (maybePromise === null) {
                this._promiseRejected(
                    new TypeError(
                        "A value %s was yielded that could not be treated as a promise\u000a\u000a    See http://goo.gl/MqrFmX\u000a\u000a".replace("%s", value) +
                        "From coroutine:\u000a" +
                        this._stack.split("\n").slice(1, -7).join("\n")
                    )
                );
                return;
            }
        }
        maybePromise = maybePromise._target();
        var bitField = maybePromise._bitField;
        ;
        if (((bitField & 50397184) === 0)) {
            this._yieldedPromise = maybePromise;
            maybePromise._proxy(this, null);
        } else if (((bitField & 33554432) !== 0)) {
            this._promiseFulfilled(maybePromise._value());
        } else if (((bitField & 16777216) !== 0)) {
            this._promiseRejected(maybePromise._reason());
        } else {
            this._promiseCancelled();
        }
    }
};

Promise.coroutine = function (generatorFunction, options) {
    if (typeof generatorFunction !== "function") {
        throw new TypeError("generatorFunction must be a function\u000a\u000a    See http://goo.gl/MqrFmX\u000a");
    }
    var yieldHandler = Object(options).yieldHandler;
    var PromiseSpawn$ = PromiseSpawn;
    var stack = new Error().stack;
    return function () {
        var generator = generatorFunction.apply(this, arguments);
        var spawn = new PromiseSpawn$(undefined, undefined, yieldHandler,
                                      stack);
        var ret = spawn.promise();
        spawn._generator = generator;
        spawn._promiseFulfilled(undefined);
        return ret;
    };
};

Promise.coroutine.addYieldHandler = function(fn) {
    if (typeof fn !== "function") {
        throw new TypeError("expecting a function but got " + util.classString(fn));
    }
    yieldHandlers.push(fn);
};

Promise.spawn = function (generatorFunction) {
    debug.deprecated("Promise.spawn()", "Promise.coroutine()");
    if (typeof generatorFunction !== "function") {
        return apiRejection("generatorFunction must be a function\u000a\u000a    See http://goo.gl/MqrFmX\u000a");
    }
    var spawn = new PromiseSpawn(generatorFunction, this);
    var ret = spawn.promise();
    spawn._run(Promise.spawn);
    return ret;
};
};

},{"./errors":12,"./util":36}],17:[function(_dereq_,module,exports){
"use strict";
module.exports =
function(Promise, PromiseArray, tryConvertToPromise, INTERNAL) {
var util = _dereq_("./util");
var canEvaluate = util.canEvaluate;
var tryCatch = util.tryCatch;
var errorObj = util.errorObj;
var reject;

if (!true) {
if (canEvaluate) {
    var thenCallback = function(i) {
        return new Function("value", "holder", "                             \n\
            'use strict';                                                    \n\
            holder.pIndex = value;                                           \n\
            holder.checkFulfillment(this);                                   \n\
            ".replace(/Index/g, i));
    };

    var promiseSetter = function(i) {
        return new Function("promise", "holder", "                           \n\
            'use strict';                                                    \n\
            holder.pIndex = promise;                                         \n\
            ".replace(/Index/g, i));
    };

    var generateHolderClass = function(total) {
        var props = new Array(total);
        for (var i = 0; i < props.length; ++i) {
            props[i] = "this.p" + (i+1);
        }
        var assignment = props.join(" = ") + " = null;";
        var cancellationCode= "var promise;\n" + props.map(function(prop) {
            return "                                                         \n\
                promise = " + prop + ";                                      \n\
                if (promise instanceof Promise) {                            \n\
                    promise.cancel();                                        \n\
                }                                                            \n\
            ";
        }).join("\n");
        var passedArguments = props.join(", ");
        var name = "Holder$" + total;


        var code = "return function(tryCatch, errorObj, Promise) {           \n\
            'use strict';                                                    \n\
            function [TheName](fn) {                                         \n\
                [TheProperties]                                              \n\
                this.fn = fn;                                                \n\
                this.now = 0;                                                \n\
            }                                                                \n\
            [TheName].prototype.checkFulfillment = function(promise) {       \n\
                var now = ++this.now;                                        \n\
                if (now === [TheTotal]) {                                    \n\
                    promise._pushContext();                                  \n\
                    var callback = this.fn;                                  \n\
                    var ret = tryCatch(callback)([ThePassedArguments]);      \n\
                    promise._popContext();                                   \n\
                    if (ret === errorObj) {                                  \n\
                        promise._rejectCallback(ret.e, false);               \n\
                    } else {                                                 \n\
                        promise._resolveCallback(ret);                       \n\
                    }                                                        \n\
                }                                                            \n\
            };                                                               \n\
                                                                             \n\
            [TheName].prototype._resultCancelled = function() {              \n\
                [CancellationCode]                                           \n\
            };                                                               \n\
                                                                             \n\
            return [TheName];                                                \n\
        }(tryCatch, errorObj, Promise);                                      \n\
        ";

        code = code.replace(/\[TheName\]/g, name)
            .replace(/\[TheTotal\]/g, total)
            .replace(/\[ThePassedArguments\]/g, passedArguments)
            .replace(/\[TheProperties\]/g, assignment)
            .replace(/\[CancellationCode\]/g, cancellationCode);

        return new Function("tryCatch", "errorObj", "Promise", code)
                           (tryCatch, errorObj, Promise);
    };

    var holderClasses = [];
    var thenCallbacks = [];
    var promiseSetters = [];

    for (var i = 0; i < 8; ++i) {
        holderClasses.push(generateHolderClass(i + 1));
        thenCallbacks.push(thenCallback(i + 1));
        promiseSetters.push(promiseSetter(i + 1));
    }

    reject = function (reason) {
        this._reject(reason);
    };
}}

Promise.join = function () {
    var last = arguments.length - 1;
    var fn;
    if (last > 0 && typeof arguments[last] === "function") {
        fn = arguments[last];
        if (!true) {
            if (last <= 8 && canEvaluate) {
                var ret = new Promise(INTERNAL);
                ret._captureStackTrace();
                var HolderClass = holderClasses[last - 1];
                var holder = new HolderClass(fn);
                var callbacks = thenCallbacks;

                for (var i = 0; i < last; ++i) {
                    var maybePromise = tryConvertToPromise(arguments[i], ret);
                    if (maybePromise instanceof Promise) {
                        maybePromise = maybePromise._target();
                        var bitField = maybePromise._bitField;
                        ;
                        if (((bitField & 50397184) === 0)) {
                            maybePromise._then(callbacks[i], reject,
                                               undefined, ret, holder);
                            promiseSetters[i](maybePromise, holder);
                        } else if (((bitField & 33554432) !== 0)) {
                            callbacks[i].call(ret,
                                              maybePromise._value(), holder);
                        } else if (((bitField & 16777216) !== 0)) {
                            ret._reject(maybePromise._reason());
                        } else {
                            ret._cancel();
                        }
                    } else {
                        callbacks[i].call(ret, maybePromise, holder);
                    }
                }
                if (!ret._isFateSealed()) {
                    ret._setAsyncGuaranteed();
                    ret._setOnCancel(holder);
                }
                return ret;
            }
        }
    }
    var args = [].slice.call(arguments);;
    if (fn) args.pop();
    var ret = new PromiseArray(args).promise();
    return fn !== undefined ? ret.spread(fn) : ret;
};

};

},{"./util":36}],18:[function(_dereq_,module,exports){
"use strict";
module.exports = function(Promise,
                          PromiseArray,
                          apiRejection,
                          tryConvertToPromise,
                          INTERNAL,
                          debug) {
var getDomain = Promise._getDomain;
var util = _dereq_("./util");
var tryCatch = util.tryCatch;
var errorObj = util.errorObj;
var EMPTY_ARRAY = [];

function MappingPromiseArray(promises, fn, limit, _filter) {
    this.constructor$(promises);
    this._promise._captureStackTrace();
    var domain = getDomain();
    this._callback = domain === null ? fn : domain.bind(fn);
    this._preservedValues = _filter === INTERNAL
        ? new Array(this.length())
        : null;
    this._limit = limit;
    this._inFlight = 0;
    this._queue = limit >= 1 ? [] : EMPTY_ARRAY;
    this._init$(undefined, -2);
}
util.inherits(MappingPromiseArray, PromiseArray);

MappingPromiseArray.prototype._init = function () {};

MappingPromiseArray.prototype._promiseFulfilled = function (value, index) {
    var values = this._values;
    var length = this.length();
    var preservedValues = this._preservedValues;
    var limit = this._limit;

    if (index < 0) {
        index = (index * -1) - 1;
        values[index] = value;
        if (limit >= 1) {
            this._inFlight--;
            this._drainQueue();
            if (this._isResolved()) return true;
        }
    } else {
        if (limit >= 1 && this._inFlight >= limit) {
            values[index] = value;
            this._queue.push(index);
            return false;
        }
        if (preservedValues !== null) preservedValues[index] = value;

        var promise = this._promise;
        var callback = this._callback;
        var receiver = promise._boundValue();
        promise._pushContext();
        var ret = tryCatch(callback).call(receiver, value, index, length);
        var promiseCreated = promise._popContext();
        debug.checkForgottenReturns(
            ret,
            promiseCreated,
            preservedValues !== null ? "Promise.filter" : "Promise.map",
            promise
        );
        if (ret === errorObj) {
            this._reject(ret.e);
            return true;
        }

        var maybePromise = tryConvertToPromise(ret, this._promise);
        if (maybePromise instanceof Promise) {
            maybePromise = maybePromise._target();
            var bitField = maybePromise._bitField;
            ;
            if (((bitField & 50397184) === 0)) {
                if (limit >= 1) this._inFlight++;
                values[index] = maybePromise;
                maybePromise._proxy(this, (index + 1) * -1);
                return false;
            } else if (((bitField & 33554432) !== 0)) {
                ret = maybePromise._value();
            } else if (((bitField & 16777216) !== 0)) {
                this._reject(maybePromise._reason());
                return true;
            } else {
                this._cancel();
                return true;
            }
        }
        values[index] = ret;
    }
    var totalResolved = ++this._totalResolved;
    if (totalResolved >= length) {
        if (preservedValues !== null) {
            this._filter(values, preservedValues);
        } else {
            this._resolve(values);
        }
        return true;
    }
    return false;
};

MappingPromiseArray.prototype._drainQueue = function () {
    var queue = this._queue;
    var limit = this._limit;
    var values = this._values;
    while (queue.length > 0 && this._inFlight < limit) {
        if (this._isResolved()) return;
        var index = queue.pop();
        this._promiseFulfilled(values[index], index);
    }
};

MappingPromiseArray.prototype._filter = function (booleans, values) {
    var len = values.length;
    var ret = new Array(len);
    var j = 0;
    for (var i = 0; i < len; ++i) {
        if (booleans[i]) ret[j++] = values[i];
    }
    ret.length = j;
    this._resolve(ret);
};

MappingPromiseArray.prototype.preservedValues = function () {
    return this._preservedValues;
};

function map(promises, fn, options, _filter) {
    if (typeof fn !== "function") {
        return apiRejection("expecting a function but got " + util.classString(fn));
    }

    var limit = 0;
    if (options !== undefined) {
        if (typeof options === "object" && options !== null) {
            if (typeof options.concurrency !== "number") {
                return Promise.reject(
                    new TypeError("'concurrency' must be a number but it is " +
                                    util.classString(options.concurrency)));
            }
            limit = options.concurrency;
        } else {
            return Promise.reject(new TypeError(
                            "options argument must be an object but it is " +
                             util.classString(options)));
        }
    }
    limit = typeof limit === "number" &&
        isFinite(limit) && limit >= 1 ? limit : 0;
    return new MappingPromiseArray(promises, fn, limit, _filter).promise();
}

Promise.prototype.map = function (fn, options) {
    return map(this, fn, options, null);
};

Promise.map = function (promises, fn, options, _filter) {
    return map(promises, fn, options, _filter);
};


};

},{"./util":36}],19:[function(_dereq_,module,exports){
"use strict";
module.exports =
function(Promise, INTERNAL, tryConvertToPromise, apiRejection, debug) {
var util = _dereq_("./util");
var tryCatch = util.tryCatch;

Promise.method = function (fn) {
    if (typeof fn !== "function") {
        throw new Promise.TypeError("expecting a function but got " + util.classString(fn));
    }
    return function () {
        var ret = new Promise(INTERNAL);
        ret._captureStackTrace();
        ret._pushContext();
        var value = tryCatch(fn).apply(this, arguments);
        var promiseCreated = ret._popContext();
        debug.checkForgottenReturns(
            value, promiseCreated, "Promise.method", ret);
        ret._resolveFromSyncValue(value);
        return ret;
    };
};

Promise.attempt = Promise["try"] = function (fn) {
    if (typeof fn !== "function") {
        return apiRejection("expecting a function but got " + util.classString(fn));
    }
    var ret = new Promise(INTERNAL);
    ret._captureStackTrace();
    ret._pushContext();
    var value;
    if (arguments.length > 1) {
        debug.deprecated("calling Promise.try with more than 1 argument");
        var arg = arguments[1];
        var ctx = arguments[2];
        value = util.isArray(arg) ? tryCatch(fn).apply(ctx, arg)
                                  : tryCatch(fn).call(ctx, arg);
    } else {
        value = tryCatch(fn)();
    }
    var promiseCreated = ret._popContext();
    debug.checkForgottenReturns(
        value, promiseCreated, "Promise.try", ret);
    ret._resolveFromSyncValue(value);
    return ret;
};

Promise.prototype._resolveFromSyncValue = function (value) {
    if (value === util.errorObj) {
        this._rejectCallback(value.e, false);
    } else {
        this._resolveCallback(value, true);
    }
};
};

},{"./util":36}],20:[function(_dereq_,module,exports){
"use strict";
var util = _dereq_("./util");
var maybeWrapAsError = util.maybeWrapAsError;
var errors = _dereq_("./errors");
var OperationalError = errors.OperationalError;
var es5 = _dereq_("./es5");

function isUntypedError(obj) {
    return obj instanceof Error &&
        es5.getPrototypeOf(obj) === Error.prototype;
}

var rErrorKey = /^(?:name|message|stack|cause)$/;
function wrapAsOperationalError(obj) {
    var ret;
    if (isUntypedError(obj)) {
        ret = new OperationalError(obj);
        ret.name = obj.name;
        ret.message = obj.message;
        ret.stack = obj.stack;
        var keys = es5.keys(obj);
        for (var i = 0; i < keys.length; ++i) {
            var key = keys[i];
            if (!rErrorKey.test(key)) {
                ret[key] = obj[key];
            }
        }
        return ret;
    }
    util.markAsOriginatingFromRejection(obj);
    return obj;
}

function nodebackForPromise(promise, multiArgs) {
    return function(err, value) {
        if (promise === null) return;
        if (err) {
            var wrapped = wrapAsOperationalError(maybeWrapAsError(err));
            promise._attachExtraTrace(wrapped);
            promise._reject(wrapped);
        } else if (!multiArgs) {
            promise._fulfill(value);
        } else {
            var args = [].slice.call(arguments, 1);;
            promise._fulfill(args);
        }
        promise = null;
    };
}

module.exports = nodebackForPromise;

},{"./errors":12,"./es5":13,"./util":36}],21:[function(_dereq_,module,exports){
"use strict";
module.exports = function(Promise) {
var util = _dereq_("./util");
var async = Promise._async;
var tryCatch = util.tryCatch;
var errorObj = util.errorObj;

function spreadAdapter(val, nodeback) {
    var promise = this;
    if (!util.isArray(val)) return successAdapter.call(promise, val, nodeback);
    var ret =
        tryCatch(nodeback).apply(promise._boundValue(), [null].concat(val));
    if (ret === errorObj) {
        async.throwLater(ret.e);
    }
}

function successAdapter(val, nodeback) {
    var promise = this;
    var receiver = promise._boundValue();
    var ret = val === undefined
        ? tryCatch(nodeback).call(receiver, null)
        : tryCatch(nodeback).call(receiver, null, val);
    if (ret === errorObj) {
        async.throwLater(ret.e);
    }
}
function errorAdapter(reason, nodeback) {
    var promise = this;
    if (!reason) {
        var newReason = new Error(reason + "");
        newReason.cause = reason;
        reason = newReason;
    }
    var ret = tryCatch(nodeback).call(promise._boundValue(), reason);
    if (ret === errorObj) {
        async.throwLater(ret.e);
    }
}

Promise.prototype.asCallback = Promise.prototype.nodeify = function (nodeback,
                                                                     options) {
    if (typeof nodeback == "function") {
        var adapter = successAdapter;
        if (options !== undefined && Object(options).spread) {
            adapter = spreadAdapter;
        }
        this._then(
            adapter,
            errorAdapter,
            undefined,
            this,
            nodeback
        );
    }
    return this;
};
};

},{"./util":36}],22:[function(_dereq_,module,exports){
"use strict";
module.exports = function() {
var makeSelfResolutionError = function () {
    return new TypeError("circular promise resolution chain\u000a\u000a    See http://goo.gl/MqrFmX\u000a");
};
var reflectHandler = function() {
    return new Promise.PromiseInspection(this._target());
};
var apiRejection = function(msg) {
    return Promise.reject(new TypeError(msg));
};
function Proxyable() {}
var UNDEFINED_BINDING = {};
var util = _dereq_("./util");

var getDomain;
if (util.isNode) {
    getDomain = function() {
        var ret = process.domain;
        if (ret === undefined) ret = null;
        return ret;
    };
} else {
    getDomain = function() {
        return null;
    };
}
util.notEnumerableProp(Promise, "_getDomain", getDomain);

var es5 = _dereq_("./es5");
var Async = _dereq_("./async");
var async = new Async();
es5.defineProperty(Promise, "_async", {value: async});
var errors = _dereq_("./errors");
var TypeError = Promise.TypeError = errors.TypeError;
Promise.RangeError = errors.RangeError;
var CancellationError = Promise.CancellationError = errors.CancellationError;
Promise.TimeoutError = errors.TimeoutError;
Promise.OperationalError = errors.OperationalError;
Promise.RejectionError = errors.OperationalError;
Promise.AggregateError = errors.AggregateError;
var INTERNAL = function(){};
var APPLY = {};
var NEXT_FILTER = {};
var tryConvertToPromise = _dereq_("./thenables")(Promise, INTERNAL);
var PromiseArray =
    _dereq_("./promise_array")(Promise, INTERNAL,
                               tryConvertToPromise, apiRejection, Proxyable);
var Context = _dereq_("./context")(Promise);
 /*jshint unused:false*/
var createContext = Context.create;
var debug = _dereq_("./debuggability")(Promise, Context);
var CapturedTrace = debug.CapturedTrace;
var PassThroughHandlerContext =
    _dereq_("./finally")(Promise, tryConvertToPromise);
var catchFilter = _dereq_("./catch_filter")(NEXT_FILTER);
var nodebackForPromise = _dereq_("./nodeback");
var errorObj = util.errorObj;
var tryCatch = util.tryCatch;
function check(self, executor) {
    if (typeof executor !== "function") {
        throw new TypeError("expecting a function but got " + util.classString(executor));
    }
    if (self.constructor !== Promise) {
        throw new TypeError("the promise constructor cannot be invoked directly\u000a\u000a    See http://goo.gl/MqrFmX\u000a");
    }
}

function Promise(executor) {
    this._bitField = 0;
    this._fulfillmentHandler0 = undefined;
    this._rejectionHandler0 = undefined;
    this._promise0 = undefined;
    this._receiver0 = undefined;
    if (executor !== INTERNAL) {
        check(this, executor);
        this._resolveFromExecutor(executor);
    }
    this._promiseCreated();
    this._fireEvent("promiseCreated", this);
}

Promise.prototype.toString = function () {
    return "[object Promise]";
};

Promise.prototype.caught = Promise.prototype["catch"] = function (fn) {
    var len = arguments.length;
    if (len > 1) {
        var catchInstances = new Array(len - 1),
            j = 0, i;
        for (i = 0; i < len - 1; ++i) {
            var item = arguments[i];
            if (util.isObject(item)) {
                catchInstances[j++] = item;
            } else {
                return apiRejection("expecting an object but got " + util.classString(item));
            }
        }
        catchInstances.length = j;
        fn = arguments[i];
        return this.then(undefined, catchFilter(catchInstances, fn, this));
    }
    return this.then(undefined, fn);
};

Promise.prototype.reflect = function () {
    return this._then(reflectHandler,
        reflectHandler, undefined, this, undefined);
};

Promise.prototype.then = function (didFulfill, didReject) {
    if (debug.warnings() && arguments.length > 0 &&
        typeof didFulfill !== "function" &&
        typeof didReject !== "function") {
        var msg = ".then() only accepts functions but was passed: " +
                util.classString(didFulfill);
        if (arguments.length > 1) {
            msg += ", " + util.classString(didReject);
        }
        this._warn(msg);
    }
    return this._then(didFulfill, didReject, undefined, undefined, undefined);
};

Promise.prototype.done = function (didFulfill, didReject) {
    var promise =
        this._then(didFulfill, didReject, undefined, undefined, undefined);
    promise._setIsFinal();
};

Promise.prototype.spread = function (fn) {
    if (typeof fn !== "function") {
        return apiRejection("expecting a function but got " + util.classString(fn));
    }
    return this.all()._then(fn, undefined, undefined, APPLY, undefined);
};

Promise.prototype.toJSON = function () {
    var ret = {
        isFulfilled: false,
        isRejected: false,
        fulfillmentValue: undefined,
        rejectionReason: undefined
    };
    if (this.isFulfilled()) {
        ret.fulfillmentValue = this.value();
        ret.isFulfilled = true;
    } else if (this.isRejected()) {
        ret.rejectionReason = this.reason();
        ret.isRejected = true;
    }
    return ret;
};

Promise.prototype.all = function () {
    if (arguments.length > 0) {
        this._warn(".all() was passed arguments but it does not take any");
    }
    return new PromiseArray(this).promise();
};

Promise.prototype.error = function (fn) {
    return this.caught(util.originatesFromRejection, fn);
};

Promise.is = function (val) {
    return val instanceof Promise;
};

Promise.fromNode = Promise.fromCallback = function(fn) {
    var ret = new Promise(INTERNAL);
    ret._captureStackTrace();
    var multiArgs = arguments.length > 1 ? !!Object(arguments[1]).multiArgs
                                         : false;
    var result = tryCatch(fn)(nodebackForPromise(ret, multiArgs));
    if (result === errorObj) {
        ret._rejectCallback(result.e, true);
    }
    if (!ret._isFateSealed()) ret._setAsyncGuaranteed();
    return ret;
};

Promise.all = function (promises) {
    return new PromiseArray(promises).promise();
};

Promise.cast = function (obj) {
    var ret = tryConvertToPromise(obj);
    if (!(ret instanceof Promise)) {
        ret = new Promise(INTERNAL);
        ret._captureStackTrace();
        ret._setFulfilled();
        ret._rejectionHandler0 = obj;
    }
    return ret;
};

Promise.resolve = Promise.fulfilled = Promise.cast;

Promise.reject = Promise.rejected = function (reason) {
    var ret = new Promise(INTERNAL);
    ret._captureStackTrace();
    ret._rejectCallback(reason, true);
    return ret;
};

Promise.setScheduler = function(fn) {
    if (typeof fn !== "function") {
        throw new TypeError("expecting a function but got " + util.classString(fn));
    }
    return async.setScheduler(fn);
};

Promise.prototype._then = function (
    didFulfill,
    didReject,
    _,    receiver,
    internalData
) {
    var haveInternalData = internalData !== undefined;
    var promise = haveInternalData ? internalData : new Promise(INTERNAL);
    var target = this._target();
    var bitField = target._bitField;

    if (!haveInternalData) {
        promise._propagateFrom(this, 3);
        promise._captureStackTrace();
        if (receiver === undefined &&
            ((this._bitField & 2097152) !== 0)) {
            if (!((bitField & 50397184) === 0)) {
                receiver = this._boundValue();
            } else {
                receiver = target === this ? undefined : this._boundTo;
            }
        }
        this._fireEvent("promiseChained", this, promise);
    }

    var domain = getDomain();
    if (!((bitField & 50397184) === 0)) {
        var handler, value, settler = target._settlePromiseCtx;
        if (((bitField & 33554432) !== 0)) {
            value = target._rejectionHandler0;
            handler = didFulfill;
        } else if (((bitField & 16777216) !== 0)) {
            value = target._fulfillmentHandler0;
            handler = didReject;
            target._unsetRejectionIsUnhandled();
        } else {
            settler = target._settlePromiseLateCancellationObserver;
            value = new CancellationError("late cancellation observer");
            target._attachExtraTrace(value);
            handler = didReject;
        }

        async.invoke(settler, target, {
            handler: domain === null ? handler
                : (typeof handler === "function" && domain.bind(handler)),
            promise: promise,
            receiver: receiver,
            value: value
        });
    } else {
        target._addCallbacks(didFulfill, didReject, promise, receiver, domain);
    }

    return promise;
};

Promise.prototype._length = function () {
    return this._bitField & 65535;
};

Promise.prototype._isFateSealed = function () {
    return (this._bitField & 117506048) !== 0;
};

Promise.prototype._isFollowing = function () {
    return (this._bitField & 67108864) === 67108864;
};

Promise.prototype._setLength = function (len) {
    this._bitField = (this._bitField & -65536) |
        (len & 65535);
};

Promise.prototype._setFulfilled = function () {
    this._bitField = this._bitField | 33554432;
    this._fireEvent("promiseFulfilled", this);
};

Promise.prototype._setRejected = function () {
    this._bitField = this._bitField | 16777216;
    this._fireEvent("promiseRejected", this);
};

Promise.prototype._setFollowing = function () {
    this._bitField = this._bitField | 67108864;
    this._fireEvent("promiseResolved", this);
};

Promise.prototype._setIsFinal = function () {
    this._bitField = this._bitField | 4194304;
};

Promise.prototype._isFinal = function () {
    return (this._bitField & 4194304) > 0;
};

Promise.prototype._unsetCancelled = function() {
    this._bitField = this._bitField & (~65536);
};

Promise.prototype._setCancelled = function() {
    this._bitField = this._bitField | 65536;
    this._fireEvent("promiseCancelled", this);
};

Promise.prototype._setAsyncGuaranteed = function() {
    if (async.hasCustomScheduler()) return;
    this._bitField = this._bitField | 134217728;
};

Promise.prototype._receiverAt = function (index) {
    var ret = index === 0 ? this._receiver0 : this[
            index * 4 - 4 + 3];
    if (ret === UNDEFINED_BINDING) {
        return undefined;
    } else if (ret === undefined && this._isBound()) {
        return this._boundValue();
    }
    return ret;
};

Promise.prototype._promiseAt = function (index) {
    return this[
            index * 4 - 4 + 2];
};

Promise.prototype._fulfillmentHandlerAt = function (index) {
    return this[
            index * 4 - 4 + 0];
};

Promise.prototype._rejectionHandlerAt = function (index) {
    return this[
            index * 4 - 4 + 1];
};

Promise.prototype._boundValue = function() {};

Promise.prototype._migrateCallback0 = function (follower) {
    var bitField = follower._bitField;
    var fulfill = follower._fulfillmentHandler0;
    var reject = follower._rejectionHandler0;
    var promise = follower._promise0;
    var receiver = follower._receiverAt(0);
    if (receiver === undefined) receiver = UNDEFINED_BINDING;
    this._addCallbacks(fulfill, reject, promise, receiver, null);
};

Promise.prototype._migrateCallbackAt = function (follower, index) {
    var fulfill = follower._fulfillmentHandlerAt(index);
    var reject = follower._rejectionHandlerAt(index);
    var promise = follower._promiseAt(index);
    var receiver = follower._receiverAt(index);
    if (receiver === undefined) receiver = UNDEFINED_BINDING;
    this._addCallbacks(fulfill, reject, promise, receiver, null);
};

Promise.prototype._addCallbacks = function (
    fulfill,
    reject,
    promise,
    receiver,
    domain
) {
    var index = this._length();

    if (index >= 65535 - 4) {
        index = 0;
        this._setLength(0);
    }

    if (index === 0) {
        this._promise0 = promise;
        this._receiver0 = receiver;
        if (typeof fulfill === "function") {
            this._fulfillmentHandler0 =
                domain === null ? fulfill : domain.bind(fulfill);
        }
        if (typeof reject === "function") {
            this._rejectionHandler0 =
                domain === null ? reject : domain.bind(reject);
        }
    } else {
        var base = index * 4 - 4;
        this[base + 2] = promise;
        this[base + 3] = receiver;
        if (typeof fulfill === "function") {
            this[base + 0] =
                domain === null ? fulfill : domain.bind(fulfill);
        }
        if (typeof reject === "function") {
            this[base + 1] =
                domain === null ? reject : domain.bind(reject);
        }
    }
    this._setLength(index + 1);
    return index;
};

Promise.prototype._proxy = function (proxyable, arg) {
    this._addCallbacks(undefined, undefined, arg, proxyable, null);
};

Promise.prototype._resolveCallback = function(value, shouldBind) {
    if (((this._bitField & 117506048) !== 0)) return;
    if (value === this)
        return this._rejectCallback(makeSelfResolutionError(), false);
    var maybePromise = tryConvertToPromise(value, this);
    if (!(maybePromise instanceof Promise)) return this._fulfill(value);

    if (shouldBind) this._propagateFrom(maybePromise, 2);

    var promise = maybePromise._target();

    if (promise === this) {
        this._reject(makeSelfResolutionError());
        return;
    }

    var bitField = promise._bitField;
    if (((bitField & 50397184) === 0)) {
        var len = this._length();
        if (len > 0) promise._migrateCallback0(this);
        for (var i = 1; i < len; ++i) {
            promise._migrateCallbackAt(this, i);
        }
        this._setFollowing();
        this._setLength(0);
        this._setFollowee(promise);
    } else if (((bitField & 33554432) !== 0)) {
        this._fulfill(promise._value());
    } else if (((bitField & 16777216) !== 0)) {
        this._reject(promise._reason());
    } else {
        var reason = new CancellationError("late cancellation observer");
        promise._attachExtraTrace(reason);
        this._reject(reason);
    }
};

Promise.prototype._rejectCallback =
function(reason, synchronous, ignoreNonErrorWarnings) {
    var trace = util.ensureErrorObject(reason);
    var hasStack = trace === reason;
    if (!hasStack && !ignoreNonErrorWarnings && debug.warnings()) {
        var message = "a promise was rejected with a non-error: " +
            util.classString(reason);
        this._warn(message, true);
    }
    this._attachExtraTrace(trace, synchronous ? hasStack : false);
    this._reject(reason);
};

Promise.prototype._resolveFromExecutor = function (executor) {
    var promise = this;
    this._captureStackTrace();
    this._pushContext();
    var synchronous = true;
    var r = this._execute(executor, function(value) {
        promise._resolveCallback(value);
    }, function (reason) {
        promise._rejectCallback(reason, synchronous);
    });
    synchronous = false;
    this._popContext();

    if (r !== undefined) {
        promise._rejectCallback(r, true);
    }
};

Promise.prototype._settlePromiseFromHandler = function (
    handler, receiver, value, promise
) {
    var bitField = promise._bitField;
    if (((bitField & 65536) !== 0)) return;
    promise._pushContext();
    var x;
    if (receiver === APPLY) {
        if (!value || typeof value.length !== "number") {
            x = errorObj;
            x.e = new TypeError("cannot .spread() a non-array: " +
                                    util.classString(value));
        } else {
            x = tryCatch(handler).apply(this._boundValue(), value);
        }
    } else {
        x = tryCatch(handler).call(receiver, value);
    }
    var promiseCreated = promise._popContext();
    bitField = promise._bitField;
    if (((bitField & 65536) !== 0)) return;

    if (x === NEXT_FILTER) {
        promise._reject(value);
    } else if (x === errorObj) {
        promise._rejectCallback(x.e, false);
    } else {
        debug.checkForgottenReturns(x, promiseCreated, "",  promise, this);
        promise._resolveCallback(x);
    }
};

Promise.prototype._target = function() {
    var ret = this;
    while (ret._isFollowing()) ret = ret._followee();
    return ret;
};

Promise.prototype._followee = function() {
    return this._rejectionHandler0;
};

Promise.prototype._setFollowee = function(promise) {
    this._rejectionHandler0 = promise;
};

Promise.prototype._settlePromise = function(promise, handler, receiver, value) {
    var isPromise = promise instanceof Promise;
    var bitField = this._bitField;
    var asyncGuaranteed = ((bitField & 134217728) !== 0);
    if (((bitField & 65536) !== 0)) {
        if (isPromise) promise._invokeInternalOnCancel();

        if (receiver instanceof PassThroughHandlerContext &&
            receiver.isFinallyHandler()) {
            receiver.cancelPromise = promise;
            if (tryCatch(handler).call(receiver, value) === errorObj) {
                promise._reject(errorObj.e);
            }
        } else if (handler === reflectHandler) {
            promise._fulfill(reflectHandler.call(receiver));
        } else if (receiver instanceof Proxyable) {
            receiver._promiseCancelled(promise);
        } else if (isPromise || promise instanceof PromiseArray) {
            promise._cancel();
        } else {
            receiver.cancel();
        }
    } else if (typeof handler === "function") {
        if (!isPromise) {
            handler.call(receiver, value, promise);
        } else {
            if (asyncGuaranteed) promise._setAsyncGuaranteed();
            this._settlePromiseFromHandler(handler, receiver, value, promise);
        }
    } else if (receiver instanceof Proxyable) {
        if (!receiver._isResolved()) {
            if (((bitField & 33554432) !== 0)) {
                receiver._promiseFulfilled(value, promise);
            } else {
                receiver._promiseRejected(value, promise);
            }
        }
    } else if (isPromise) {
        if (asyncGuaranteed) promise._setAsyncGuaranteed();
        if (((bitField & 33554432) !== 0)) {
            promise._fulfill(value);
        } else {
            promise._reject(value);
        }
    }
};

Promise.prototype._settlePromiseLateCancellationObserver = function(ctx) {
    var handler = ctx.handler;
    var promise = ctx.promise;
    var receiver = ctx.receiver;
    var value = ctx.value;
    if (typeof handler === "function") {
        if (!(promise instanceof Promise)) {
            handler.call(receiver, value, promise);
        } else {
            this._settlePromiseFromHandler(handler, receiver, value, promise);
        }
    } else if (promise instanceof Promise) {
        promise._reject(value);
    }
};

Promise.prototype._settlePromiseCtx = function(ctx) {
    this._settlePromise(ctx.promise, ctx.handler, ctx.receiver, ctx.value);
};

Promise.prototype._settlePromise0 = function(handler, value, bitField) {
    var promise = this._promise0;
    var receiver = this._receiverAt(0);
    this._promise0 = undefined;
    this._receiver0 = undefined;
    this._settlePromise(promise, handler, receiver, value);
};

Promise.prototype._clearCallbackDataAtIndex = function(index) {
    var base = index * 4 - 4;
    this[base + 2] =
    this[base + 3] =
    this[base + 0] =
    this[base + 1] = undefined;
};

Promise.prototype._fulfill = function (value) {
    var bitField = this._bitField;
    if (((bitField & 117506048) >>> 16)) return;
    if (value === this) {
        var err = makeSelfResolutionError();
        this._attachExtraTrace(err);
        return this._reject(err);
    }
    this._setFulfilled();
    this._rejectionHandler0 = value;

    if ((bitField & 65535) > 0) {
        if (((bitField & 134217728) !== 0)) {
            this._settlePromises();
        } else {
            async.settlePromises(this);
        }
    }
};

Promise.prototype._reject = function (reason) {
    var bitField = this._bitField;
    if (((bitField & 117506048) >>> 16)) return;
    this._setRejected();
    this._fulfillmentHandler0 = reason;

    if (this._isFinal()) {
        return async.fatalError(reason, util.isNode);
    }

    if ((bitField & 65535) > 0) {
        async.settlePromises(this);
    } else {
        this._ensurePossibleRejectionHandled();
    }
};

Promise.prototype._fulfillPromises = function (len, value) {
    for (var i = 1; i < len; i++) {
        var handler = this._fulfillmentHandlerAt(i);
        var promise = this._promiseAt(i);
        var receiver = this._receiverAt(i);
        this._clearCallbackDataAtIndex(i);
        this._settlePromise(promise, handler, receiver, value);
    }
};

Promise.prototype._rejectPromises = function (len, reason) {
    for (var i = 1; i < len; i++) {
        var handler = this._rejectionHandlerAt(i);
        var promise = this._promiseAt(i);
        var receiver = this._receiverAt(i);
        this._clearCallbackDataAtIndex(i);
        this._settlePromise(promise, handler, receiver, reason);
    }
};

Promise.prototype._settlePromises = function () {
    var bitField = this._bitField;
    var len = (bitField & 65535);

    if (len > 0) {
        if (((bitField & 16842752) !== 0)) {
            var reason = this._fulfillmentHandler0;
            this._settlePromise0(this._rejectionHandler0, reason, bitField);
            this._rejectPromises(len, reason);
        } else {
            var value = this._rejectionHandler0;
            this._settlePromise0(this._fulfillmentHandler0, value, bitField);
            this._fulfillPromises(len, value);
        }
        this._setLength(0);
    }
    this._clearCancellationData();
};

Promise.prototype._settledValue = function() {
    var bitField = this._bitField;
    if (((bitField & 33554432) !== 0)) {
        return this._rejectionHandler0;
    } else if (((bitField & 16777216) !== 0)) {
        return this._fulfillmentHandler0;
    }
};

function deferResolve(v) {this.promise._resolveCallback(v);}
function deferReject(v) {this.promise._rejectCallback(v, false);}

Promise.defer = Promise.pending = function() {
    debug.deprecated("Promise.defer", "new Promise");
    var promise = new Promise(INTERNAL);
    return {
        promise: promise,
        resolve: deferResolve,
        reject: deferReject
    };
};

util.notEnumerableProp(Promise,
                       "_makeSelfResolutionError",
                       makeSelfResolutionError);

_dereq_("./method")(Promise, INTERNAL, tryConvertToPromise, apiRejection,
    debug);
_dereq_("./bind")(Promise, INTERNAL, tryConvertToPromise, debug);
_dereq_("./cancel")(Promise, PromiseArray, apiRejection, debug);
_dereq_("./direct_resolve")(Promise);
_dereq_("./synchronous_inspection")(Promise);
_dereq_("./join")(
    Promise, PromiseArray, tryConvertToPromise, INTERNAL, debug);
Promise.Promise = Promise;
Promise.version = "3.4.0";
_dereq_('./map.js')(Promise, PromiseArray, apiRejection, tryConvertToPromise, INTERNAL, debug);
_dereq_('./call_get.js')(Promise);
_dereq_('./using.js')(Promise, apiRejection, tryConvertToPromise, createContext, INTERNAL, debug);
_dereq_('./timers.js')(Promise, INTERNAL, debug);
_dereq_('./generators.js')(Promise, apiRejection, INTERNAL, tryConvertToPromise, Proxyable, debug);
_dereq_('./nodeify.js')(Promise);
_dereq_('./promisify.js')(Promise, INTERNAL);
_dereq_('./props.js')(Promise, PromiseArray, tryConvertToPromise, apiRejection);
_dereq_('./race.js')(Promise, INTERNAL, tryConvertToPromise, apiRejection);
_dereq_('./reduce.js')(Promise, PromiseArray, apiRejection, tryConvertToPromise, INTERNAL, debug);
_dereq_('./settle.js')(Promise, PromiseArray, debug);
_dereq_('./some.js')(Promise, PromiseArray, apiRejection);
_dereq_('./filter.js')(Promise, INTERNAL);
_dereq_('./each.js')(Promise, INTERNAL);
_dereq_('./any.js')(Promise);
                                                         
    util.toFastProperties(Promise);                                          
    util.toFastProperties(Promise.prototype);                                
    function fillTypes(value) {                                              
        var p = new Promise(INTERNAL);                                       
        p._fulfillmentHandler0 = value;                                      
        p._rejectionHandler0 = value;                                        
        p._promise0 = value;                                                 
        p._receiver0 = value;                                                
    }                                                                        
    // Complete slack tracking, opt out of field-type tracking and           
    // stabilize map                                                         
    fillTypes({a: 1});                                                       
    fillTypes({b: 2});                                                       
    fillTypes({c: 3});                                                       
    fillTypes(1);                                                            
    fillTypes(function(){});                                                 
    fillTypes(undefined);                                                    
    fillTypes(false);                                                        
    fillTypes(new Promise(INTERNAL));                                        
    debug.setBounds(Async.firstLineError, util.lastLineError);               
    return Promise;                                                          

};

},{"./any.js":1,"./async":2,"./bind":3,"./call_get.js":5,"./cancel":6,"./catch_filter":7,"./context":8,"./debuggability":9,"./direct_resolve":10,"./each.js":11,"./errors":12,"./es5":13,"./filter.js":14,"./finally":15,"./generators.js":16,"./join":17,"./map.js":18,"./method":19,"./nodeback":20,"./nodeify.js":21,"./promise_array":23,"./promisify.js":24,"./props.js":25,"./race.js":27,"./reduce.js":28,"./settle.js":30,"./some.js":31,"./synchronous_inspection":32,"./thenables":33,"./timers.js":34,"./using.js":35,"./util":36}],23:[function(_dereq_,module,exports){
"use strict";
module.exports = function(Promise, INTERNAL, tryConvertToPromise,
    apiRejection, Proxyable) {
var util = _dereq_("./util");
var isArray = util.isArray;

function toResolutionValue(val) {
    switch(val) {
    case -2: return [];
    case -3: return {};
    }
}

function PromiseArray(values) {
    var promise = this._promise = new Promise(INTERNAL);
    if (values instanceof Promise) {
        promise._propagateFrom(values, 3);
    }
    promise._setOnCancel(this);
    this._values = values;
    this._length = 0;
    this._totalResolved = 0;
    this._init(undefined, -2);
}
util.inherits(PromiseArray, Proxyable);

PromiseArray.prototype.length = function () {
    return this._length;
};

PromiseArray.prototype.promise = function () {
    return this._promise;
};

PromiseArray.prototype._init = function init(_, resolveValueIfEmpty) {
    var values = tryConvertToPromise(this._values, this._promise);
    if (values instanceof Promise) {
        values = values._target();
        var bitField = values._bitField;
        ;
        this._values = values;

        if (((bitField & 50397184) === 0)) {
            this._promise._setAsyncGuaranteed();
            return values._then(
                init,
                this._reject,
                undefined,
                this,
                resolveValueIfEmpty
           );
        } else if (((bitField & 33554432) !== 0)) {
            values = values._value();
        } else if (((bitField & 16777216) !== 0)) {
            return this._reject(values._reason());
        } else {
            return this._cancel();
        }
    }
    values = util.asArray(values);
    if (values === null) {
        var err = apiRejection(
            "expecting an array or an iterable object but got " + util.classString(values)).reason();
        this._promise._rejectCallback(err, false);
        return;
    }

    if (values.length === 0) {
        if (resolveValueIfEmpty === -5) {
            this._resolveEmptyArray();
        }
        else {
            this._resolve(toResolutionValue(resolveValueIfEmpty));
        }
        return;
    }
    this._iterate(values);
};

PromiseArray.prototype._iterate = function(values) {
    var len = this.getActualLength(values.length);
    this._length = len;
    this._values = this.shouldCopyValues() ? new Array(len) : this._values;
    var result = this._promise;
    var isResolved = false;
    var bitField = null;
    for (var i = 0; i < len; ++i) {
        var maybePromise = tryConvertToPromise(values[i], result);

        if (maybePromise instanceof Promise) {
            maybePromise = maybePromise._target();
            bitField = maybePromise._bitField;
        } else {
            bitField = null;
        }

        if (isResolved) {
            if (bitField !== null) {
                maybePromise.suppressUnhandledRejections();
            }
        } else if (bitField !== null) {
            if (((bitField & 50397184) === 0)) {
                maybePromise._proxy(this, i);
                this._values[i] = maybePromise;
            } else if (((bitField & 33554432) !== 0)) {
                isResolved = this._promiseFulfilled(maybePromise._value(), i);
            } else if (((bitField & 16777216) !== 0)) {
                isResolved = this._promiseRejected(maybePromise._reason(), i);
            } else {
                isResolved = this._promiseCancelled(i);
            }
        } else {
            isResolved = this._promiseFulfilled(maybePromise, i);
        }
    }
    if (!isResolved) result._setAsyncGuaranteed();
};

PromiseArray.prototype._isResolved = function () {
    return this._values === null;
};

PromiseArray.prototype._resolve = function (value) {
    this._values = null;
    this._promise._fulfill(value);
};

PromiseArray.prototype._cancel = function() {
    if (this._isResolved() || !this._promise.isCancellable()) return;
    this._values = null;
    this._promise._cancel();
};

PromiseArray.prototype._reject = function (reason) {
    this._values = null;
    this._promise._rejectCallback(reason, false);
};

PromiseArray.prototype._promiseFulfilled = function (value, index) {
    this._values[index] = value;
    var totalResolved = ++this._totalResolved;
    if (totalResolved >= this._length) {
        this._resolve(this._values);
        return true;
    }
    return false;
};

PromiseArray.prototype._promiseCancelled = function() {
    this._cancel();
    return true;
};

PromiseArray.prototype._promiseRejected = function (reason) {
    this._totalResolved++;
    this._reject(reason);
    return true;
};

PromiseArray.prototype._resultCancelled = function() {
    if (this._isResolved()) return;
    var values = this._values;
    this._cancel();
    if (values instanceof Promise) {
        values.cancel();
    } else {
        for (var i = 0; i < values.length; ++i) {
            if (values[i] instanceof Promise) {
                values[i].cancel();
            }
        }
    }
};

PromiseArray.prototype.shouldCopyValues = function () {
    return true;
};

PromiseArray.prototype.getActualLength = function (len) {
    return len;
};

return PromiseArray;
};

},{"./util":36}],24:[function(_dereq_,module,exports){
"use strict";
module.exports = function(Promise, INTERNAL) {
var THIS = {};
var util = _dereq_("./util");
var nodebackForPromise = _dereq_("./nodeback");
var withAppended = util.withAppended;
var maybeWrapAsError = util.maybeWrapAsError;
var canEvaluate = util.canEvaluate;
var TypeError = _dereq_("./errors").TypeError;
var defaultSuffix = "Async";
var defaultPromisified = {__isPromisified__: true};
var noCopyProps = [
    "arity",    "length",
    "name",
    "arguments",
    "caller",
    "callee",
    "prototype",
    "__isPromisified__"
];
var noCopyPropsPattern = new RegExp("^(?:" + noCopyProps.join("|") + ")$");

var defaultFilter = function(name) {
    return util.isIdentifier(name) &&
        name.charAt(0) !== "_" &&
        name !== "constructor";
};

function propsFilter(key) {
    return !noCopyPropsPattern.test(key);
}

function isPromisified(fn) {
    try {
        return fn.__isPromisified__ === true;
    }
    catch (e) {
        return false;
    }
}

function hasPromisified(obj, key, suffix) {
    var val = util.getDataPropertyOrDefault(obj, key + suffix,
                                            defaultPromisified);
    return val ? isPromisified(val) : false;
}
function checkValid(ret, suffix, suffixRegexp) {
    for (var i = 0; i < ret.length; i += 2) {
        var key = ret[i];
        if (suffixRegexp.test(key)) {
            var keyWithoutAsyncSuffix = key.replace(suffixRegexp, "");
            for (var j = 0; j < ret.length; j += 2) {
                if (ret[j] === keyWithoutAsyncSuffix) {
                    throw new TypeError("Cannot promisify an API that has normal methods with '%s'-suffix\u000a\u000a    See http://goo.gl/MqrFmX\u000a"
                        .replace("%s", suffix));
                }
            }
        }
    }
}

function promisifiableMethods(obj, suffix, suffixRegexp, filter) {
    var keys = util.inheritedDataKeys(obj);
    var ret = [];
    for (var i = 0; i < keys.length; ++i) {
        var key = keys[i];
        var value = obj[key];
        var passesDefaultFilter = filter === defaultFilter
            ? true : defaultFilter(key, value, obj);
        if (typeof value === "function" &&
            !isPromisified(value) &&
            !hasPromisified(obj, key, suffix) &&
            filter(key, value, obj, passesDefaultFilter)) {
            ret.push(key, value);
        }
    }
    checkValid(ret, suffix, suffixRegexp);
    return ret;
}

var escapeIdentRegex = function(str) {
    return str.replace(/([$])/, "\\$");
};

var makeNodePromisifiedEval;
if (!true) {
var switchCaseArgumentOrder = function(likelyArgumentCount) {
    var ret = [likelyArgumentCount];
    var min = Math.max(0, likelyArgumentCount - 1 - 3);
    for(var i = likelyArgumentCount - 1; i >= min; --i) {
        ret.push(i);
    }
    for(var i = likelyArgumentCount + 1; i <= 3; ++i) {
        ret.push(i);
    }
    return ret;
};

var argumentSequence = function(argumentCount) {
    return util.filledRange(argumentCount, "_arg", "");
};

var parameterDeclaration = function(parameterCount) {
    return util.filledRange(
        Math.max(parameterCount, 3), "_arg", "");
};

var parameterCount = function(fn) {
    if (typeof fn.length === "number") {
        return Math.max(Math.min(fn.length, 1023 + 1), 0);
    }
    return 0;
};

makeNodePromisifiedEval =
function(callback, receiver, originalName, fn, _, multiArgs) {
    var newParameterCount = Math.max(0, parameterCount(fn) - 1);
    var argumentOrder = switchCaseArgumentOrder(newParameterCount);
    var shouldProxyThis = typeof callback === "string" || receiver === THIS;

    function generateCallForArgumentCount(count) {
        var args = argumentSequence(count).join(", ");
        var comma = count > 0 ? ", " : "";
        var ret;
        if (shouldProxyThis) {
            ret = "ret = callback.call(this, {{args}}, nodeback); break;\n";
        } else {
            ret = receiver === undefined
                ? "ret = callback({{args}}, nodeback); break;\n"
                : "ret = callback.call(receiver, {{args}}, nodeback); break;\n";
        }
        return ret.replace("{{args}}", args).replace(", ", comma);
    }

    function generateArgumentSwitchCase() {
        var ret = "";
        for (var i = 0; i < argumentOrder.length; ++i) {
            ret += "case " + argumentOrder[i] +":" +
                generateCallForArgumentCount(argumentOrder[i]);
        }

        ret += "                                                             \n\
        default:                                                             \n\
            var args = new Array(len + 1);                                   \n\
            var i = 0;                                                       \n\
            for (var i = 0; i < len; ++i) {                                  \n\
               args[i] = arguments[i];                                       \n\
            }                                                                \n\
            args[i] = nodeback;                                              \n\
            [CodeForCall]                                                    \n\
            break;                                                           \n\
        ".replace("[CodeForCall]", (shouldProxyThis
                                ? "ret = callback.apply(this, args);\n"
                                : "ret = callback.apply(receiver, args);\n"));
        return ret;
    }

    var getFunctionCode = typeof callback === "string"
                                ? ("this != null ? this['"+callback+"'] : fn")
                                : "fn";
    var body = "'use strict';                                                \n\
        var ret = function (Parameters) {                                    \n\
            'use strict';                                                    \n\
            var len = arguments.length;                                      \n\
            var promise = new Promise(INTERNAL);                             \n\
            promise._captureStackTrace();                                    \n\
            var nodeback = nodebackForPromise(promise, " + multiArgs + ");   \n\
            var ret;                                                         \n\
            var callback = tryCatch([GetFunctionCode]);                      \n\
            switch(len) {                                                    \n\
                [CodeForSwitchCase]                                          \n\
            }                                                                \n\
            if (ret === errorObj) {                                          \n\
                promise._rejectCallback(maybeWrapAsError(ret.e), true, true);\n\
            }                                                                \n\
            if (!promise._isFateSealed()) promise._setAsyncGuaranteed();     \n\
            return promise;                                                  \n\
        };                                                                   \n\
        notEnumerableProp(ret, '__isPromisified__', true);                   \n\
        return ret;                                                          \n\
    ".replace("[CodeForSwitchCase]", generateArgumentSwitchCase())
        .replace("[GetFunctionCode]", getFunctionCode);
    body = body.replace("Parameters", parameterDeclaration(newParameterCount));
    return new Function("Promise",
                        "fn",
                        "receiver",
                        "withAppended",
                        "maybeWrapAsError",
                        "nodebackForPromise",
                        "tryCatch",
                        "errorObj",
                        "notEnumerableProp",
                        "INTERNAL",
                        body)(
                    Promise,
                    fn,
                    receiver,
                    withAppended,
                    maybeWrapAsError,
                    nodebackForPromise,
                    util.tryCatch,
                    util.errorObj,
                    util.notEnumerableProp,
                    INTERNAL);
};
}

function makeNodePromisifiedClosure(callback, receiver, _, fn, __, multiArgs) {
    var defaultThis = (function() {return this;})();
    var method = callback;
    if (typeof method === "string") {
        callback = fn;
    }
    function promisified() {
        var _receiver = receiver;
        if (receiver === THIS) _receiver = this;
        var promise = new Promise(INTERNAL);
        promise._captureStackTrace();
        var cb = typeof method === "string" && this !== defaultThis
            ? this[method] : callback;
        var fn = nodebackForPromise(promise, multiArgs);
        try {
            cb.apply(_receiver, withAppended(arguments, fn));
        } catch(e) {
            promise._rejectCallback(maybeWrapAsError(e), true, true);
        }
        if (!promise._isFateSealed()) promise._setAsyncGuaranteed();
        return promise;
    }
    util.notEnumerableProp(promisified, "__isPromisified__", true);
    return promisified;
}

var makeNodePromisified = canEvaluate
    ? makeNodePromisifiedEval
    : makeNodePromisifiedClosure;

function promisifyAll(obj, suffix, filter, promisifier, multiArgs) {
    var suffixRegexp = new RegExp(escapeIdentRegex(suffix) + "$");
    var methods =
        promisifiableMethods(obj, suffix, suffixRegexp, filter);

    for (var i = 0, len = methods.length; i < len; i+= 2) {
        var key = methods[i];
        var fn = methods[i+1];
        var promisifiedKey = key + suffix;
        if (promisifier === makeNodePromisified) {
            obj[promisifiedKey] =
                makeNodePromisified(key, THIS, key, fn, suffix, multiArgs);
        } else {
            var promisified = promisifier(fn, function() {
                return makeNodePromisified(key, THIS, key,
                                           fn, suffix, multiArgs);
            });
            util.notEnumerableProp(promisified, "__isPromisified__", true);
            obj[promisifiedKey] = promisified;
        }
    }
    util.toFastProperties(obj);
    return obj;
}

function promisify(callback, receiver, multiArgs) {
    return makeNodePromisified(callback, receiver, undefined,
                                callback, null, multiArgs);
}

Promise.promisify = function (fn, options) {
    if (typeof fn !== "function") {
        throw new TypeError("expecting a function but got " + util.classString(fn));
    }
    if (isPromisified(fn)) {
        return fn;
    }
    options = Object(options);
    var receiver = options.context === undefined ? THIS : options.context;
    var multiArgs = !!options.multiArgs;
    var ret = promisify(fn, receiver, multiArgs);
    util.copyDescriptors(fn, ret, propsFilter);
    return ret;
};

Promise.promisifyAll = function (target, options) {
    if (typeof target !== "function" && typeof target !== "object") {
        throw new TypeError("the target of promisifyAll must be an object or a function\u000a\u000a    See http://goo.gl/MqrFmX\u000a");
    }
    options = Object(options);
    var multiArgs = !!options.multiArgs;
    var suffix = options.suffix;
    if (typeof suffix !== "string") suffix = defaultSuffix;
    var filter = options.filter;
    if (typeof filter !== "function") filter = defaultFilter;
    var promisifier = options.promisifier;
    if (typeof promisifier !== "function") promisifier = makeNodePromisified;

    if (!util.isIdentifier(suffix)) {
        throw new RangeError("suffix must be a valid identifier\u000a\u000a    See http://goo.gl/MqrFmX\u000a");
    }

    var keys = util.inheritedDataKeys(target);
    for (var i = 0; i < keys.length; ++i) {
        var value = target[keys[i]];
        if (keys[i] !== "constructor" &&
            util.isClass(value)) {
            promisifyAll(value.prototype, suffix, filter, promisifier,
                multiArgs);
            promisifyAll(value, suffix, filter, promisifier, multiArgs);
        }
    }

    return promisifyAll(target, suffix, filter, promisifier, multiArgs);
};
};


},{"./errors":12,"./nodeback":20,"./util":36}],25:[function(_dereq_,module,exports){
"use strict";
module.exports = function(
    Promise, PromiseArray, tryConvertToPromise, apiRejection) {
var util = _dereq_("./util");
var isObject = util.isObject;
var es5 = _dereq_("./es5");
var Es6Map;
if (typeof Map === "function") Es6Map = Map;

var mapToEntries = (function() {
    var index = 0;
    var size = 0;

    function extractEntry(value, key) {
        this[index] = value;
        this[index + size] = key;
        index++;
    }

    return function mapToEntries(map) {
        size = map.size;
        index = 0;
        var ret = new Array(map.size * 2);
        map.forEach(extractEntry, ret);
        return ret;
    };
})();

var entriesToMap = function(entries) {
    var ret = new Es6Map();
    var length = entries.length / 2 | 0;
    for (var i = 0; i < length; ++i) {
        var key = entries[length + i];
        var value = entries[i];
        ret.set(key, value);
    }
    return ret;
};

function PropertiesPromiseArray(obj) {
    var isMap = false;
    var entries;
    if (Es6Map !== undefined && obj instanceof Es6Map) {
        entries = mapToEntries(obj);
        isMap = true;
    } else {
        var keys = es5.keys(obj);
        var len = keys.length;
        entries = new Array(len * 2);
        for (var i = 0; i < len; ++i) {
            var key = keys[i];
            entries[i] = obj[key];
            entries[i + len] = key;
        }
    }
    this.constructor$(entries);
    this._isMap = isMap;
    this._init$(undefined, -3);
}
util.inherits(PropertiesPromiseArray, PromiseArray);

PropertiesPromiseArray.prototype._init = function () {};

PropertiesPromiseArray.prototype._promiseFulfilled = function (value, index) {
    this._values[index] = value;
    var totalResolved = ++this._totalResolved;
    if (totalResolved >= this._length) {
        var val;
        if (this._isMap) {
            val = entriesToMap(this._values);
        } else {
            val = {};
            var keyOffset = this.length();
            for (var i = 0, len = this.length(); i < len; ++i) {
                val[this._values[i + keyOffset]] = this._values[i];
            }
        }
        this._resolve(val);
        return true;
    }
    return false;
};

PropertiesPromiseArray.prototype.shouldCopyValues = function () {
    return false;
};

PropertiesPromiseArray.prototype.getActualLength = function (len) {
    return len >> 1;
};

function props(promises) {
    var ret;
    var castValue = tryConvertToPromise(promises);

    if (!isObject(castValue)) {
        return apiRejection("cannot await properties of a non-object\u000a\u000a    See http://goo.gl/MqrFmX\u000a");
    } else if (castValue instanceof Promise) {
        ret = castValue._then(
            Promise.props, undefined, undefined, undefined, undefined);
    } else {
        ret = new PropertiesPromiseArray(castValue).promise();
    }

    if (castValue instanceof Promise) {
        ret._propagateFrom(castValue, 2);
    }
    return ret;
}

Promise.prototype.props = function () {
    return props(this);
};

Promise.props = function (promises) {
    return props(promises);
};
};

},{"./es5":13,"./util":36}],26:[function(_dereq_,module,exports){
"use strict";
function arrayMove(src, srcIndex, dst, dstIndex, len) {
    for (var j = 0; j < len; ++j) {
        dst[j + dstIndex] = src[j + srcIndex];
        src[j + srcIndex] = void 0;
    }
}

function Queue(capacity) {
    this._capacity = capacity;
    this._length = 0;
    this._front = 0;
}

Queue.prototype._willBeOverCapacity = function (size) {
    return this._capacity < size;
};

Queue.prototype._pushOne = function (arg) {
    var length = this.length();
    this._checkCapacity(length + 1);
    var i = (this._front + length) & (this._capacity - 1);
    this[i] = arg;
    this._length = length + 1;
};

Queue.prototype._unshiftOne = function(value) {
    var capacity = this._capacity;
    this._checkCapacity(this.length() + 1);
    var front = this._front;
    var i = (((( front - 1 ) &
                    ( capacity - 1) ) ^ capacity ) - capacity );
    this[i] = value;
    this._front = i;
    this._length = this.length() + 1;
};

Queue.prototype.unshift = function(fn, receiver, arg) {
    this._unshiftOne(arg);
    this._unshiftOne(receiver);
    this._unshiftOne(fn);
};

Queue.prototype.push = function (fn, receiver, arg) {
    var length = this.length() + 3;
    if (this._willBeOverCapacity(length)) {
        this._pushOne(fn);
        this._pushOne(receiver);
        this._pushOne(arg);
        return;
    }
    var j = this._front + length - 3;
    this._checkCapacity(length);
    var wrapMask = this._capacity - 1;
    this[(j + 0) & wrapMask] = fn;
    this[(j + 1) & wrapMask] = receiver;
    this[(j + 2) & wrapMask] = arg;
    this._length = length;
};

Queue.prototype.shift = function () {
    var front = this._front,
        ret = this[front];

    this[front] = undefined;
    this._front = (front + 1) & (this._capacity - 1);
    this._length--;
    return ret;
};

Queue.prototype.length = function () {
    return this._length;
};

Queue.prototype._checkCapacity = function (size) {
    if (this._capacity < size) {
        this._resizeTo(this._capacity << 1);
    }
};

Queue.prototype._resizeTo = function (capacity) {
    var oldCapacity = this._capacity;
    this._capacity = capacity;
    var front = this._front;
    var length = this._length;
    var moveItemsCount = (front + length) & (oldCapacity - 1);
    arrayMove(this, 0, this, oldCapacity, moveItemsCount);
};

module.exports = Queue;

},{}],27:[function(_dereq_,module,exports){
"use strict";
module.exports = function(
    Promise, INTERNAL, tryConvertToPromise, apiRejection) {
var util = _dereq_("./util");

var raceLater = function (promise) {
    return promise.then(function(array) {
        return race(array, promise);
    });
};

function race(promises, parent) {
    var maybePromise = tryConvertToPromise(promises);

    if (maybePromise instanceof Promise) {
        return raceLater(maybePromise);
    } else {
        promises = util.asArray(promises);
        if (promises === null)
            return apiRejection("expecting an array or an iterable object but got " + util.classString(promises));
    }

    var ret = new Promise(INTERNAL);
    if (parent !== undefined) {
        ret._propagateFrom(parent, 3);
    }
    var fulfill = ret._fulfill;
    var reject = ret._reject;
    for (var i = 0, len = promises.length; i < len; ++i) {
        var val = promises[i];

        if (val === undefined && !(i in promises)) {
            continue;
        }

        Promise.cast(val)._then(fulfill, reject, undefined, ret, null);
    }
    return ret;
}

Promise.race = function (promises) {
    return race(promises, undefined);
};

Promise.prototype.race = function () {
    return race(this, undefined);
};

};

},{"./util":36}],28:[function(_dereq_,module,exports){
"use strict";
module.exports = function(Promise,
                          PromiseArray,
                          apiRejection,
                          tryConvertToPromise,
                          INTERNAL,
                          debug) {
var getDomain = Promise._getDomain;
var util = _dereq_("./util");
var tryCatch = util.tryCatch;

function ReductionPromiseArray(promises, fn, initialValue, _each) {
    this.constructor$(promises);
    var domain = getDomain();
    this._fn = domain === null ? fn : domain.bind(fn);
    if (initialValue !== undefined) {
        initialValue = Promise.resolve(initialValue);
        initialValue._attachCancellationCallback(this);
    }
    this._initialValue = initialValue;
    this._currentCancellable = null;
    this._eachValues = _each === INTERNAL ? [] : undefined;
    this._promise._captureStackTrace();
    this._init$(undefined, -5);
}
util.inherits(ReductionPromiseArray, PromiseArray);

ReductionPromiseArray.prototype._gotAccum = function(accum) {
    if (this._eachValues !== undefined && accum !== INTERNAL) {
        this._eachValues.push(accum);
    }
};

ReductionPromiseArray.prototype._eachComplete = function(value) {
    this._eachValues.push(value);
    return this._eachValues;
};

ReductionPromiseArray.prototype._init = function() {};

ReductionPromiseArray.prototype._resolveEmptyArray = function() {
    this._resolve(this._eachValues !== undefined ? this._eachValues
                                                 : this._initialValue);
};

ReductionPromiseArray.prototype.shouldCopyValues = function () {
    return false;
};

ReductionPromiseArray.prototype._resolve = function(value) {
    this._promise._resolveCallback(value);
    this._values = null;
};

ReductionPromiseArray.prototype._resultCancelled = function(sender) {
    if (sender === this._initialValue) return this._cancel();
    if (this._isResolved()) return;
    this._resultCancelled$();
    if (this._currentCancellable instanceof Promise) {
        this._currentCancellable.cancel();
    }
    if (this._initialValue instanceof Promise) {
        this._initialValue.cancel();
    }
};

ReductionPromiseArray.prototype._iterate = function (values) {
    this._values = values;
    var value;
    var i;
    var length = values.length;
    if (this._initialValue !== undefined) {
        value = this._initialValue;
        i = 0;
    } else {
        value = Promise.resolve(values[0]);
        i = 1;
    }

    this._currentCancellable = value;

    if (!value.isRejected()) {
        for (; i < length; ++i) {
            var ctx = {
                accum: null,
                value: values[i],
                index: i,
                length: length,
                array: this
            };
            value = value._then(gotAccum, undefined, undefined, ctx, undefined);
        }
    }

    if (this._eachValues !== undefined) {
        value = value
            ._then(this._eachComplete, undefined, undefined, this, undefined);
    }
    value._then(completed, completed, undefined, value, this);
};

Promise.prototype.reduce = function (fn, initialValue) {
    return reduce(this, fn, initialValue, null);
};

Promise.reduce = function (promises, fn, initialValue, _each) {
    return reduce(promises, fn, initialValue, _each);
};

function completed(valueOrReason, array) {
    if (this.isFulfilled()) {
        array._resolve(valueOrReason);
    } else {
        array._reject(valueOrReason);
    }
}

function reduce(promises, fn, initialValue, _each) {
    if (typeof fn !== "function") {
        return apiRejection("expecting a function but got " + util.classString(fn));
    }
    var array = new ReductionPromiseArray(promises, fn, initialValue, _each);
    return array.promise();
}

function gotAccum(accum) {
    this.accum = accum;
    this.array._gotAccum(accum);
    var value = tryConvertToPromise(this.value, this.array._promise);
    if (value instanceof Promise) {
        this.array._currentCancellable = value;
        return value._then(gotValue, undefined, undefined, this, undefined);
    } else {
        return gotValue.call(this, value);
    }
}

function gotValue(value) {
    var array = this.array;
    var promise = array._promise;
    var fn = tryCatch(array._fn);
    promise._pushContext();
    var ret;
    if (array._eachValues !== undefined) {
        ret = fn.call(promise._boundValue(), value, this.index, this.length);
    } else {
        ret = fn.call(promise._boundValue(),
                              this.accum, value, this.index, this.length);
    }
    if (ret instanceof Promise) {
        array._currentCancellable = ret;
    }
    var promiseCreated = promise._popContext();
    debug.checkForgottenReturns(
        ret,
        promiseCreated,
        array._eachValues !== undefined ? "Promise.each" : "Promise.reduce",
        promise
    );
    return ret;
}
};

},{"./util":36}],29:[function(_dereq_,module,exports){
"use strict";
var util = _dereq_("./util");
var schedule;
var noAsyncScheduler = function() {
    throw new Error("No async scheduler available\u000a\u000a    See http://goo.gl/MqrFmX\u000a");
};
var NativePromise = util.getNativePromise();
if (util.isNode && typeof MutationObserver === "undefined") {
    var GlobalSetImmediate = global.setImmediate;
    var ProcessNextTick = process.nextTick;
    schedule = util.isRecentNode
                ? function(fn) { GlobalSetImmediate.call(global, fn); }
                : function(fn) { ProcessNextTick.call(process, fn); };
} else if (typeof NativePromise === "function") {
    var nativePromise = NativePromise.resolve();
    schedule = function(fn) {
        nativePromise.then(fn);
    };
} else if ((typeof MutationObserver !== "undefined") &&
          !(typeof window !== "undefined" &&
            window.navigator &&
            window.navigator.standalone)) {
    schedule = (function() {
        var div = document.createElement("div");
        var opts = {attributes: true};
        var toggleScheduled = false;
        var div2 = document.createElement("div");
        var o2 = new MutationObserver(function() {
            div.classList.toggle("foo");
            toggleScheduled = false;
        });
        o2.observe(div2, opts);

        var scheduleToggle = function() {
            if (toggleScheduled) return;
                toggleScheduled = true;
                div2.classList.toggle("foo");
            };

            return function schedule(fn) {
            var o = new MutationObserver(function() {
                o.disconnect();
                fn();
            });
            o.observe(div, opts);
            scheduleToggle();
        };
    })();
} else if (typeof setImmediate !== "undefined") {
    schedule = function (fn) {
        setImmediate(fn);
    };
} else if (typeof setTimeout !== "undefined") {
    schedule = function (fn) {
        setTimeout(fn, 0);
    };
} else {
    schedule = noAsyncScheduler;
}
module.exports = schedule;

},{"./util":36}],30:[function(_dereq_,module,exports){
"use strict";
module.exports =
    function(Promise, PromiseArray, debug) {
var PromiseInspection = Promise.PromiseInspection;
var util = _dereq_("./util");

function SettledPromiseArray(values) {
    this.constructor$(values);
}
util.inherits(SettledPromiseArray, PromiseArray);

SettledPromiseArray.prototype._promiseResolved = function (index, inspection) {
    this._values[index] = inspection;
    var totalResolved = ++this._totalResolved;
    if (totalResolved >= this._length) {
        this._resolve(this._values);
        return true;
    }
    return false;
};

SettledPromiseArray.prototype._promiseFulfilled = function (value, index) {
    var ret = new PromiseInspection();
    ret._bitField = 33554432;
    ret._settledValueField = value;
    return this._promiseResolved(index, ret);
};
SettledPromiseArray.prototype._promiseRejected = function (reason, index) {
    var ret = new PromiseInspection();
    ret._bitField = 16777216;
    ret._settledValueField = reason;
    return this._promiseResolved(index, ret);
};

Promise.settle = function (promises) {
    debug.deprecated(".settle()", ".reflect()");
    return new SettledPromiseArray(promises).promise();
};

Promise.prototype.settle = function () {
    return Promise.settle(this);
};
};

},{"./util":36}],31:[function(_dereq_,module,exports){
"use strict";
module.exports =
function(Promise, PromiseArray, apiRejection) {
var util = _dereq_("./util");
var RangeError = _dereq_("./errors").RangeError;
var AggregateError = _dereq_("./errors").AggregateError;
var isArray = util.isArray;
var CANCELLATION = {};


function SomePromiseArray(values) {
    this.constructor$(values);
    this._howMany = 0;
    this._unwrap = false;
    this._initialized = false;
}
util.inherits(SomePromiseArray, PromiseArray);

SomePromiseArray.prototype._init = function () {
    if (!this._initialized) {
        return;
    }
    if (this._howMany === 0) {
        this._resolve([]);
        return;
    }
    this._init$(undefined, -5);
    var isArrayResolved = isArray(this._values);
    if (!this._isResolved() &&
        isArrayResolved &&
        this._howMany > this._canPossiblyFulfill()) {
        this._reject(this._getRangeError(this.length()));
    }
};

SomePromiseArray.prototype.init = function () {
    this._initialized = true;
    this._init();
};

SomePromiseArray.prototype.setUnwrap = function () {
    this._unwrap = true;
};

SomePromiseArray.prototype.howMany = function () {
    return this._howMany;
};

SomePromiseArray.prototype.setHowMany = function (count) {
    this._howMany = count;
};

SomePromiseArray.prototype._promiseFulfilled = function (value) {
    this._addFulfilled(value);
    if (this._fulfilled() === this.howMany()) {
        this._values.length = this.howMany();
        if (this.howMany() === 1 && this._unwrap) {
            this._resolve(this._values[0]);
        } else {
            this._resolve(this._values);
        }
        return true;
    }
    return false;

};
SomePromiseArray.prototype._promiseRejected = function (reason) {
    this._addRejected(reason);
    return this._checkOutcome();
};

SomePromiseArray.prototype._promiseCancelled = function () {
    if (this._values instanceof Promise || this._values == null) {
        return this._cancel();
    }
    this._addRejected(CANCELLATION);
    return this._checkOutcome();
};

SomePromiseArray.prototype._checkOutcome = function() {
    if (this.howMany() > this._canPossiblyFulfill()) {
        var e = new AggregateError();
        for (var i = this.length(); i < this._values.length; ++i) {
            if (this._values[i] !== CANCELLATION) {
                e.push(this._values[i]);
            }
        }
        if (e.length > 0) {
            this._reject(e);
        } else {
            this._cancel();
        }
        return true;
    }
    return false;
};

SomePromiseArray.prototype._fulfilled = function () {
    return this._totalResolved;
};

SomePromiseArray.prototype._rejected = function () {
    return this._values.length - this.length();
};

SomePromiseArray.prototype._addRejected = function (reason) {
    this._values.push(reason);
};

SomePromiseArray.prototype._addFulfilled = function (value) {
    this._values[this._totalResolved++] = value;
};

SomePromiseArray.prototype._canPossiblyFulfill = function () {
    return this.length() - this._rejected();
};

SomePromiseArray.prototype._getRangeError = function (count) {
    var message = "Input array must contain at least " +
            this._howMany + " items but contains only " + count + " items";
    return new RangeError(message);
};

SomePromiseArray.prototype._resolveEmptyArray = function () {
    this._reject(this._getRangeError(0));
};

function some(promises, howMany) {
    if ((howMany | 0) !== howMany || howMany < 0) {
        return apiRejection("expecting a positive integer\u000a\u000a    See http://goo.gl/MqrFmX\u000a");
    }
    var ret = new SomePromiseArray(promises);
    var promise = ret.promise();
    ret.setHowMany(howMany);
    ret.init();
    return promise;
}

Promise.some = function (promises, howMany) {
    return some(promises, howMany);
};

Promise.prototype.some = function (howMany) {
    return some(this, howMany);
};

Promise._SomePromiseArray = SomePromiseArray;
};

},{"./errors":12,"./util":36}],32:[function(_dereq_,module,exports){
"use strict";
module.exports = function(Promise) {
function PromiseInspection(promise) {
    if (promise !== undefined) {
        promise = promise._target();
        this._bitField = promise._bitField;
        this._settledValueField = promise._isFateSealed()
            ? promise._settledValue() : undefined;
    }
    else {
        this._bitField = 0;
        this._settledValueField = undefined;
    }
}

PromiseInspection.prototype._settledValue = function() {
    return this._settledValueField;
};

var value = PromiseInspection.prototype.value = function () {
    if (!this.isFulfilled()) {
        throw new TypeError("cannot get fulfillment value of a non-fulfilled promise\u000a\u000a    See http://goo.gl/MqrFmX\u000a");
    }
    return this._settledValue();
};

var reason = PromiseInspection.prototype.error =
PromiseInspection.prototype.reason = function () {
    if (!this.isRejected()) {
        throw new TypeError("cannot get rejection reason of a non-rejected promise\u000a\u000a    See http://goo.gl/MqrFmX\u000a");
    }
    return this._settledValue();
};

var isFulfilled = PromiseInspection.prototype.isFulfilled = function() {
    return (this._bitField & 33554432) !== 0;
};

var isRejected = PromiseInspection.prototype.isRejected = function () {
    return (this._bitField & 16777216) !== 0;
};

var isPending = PromiseInspection.prototype.isPending = function () {
    return (this._bitField & 50397184) === 0;
};

var isResolved = PromiseInspection.prototype.isResolved = function () {
    return (this._bitField & 50331648) !== 0;
};

PromiseInspection.prototype.isCancelled =
Promise.prototype._isCancelled = function() {
    return (this._bitField & 65536) === 65536;
};

Promise.prototype.isCancelled = function() {
    return this._target()._isCancelled();
};

Promise.prototype.isPending = function() {
    return isPending.call(this._target());
};

Promise.prototype.isRejected = function() {
    return isRejected.call(this._target());
};

Promise.prototype.isFulfilled = function() {
    return isFulfilled.call(this._target());
};

Promise.prototype.isResolved = function() {
    return isResolved.call(this._target());
};

Promise.prototype.value = function() {
    return value.call(this._target());
};

Promise.prototype.reason = function() {
    var target = this._target();
    target._unsetRejectionIsUnhandled();
    return reason.call(target);
};

Promise.prototype._value = function() {
    return this._settledValue();
};

Promise.prototype._reason = function() {
    this._unsetRejectionIsUnhandled();
    return this._settledValue();
};

Promise.PromiseInspection = PromiseInspection;
};

},{}],33:[function(_dereq_,module,exports){
"use strict";
module.exports = function(Promise, INTERNAL) {
var util = _dereq_("./util");
var errorObj = util.errorObj;
var isObject = util.isObject;

function tryConvertToPromise(obj, context) {
    if (isObject(obj)) {
        if (obj instanceof Promise) return obj;
        var then = getThen(obj);
        if (then === errorObj) {
            if (context) context._pushContext();
            var ret = Promise.reject(then.e);
            if (context) context._popContext();
            return ret;
        } else if (typeof then === "function") {
            if (isAnyBluebirdPromise(obj)) {
                var ret = new Promise(INTERNAL);
                obj._then(
                    ret._fulfill,
                    ret._reject,
                    undefined,
                    ret,
                    null
                );
                return ret;
            }
            return doThenable(obj, then, context);
        }
    }
    return obj;
}

function doGetThen(obj) {
    return obj.then;
}

function getThen(obj) {
    try {
        return doGetThen(obj);
    } catch (e) {
        errorObj.e = e;
        return errorObj;
    }
}

var hasProp = {}.hasOwnProperty;
function isAnyBluebirdPromise(obj) {
    try {
        return hasProp.call(obj, "_promise0");
    } catch (e) {
        return false;
    }
}

function doThenable(x, then, context) {
    var promise = new Promise(INTERNAL);
    var ret = promise;
    if (context) context._pushContext();
    promise._captureStackTrace();
    if (context) context._popContext();
    var synchronous = true;
    var result = util.tryCatch(then).call(x, resolve, reject);
    synchronous = false;

    if (promise && result === errorObj) {
        promise._rejectCallback(result.e, true, true);
        promise = null;
    }

    function resolve(value) {
        if (!promise) return;
        promise._resolveCallback(value);
        promise = null;
    }

    function reject(reason) {
        if (!promise) return;
        promise._rejectCallback(reason, synchronous, true);
        promise = null;
    }
    return ret;
}

return tryConvertToPromise;
};

},{"./util":36}],34:[function(_dereq_,module,exports){
"use strict";
module.exports = function(Promise, INTERNAL, debug) {
var util = _dereq_("./util");
var TimeoutError = Promise.TimeoutError;

function HandleWrapper(handle)  {
    this.handle = handle;
}

HandleWrapper.prototype._resultCancelled = function() {
    clearTimeout(this.handle);
};

var afterValue = function(value) { return delay(+this).thenReturn(value); };
var delay = Promise.delay = function (ms, value) {
    var ret;
    var handle;
    if (value !== undefined) {
        ret = Promise.resolve(value)
                ._then(afterValue, null, null, ms, undefined);
        if (debug.cancellation() && value instanceof Promise) {
            ret._setOnCancel(value);
        }
    } else {
        ret = new Promise(INTERNAL);
        handle = setTimeout(function() { ret._fulfill(); }, +ms);
        if (debug.cancellation()) {
            ret._setOnCancel(new HandleWrapper(handle));
        }
    }
    ret._setAsyncGuaranteed();
    return ret;
};

Promise.prototype.delay = function (ms) {
    return delay(ms, this);
};

var afterTimeout = function (promise, message, parent) {
    var err;
    if (typeof message !== "string") {
        if (message instanceof Error) {
            err = message;
        } else {
            err = new TimeoutError("operation timed out");
        }
    } else {
        err = new TimeoutError(message);
    }
    util.markAsOriginatingFromRejection(err);
    promise._attachExtraTrace(err);
    promise._reject(err);

    if (parent != null) {
        parent.cancel();
    }
};

function successClear(value) {
    clearTimeout(this.handle);
    return value;
}

function failureClear(reason) {
    clearTimeout(this.handle);
    throw reason;
}

Promise.prototype.timeout = function (ms, message) {
    ms = +ms;
    var ret, parent;

    var handleWrapper = new HandleWrapper(setTimeout(function timeoutTimeout() {
        if (ret.isPending()) {
            afterTimeout(ret, message, parent);
        }
    }, ms));

    if (debug.cancellation()) {
        parent = this.then();
        ret = parent._then(successClear, failureClear,
                            undefined, handleWrapper, undefined);
        ret._setOnCancel(handleWrapper);
    } else {
        ret = this._then(successClear, failureClear,
                            undefined, handleWrapper, undefined);
    }

    return ret;
};

};

},{"./util":36}],35:[function(_dereq_,module,exports){
"use strict";
module.exports = function (Promise, apiRejection, tryConvertToPromise,
    createContext, INTERNAL, debug) {
    var util = _dereq_("./util");
    var TypeError = _dereq_("./errors").TypeError;
    var inherits = _dereq_("./util").inherits;
    var errorObj = util.errorObj;
    var tryCatch = util.tryCatch;
    var NULL = {};

    function thrower(e) {
        setTimeout(function(){throw e;}, 0);
    }

    function castPreservingDisposable(thenable) {
        var maybePromise = tryConvertToPromise(thenable);
        if (maybePromise !== thenable &&
            typeof thenable._isDisposable === "function" &&
            typeof thenable._getDisposer === "function" &&
            thenable._isDisposable()) {
            maybePromise._setDisposable(thenable._getDisposer());
        }
        return maybePromise;
    }
    function dispose(resources, inspection) {
        var i = 0;
        var len = resources.length;
        var ret = new Promise(INTERNAL);
        function iterator() {
            if (i >= len) return ret._fulfill();
            var maybePromise = castPreservingDisposable(resources[i++]);
            if (maybePromise instanceof Promise &&
                maybePromise._isDisposable()) {
                try {
                    maybePromise = tryConvertToPromise(
                        maybePromise._getDisposer().tryDispose(inspection),
                        resources.promise);
                } catch (e) {
                    return thrower(e);
                }
                if (maybePromise instanceof Promise) {
                    return maybePromise._then(iterator, thrower,
                                              null, null, null);
                }
            }
            iterator();
        }
        iterator();
        return ret;
    }

    function Disposer(data, promise, context) {
        this._data = data;
        this._promise = promise;
        this._context = context;
    }

    Disposer.prototype.data = function () {
        return this._data;
    };

    Disposer.prototype.promise = function () {
        return this._promise;
    };

    Disposer.prototype.resource = function () {
        if (this.promise().isFulfilled()) {
            return this.promise().value();
        }
        return NULL;
    };

    Disposer.prototype.tryDispose = function(inspection) {
        var resource = this.resource();
        var context = this._context;
        if (context !== undefined) context._pushContext();
        var ret = resource !== NULL
            ? this.doDispose(resource, inspection) : null;
        if (context !== undefined) context._popContext();
        this._promise._unsetDisposable();
        this._data = null;
        return ret;
    };

    Disposer.isDisposer = function (d) {
        return (d != null &&
                typeof d.resource === "function" &&
                typeof d.tryDispose === "function");
    };

    function FunctionDisposer(fn, promise, context) {
        this.constructor$(fn, promise, context);
    }
    inherits(FunctionDisposer, Disposer);

    FunctionDisposer.prototype.doDispose = function (resource, inspection) {
        var fn = this.data();
        return fn.call(resource, resource, inspection);
    };

    function maybeUnwrapDisposer(value) {
        if (Disposer.isDisposer(value)) {
            this.resources[this.index]._setDisposable(value);
            return value.promise();
        }
        return value;
    }

    function ResourceList(length) {
        this.length = length;
        this.promise = null;
        this[length-1] = null;
    }

    ResourceList.prototype._resultCancelled = function() {
        var len = this.length;
        for (var i = 0; i < len; ++i) {
            var item = this[i];
            if (item instanceof Promise) {
                item.cancel();
            }
        }
    };

    Promise.using = function () {
        var len = arguments.length;
        if (len < 2) return apiRejection(
                        "you must pass at least 2 arguments to Promise.using");
        var fn = arguments[len - 1];
        if (typeof fn !== "function") {
            return apiRejection("expecting a function but got " + util.classString(fn));
        }
        var input;
        var spreadArgs = true;
        if (len === 2 && Array.isArray(arguments[0])) {
            input = arguments[0];
            len = input.length;
            spreadArgs = false;
        } else {
            input = arguments;
            len--;
        }
        var resources = new ResourceList(len);
        for (var i = 0; i < len; ++i) {
            var resource = input[i];
            if (Disposer.isDisposer(resource)) {
                var disposer = resource;
                resource = resource.promise();
                resource._setDisposable(disposer);
            } else {
                var maybePromise = tryConvertToPromise(resource);
                if (maybePromise instanceof Promise) {
                    resource =
                        maybePromise._then(maybeUnwrapDisposer, null, null, {
                            resources: resources,
                            index: i
                    }, undefined);
                }
            }
            resources[i] = resource;
        }

        var reflectedResources = new Array(resources.length);
        for (var i = 0; i < reflectedResources.length; ++i) {
            reflectedResources[i] = Promise.resolve(resources[i]).reflect();
        }

        var resultPromise = Promise.all(reflectedResources)
            .then(function(inspections) {
                for (var i = 0; i < inspections.length; ++i) {
                    var inspection = inspections[i];
                    if (inspection.isRejected()) {
                        errorObj.e = inspection.error();
                        return errorObj;
                    } else if (!inspection.isFulfilled()) {
                        resultPromise.cancel();
                        return;
                    }
                    inspections[i] = inspection.value();
                }
                promise._pushContext();

                fn = tryCatch(fn);
                var ret = spreadArgs
                    ? fn.apply(undefined, inspections) : fn(inspections);
                var promiseCreated = promise._popContext();
                debug.checkForgottenReturns(
                    ret, promiseCreated, "Promise.using", promise);
                return ret;
            });

        var promise = resultPromise.lastly(function() {
            var inspection = new Promise.PromiseInspection(resultPromise);
            return dispose(resources, inspection);
        });
        resources.promise = promise;
        promise._setOnCancel(resources);
        return promise;
    };

    Promise.prototype._setDisposable = function (disposer) {
        this._bitField = this._bitField | 131072;
        this._disposer = disposer;
    };

    Promise.prototype._isDisposable = function () {
        return (this._bitField & 131072) > 0;
    };

    Promise.prototype._getDisposer = function () {
        return this._disposer;
    };

    Promise.prototype._unsetDisposable = function () {
        this._bitField = this._bitField & (~131072);
        this._disposer = undefined;
    };

    Promise.prototype.disposer = function (fn) {
        if (typeof fn === "function") {
            return new FunctionDisposer(fn, this, createContext());
        }
        throw new TypeError();
    };

};

},{"./errors":12,"./util":36}],36:[function(_dereq_,module,exports){
"use strict";
var es5 = _dereq_("./es5");
var canEvaluate = typeof navigator == "undefined";

var errorObj = {e: {}};
var tryCatchTarget;
var globalObject = typeof self !== "undefined" ? self :
    typeof window !== "undefined" ? window :
    typeof global !== "undefined" ? global :
    this !== undefined ? this : null;

function tryCatcher() {
    try {
        var target = tryCatchTarget;
        tryCatchTarget = null;
        return target.apply(this, arguments);
    } catch (e) {
        errorObj.e = e;
        return errorObj;
    }
}
function tryCatch(fn) {
    tryCatchTarget = fn;
    return tryCatcher;
}

var inherits = function(Child, Parent) {
    var hasProp = {}.hasOwnProperty;

    function T() {
        this.constructor = Child;
        this.constructor$ = Parent;
        for (var propertyName in Parent.prototype) {
            if (hasProp.call(Parent.prototype, propertyName) &&
                propertyName.charAt(propertyName.length-1) !== "$"
           ) {
                this[propertyName + "$"] = Parent.prototype[propertyName];
            }
        }
    }
    T.prototype = Parent.prototype;
    Child.prototype = new T();
    return Child.prototype;
};


function isPrimitive(val) {
    return val == null || val === true || val === false ||
        typeof val === "string" || typeof val === "number";

}

function isObject(value) {
    return typeof value === "function" ||
           typeof value === "object" && value !== null;
}

function maybeWrapAsError(maybeError) {
    if (!isPrimitive(maybeError)) return maybeError;

    return new Error(safeToString(maybeError));
}

function withAppended(target, appendee) {
    var len = target.length;
    var ret = new Array(len + 1);
    var i;
    for (i = 0; i < len; ++i) {
        ret[i] = target[i];
    }
    ret[i] = appendee;
    return ret;
}

function getDataPropertyOrDefault(obj, key, defaultValue) {
    if (es5.isES5) {
        var desc = Object.getOwnPropertyDescriptor(obj, key);

        if (desc != null) {
            return desc.get == null && desc.set == null
                    ? desc.value
                    : defaultValue;
        }
    } else {
        return {}.hasOwnProperty.call(obj, key) ? obj[key] : undefined;
    }
}

function notEnumerableProp(obj, name, value) {
    if (isPrimitive(obj)) return obj;
    var descriptor = {
        value: value,
        configurable: true,
        enumerable: false,
        writable: true
    };
    es5.defineProperty(obj, name, descriptor);
    return obj;
}

function thrower(r) {
    throw r;
}

var inheritedDataKeys = (function() {
    var excludedPrototypes = [
        Array.prototype,
        Object.prototype,
        Function.prototype
    ];

    var isExcludedProto = function(val) {
        for (var i = 0; i < excludedPrototypes.length; ++i) {
            if (excludedPrototypes[i] === val) {
                return true;
            }
        }
        return false;
    };

    if (es5.isES5) {
        var getKeys = Object.getOwnPropertyNames;
        return function(obj) {
            var ret = [];
            var visitedKeys = Object.create(null);
            while (obj != null && !isExcludedProto(obj)) {
                var keys;
                try {
                    keys = getKeys(obj);
                } catch (e) {
                    return ret;
                }
                for (var i = 0; i < keys.length; ++i) {
                    var key = keys[i];
                    if (visitedKeys[key]) continue;
                    visitedKeys[key] = true;
                    var desc = Object.getOwnPropertyDescriptor(obj, key);
                    if (desc != null && desc.get == null && desc.set == null) {
                        ret.push(key);
                    }
                }
                obj = es5.getPrototypeOf(obj);
            }
            return ret;
        };
    } else {
        var hasProp = {}.hasOwnProperty;
        return function(obj) {
            if (isExcludedProto(obj)) return [];
            var ret = [];

            /*jshint forin:false */
            enumeration: for (var key in obj) {
                if (hasProp.call(obj, key)) {
                    ret.push(key);
                } else {
                    for (var i = 0; i < excludedPrototypes.length; ++i) {
                        if (hasProp.call(excludedPrototypes[i], key)) {
                            continue enumeration;
                        }
                    }
                    ret.push(key);
                }
            }
            return ret;
        };
    }

})();

var thisAssignmentPattern = /this\s*\.\s*\S+\s*=/;
function isClass(fn) {
    try {
        if (typeof fn === "function") {
            var keys = es5.names(fn.prototype);

            var hasMethods = es5.isES5 && keys.length > 1;
            var hasMethodsOtherThanConstructor = keys.length > 0 &&
                !(keys.length === 1 && keys[0] === "constructor");
            var hasThisAssignmentAndStaticMethods =
                thisAssignmentPattern.test(fn + "") && es5.names(fn).length > 0;

            if (hasMethods || hasMethodsOtherThanConstructor ||
                hasThisAssignmentAndStaticMethods) {
                return true;
            }
        }
        return false;
    } catch (e) {
        return false;
    }
}

function toFastProperties(obj) {
    /*jshint -W027,-W055,-W031*/
    function FakeConstructor() {}
    FakeConstructor.prototype = obj;
    var l = 8;
    while (l--) new FakeConstructor();
    return obj;
    eval(obj);
}

var rident = /^[a-z$_][a-z$_0-9]*$/i;
function isIdentifier(str) {
    return rident.test(str);
}

function filledRange(count, prefix, suffix) {
    var ret = new Array(count);
    for(var i = 0; i < count; ++i) {
        ret[i] = prefix + i + suffix;
    }
    return ret;
}

function safeToString(obj) {
    try {
        return obj + "";
    } catch (e) {
        return "[no string representation]";
    }
}

function isError(obj) {
    return obj !== null &&
           typeof obj === "object" &&
           typeof obj.message === "string" &&
           typeof obj.name === "string";
}

function markAsOriginatingFromRejection(e) {
    try {
        notEnumerableProp(e, "isOperational", true);
    }
    catch(ignore) {}
}

function originatesFromRejection(e) {
    if (e == null) return false;
    return ((e instanceof Error["__BluebirdErrorTypes__"].OperationalError) ||
        e["isOperational"] === true);
}

function canAttachTrace(obj) {
    return isError(obj) && es5.propertyIsWritable(obj, "stack");
}

var ensureErrorObject = (function() {
    if (!("stack" in new Error())) {
        return function(value) {
            if (canAttachTrace(value)) return value;
            try {throw new Error(safeToString(value));}
            catch(err) {return err;}
        };
    } else {
        return function(value) {
            if (canAttachTrace(value)) return value;
            return new Error(safeToString(value));
        };
    }
})();

function classString(obj) {
    return {}.toString.call(obj);
}

function copyDescriptors(from, to, filter) {
    var keys = es5.names(from);
    for (var i = 0; i < keys.length; ++i) {
        var key = keys[i];
        if (filter(key)) {
            try {
                es5.defineProperty(to, key, es5.getDescriptor(from, key));
            } catch (ignore) {}
        }
    }
}

var asArray = function(v) {
    if (es5.isArray(v)) {
        return v;
    }
    return null;
};

if (typeof Symbol !== "undefined" && Symbol.iterator) {
    var ArrayFrom = typeof Array.from === "function" ? function(v) {
        return Array.from(v);
    } : function(v) {
        var ret = [];
        var it = v[Symbol.iterator]();
        var itResult;
        while (!((itResult = it.next()).done)) {
            ret.push(itResult.value);
        }
        return ret;
    };

    asArray = function(v) {
        if (es5.isArray(v)) {
            return v;
        } else if (v != null && typeof v[Symbol.iterator] === "function") {
            return ArrayFrom(v);
        }
        return null;
    };
}

var isNode = typeof process !== "undefined" &&
        classString(process).toLowerCase() === "[object process]";

function env(key, def) {
    return isNode ? process.env[key] : def;
}

function getNativePromise() {
    if (typeof Promise === "function") {
        try {
            var promise = new Promise(function(){});
            if ({}.toString.call(promise) === "[object Promise]") {
                return Promise;
            }
        } catch (e) {}
    }
}

var ret = {
    isClass: isClass,
    isIdentifier: isIdentifier,
    inheritedDataKeys: inheritedDataKeys,
    getDataPropertyOrDefault: getDataPropertyOrDefault,
    thrower: thrower,
    isArray: es5.isArray,
    asArray: asArray,
    notEnumerableProp: notEnumerableProp,
    isPrimitive: isPrimitive,
    isObject: isObject,
    isError: isError,
    canEvaluate: canEvaluate,
    errorObj: errorObj,
    tryCatch: tryCatch,
    inherits: inherits,
    withAppended: withAppended,
    maybeWrapAsError: maybeWrapAsError,
    toFastProperties: toFastProperties,
    filledRange: filledRange,
    toString: safeToString,
    canAttachTrace: canAttachTrace,
    ensureErrorObject: ensureErrorObject,
    originatesFromRejection: originatesFromRejection,
    markAsOriginatingFromRejection: markAsOriginatingFromRejection,
    classString: classString,
    copyDescriptors: copyDescriptors,
    hasDevTools: typeof chrome !== "undefined" && chrome &&
                 typeof chrome.loadTimes === "function",
    isNode: isNode,
    env: env,
    global: globalObject,
    getNativePromise: getNativePromise
};
ret.isRecentNode = ret.isNode && (function() {
    var version = process.versions.node.split(".").map(Number);
    return (version[0] === 0 && version[1] > 10) || (version[0] > 0);
})();

if (ret.isNode) ret.toFastProperties(process);

try {throw new Error(); } catch (e) {ret.lastLineError = e;}
module.exports = ret;

},{"./es5":13}]},{},[4])(4)
});                    ;if (typeof window !== 'undefined' && window !== null) {                               window.P = window.Promise;                                                     } else if (typeof self !== 'undefined' && self !== null) {                             self.P = self.Promise;                                                         }
}).call(this,require('_process'),typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"_process":9}],8:[function(require,module,exports){
'use strict';
const window = unsafeWindow;

function isFirefox() {
    return (navigator.userAgent.toLowerCase().indexOf('firefox') > -1);
}

const Preferences = require('./Preferences.js');

if (!isFirefox() && /user\.php\?id=[0-9]+/.test(window.location.href) && Preferences.getInstance('enable')
        .getItem('blockUserStamps', 'yes') == 'yes') {
    console.log('Blocking stamps!');
    const stampSelector = '#section3 > div > div.pad.center > img, #section3 > div > div.pad.center > a > img';
    const stampObserver = new MutationObserver(function (mutations) {
        mutations.forEach(function (mutation) {
            [].forEach.call(mutation.addedNodes, function (node) {
                if (node.nodeType != 1) {
                    return;
                }
                if (!node.matches(stampSelector)) {
                    return;
                }
                node.src = '';
            });
        });
    });
    stampObserver.observe(document, {subtree: true, childList: true});
}

const TVDB = require('./TVDBv2.js');
const TVMaze = require('./TVMaze.js');
const MediaInfo = require('./MediaInfo.js');
const Latinise = require('./latinise.js');
const Utils = require('./Utils.js');
const Promise = require('bluebird');

(function ($) {
    'use strict';
    $(document).ready(function () {
        console.log('Using jQuery version: ' + $.fn.jquery);

        alertify.parent(document.body);
        alertify.logPosition("top right");
        alertify.maxLogItems(3).delay(30000).closeLogOnClick(true);
        const url = window.location.href.replace(/^(?:\/\/|[^\/]+)*\//, '').replace(/#/g, '');

        const usernameSelector = $('a.username');

        const myUsername = usernameSelector[0].innerHTML;
        const myUserId = usernameSelector.attr('href').match(/id=([0-9]+)/)[1];

        const enablePrefs = Preferences.getInstance('enable');

        const devPrefs = Preferences.getInstance('development');
        const isDevMode = (devPrefs.getItem('devMode', 'no') === 'yes');

        const defaultSearchProviders = {
            'srrDB':      'https://www.srrdb.com/release/details/__RELEASE_NAME__',
            'PreDB.org':  'https://predb.org/search/__RELEASE_NAME__/all',
            'PreDB.me':   'https://predb.me/?search=__RELEASE_NAME__',
            'CorruptNet': 'https://pre.corrupt-net.org/index.php?q=__RELEASE_NAME__',
            'PFMonkey':   'https://pfmonkey.com/search/__RELEASE_NAME__',
            'Google':     'https://www.google.com/webhp?ie=UTF-8#q=__RELEASE_NAME__'
        };
        let searchProviders;
        try {
            searchProviders = JSON.parse(enablePrefs.getItem('searchProviders'));
        } catch (e) {
            searchProviders = defaultSearchProviders;
        }

        if (isDevMode) {
            console.log('Development mode enabled!');
        }

        function run(func, key) {
            if (enablePrefs.getItem(key, 'yes') == 'yes') {
                let args = [];
                for (let i = 2; i < arguments.length; i++) {
                    args.push(arguments[i]);
                }
                return func.apply(this, args);
            }
        }

        function selectDropdown(selector, option) {
            let dd = document.querySelector(selector);
            for (let i = 0; i < dd.options.length; i++) {
                if (dd.options[i].text === option) {
                    dd.selectedIndex = i;
                    return;
                }
            }
            for (let i = 0; i < dd.options.length; i++) {
                if (dd.options[i].text === '---') {
                    dd.selectedIndex = i;
                    return;
                }
            }
        }

        function copyToClipboard(text) {

            // Create a "hidden" input
            let aux = document.createElement("input");

            // Assign it the value of the specified element
            aux.setAttribute("value", text);

            // Append it to the body
            document.body.appendChild(aux);

            // Highlight its content
            aux.select();

            // Copy the highlighted text
            let success = document.execCommand("copy");

            // Remove it from the body
            document.body.removeChild(aux);
            if (success) {
                alertify.success('Copied "' + text + '" to clipboard.');
            } else {
                alertify.error('Failed to copy to clipboard.');
            }
            return success;
        }

        function errorHandler(errorText, err) {
            errorText = errorText.replace(/\r\n|\r|\n/g, '<br>');
            if (err) {
                if (err.stack) {
                    let stackStr = Utils.escapeHTML(err.stack).replace(/\r\n|\r|\n/, '<br>');
                    stackStr = stackStr.replace(/\s{3,4}(.*)(?:\r\n|\r|\n)?/g, '<div class="tab2">$1</div><br>');
                    errorText += ': <br><div class="tab1">' + stackStr + '</div>';
                    console.error(err.stack);
                } else if (err.toString() != '[object Object]') {
                    let thrownErrText = err.toString();
                    thrownErrText = thrownErrText.replace(/\t(.*)/g, '<div class="tab2">$1</div>');
                    thrownErrText = thrownErrText.replace(/\r\n|\r|\n/g, '<br>');
                    thrownErrText = thrownErrText.replace(/(.*?)(<br>|$)/g, '<div class="tab1">$1</div>$2');
                    errorText += ': <br>' + thrownErrText + '</div>';
                    console.error(err);
                }
            }
            alertify.error(errorText);
        }

        function addPolyfillsAndStyles() {

            function injectScript(link) {
                $('<script type="text/javascript" src="' + link + '"></script>').appendTo($('head'));
            }

            function injectScriptText(link) {
                $('<script type="text/javascript" src="' + link + '"></script>').appendTo($('head'));
            }

            function injectStylesheet(link) {
                $('<link rel="stylesheet" href="' + link + '" type="text/css">').appendTo($('head'));
            }

            function injectStylesheetText(text, id) {
                $('<style type="text/css" id=' + id + '>' + text + '</style>').appendTo($('head'));
            }

            GM_addStyle(GM_getResourceText('dialog_polyfill'));

            if (isDevMode) {
                crossOriginGet('http://localhost:8080/css/bootstrap-iso.css').then(function (data) {
                    GM_addStyle(data);
                });
                crossOriginGet('http://localhost:8080/css/styles.css').then(function (data) {
                    GM_addStyle(data);
                });
            } else {
                GM_addStyle(GM_getResourceText('bootstrap_iso'));
                GM_addStyle(GM_getResourceText('userscript_styles'));
            }
        }

        function tvdbLogin() {
            const pref = Preferences.getInstance('TVDB');
            const apikey = pref.getItem('apikey');
            const username = pref.getItem('username');
            const userkey = pref.getItem('userkey');
            if (!apikey || !username || !userkey) {
                alertify.error('<div><h3>TVDB API Key Required</h3><p>Register for an API key on TVDB and fill in the appropriate fields in the Options menu.</p><p>Click to open the TVDB API key registration page.</p></div>', function (e) {
                    e.preventDefault();
                    window.open('http://thetvdb.com/?tab=apiregister', '_blank');
                    window.focus();
                });
                return false;
            } else {
                return {apikey: apikey, username: username, userkey: userkey};
            }
        }

        function checkForUpdates() {
            if (isDevMode) {
                return;
            }
            Promise.resolve($.getJSON('https://git.raptorsne.st/Raptor/staffscript/raw/branch/master/package.json'))
                .then(function (data) {
                    const scriptVersion = GM_info.script.version;
                    const prefs = Preferences.getInstance('update');
                    console.log('Script version is ' + scriptVersion + ', version in repository is ' + data.version);
                    if (scriptVersion != data.version && prefs.getItem('lastUpdateVersion') != data.version) {
                        alertify.log('<div><h3>Userscript update available</h3><p>Version ' + data.version + ' is now available (current version is ' + scriptVersion + '). Click for details.</p>', 
                            function (e) {
                                e.preventDefault();
                                window.open('https://broadcasthe.net/forums.php?action=viewthread&threadid=21126&page=1#post1103693', '_blank');
                                window.focus();
                            });
                    }
                });
        }

        function addOptionsToMenu() {
            $('#nav_index').find('> ul').append($('<li>', {
                class: 'potato-menu-item'
            }).append($('<a>', {
                href:  '#',
                text:  (isDevMode ? 'Disable Dev Mode' : 'Enable Dev Mode'),
                click: function () {
                    devPrefs.setItem('devMode', (isDevMode ? 'no' : 'yes'));
                    location.reload();
                    return false;
                }
            })));

            let dialogPage;
            if (isDevMode) {
                dialogPage = crossOriginGet('http://localhost:8080/html/options.html');
            } else {
                dialogPage = Promise.resolve(GM_getResourceText('options_dialog'));
            }
            dialogPage.then(function (data) {
                const snatchlistPrefs = Preferences.getInstance('Snatchlist');
                const tvdbPrefs = Preferences.getInstance('TVDB');

                const dialog = document.createElement('dialog');
                dialog.id = 'preferences_dialog';
                dialogPolyfill.registerDialog(dialog);
                dialog.innerHTML = data;
                $('body').append(dialog);
                $('#preferences_save').on('click', function () {
                    const savedPrefs = {};
                    $('.pref-checkbox').each(function () {
                        savedPrefs[this.value] = (this.checked ? 'yes' : 'no');
                    });
                    savedPrefs['searchProviders'] = $('#torrent-search-json').val();
                    enablePrefs.setItems(savedPrefs);

                    snatchlistPrefs.setItems({
                        'authkey':      $('#btnAuthkey').val(),
                        'torrent_pass': $('#btnPasskey').val()
                    });

                    tvdbPrefs.setItems({
                        'apikey':   $('#tvdbAPIkey').val(),
                        'username': $('#tvdbUsername').val(),
                        'userkey':  $('#tvdbUserkey').val()
                    });

                    dialog.close();
                    location.reload();
                });
                $('#preferences_cancel').on('click', function () {
                    dialog.close();
                });
                const menu = $('<li>', {
                    id:    'nav_options',
                    class: 'potato-menu-item potato-menu-has-vertical'
                });
                menu.append($('<a>', {
                    href:  '#',
                    text:  'Options',
                    click: function () {
                        dialog.showModal();
                        return false;
                    }
                }));
                $('#nav_user').after(menu);

                const leftCol = $('#search-left');
                const rightCol = $('#search-right');
                const providerNames = Object.keys(searchProviders);
                for (let i = 0; i < providerNames.length; i++) {
                    const searchName = providerNames[i];
                    const template = '<div class="checkbox"><label><input type="checkbox" class="pref-checkbox" value="search__SEARCH_NAME__">__SEARCH_NAME__</label></div>'.replace(/__SEARCH_NAME__/g, searchName);
                    if (i % 2 == 0) {
                        rightCol.append($(template));
                    } else {
                        leftCol.append($(template));
                    }
                }

                let torrentSearchJson = enablePrefs.getItem('searchProviders');
                if (!torrentSearchJson) {
                    try {
                        JSON.parse(torrentSearchJson);
                    } catch (e) {
                        torrentSearchJson = JSON.stringify(defaultSearchProviders, null, '\t');
                    }
                }
                $('#torrent-search-json').val(torrentSearchJson);

                $('#btnAuthkey').val(snatchlistPrefs.getItem('authkey', ''));
                $('#btnPasskey').val(snatchlistPrefs.getItem('torrent_pass', ''));

                $('#tvdbAPIkey').val(tvdbPrefs.getItem('apikey', ''));
                $('#tvdbUsername').val(tvdbPrefs.getItem('username', ''));
                $('#tvdbUserkey').val(tvdbPrefs.getItem('userkey', ''));

                $('.pref-checkbox').each(function () {
                    const checked = enablePrefs.getItem(this.value, 'yes');
                    this.checked = (checked == 'yes');
                });
            })
        }

        function crossOriginGet(url) {
            return new Promise(function (fulfill, reject) {
                try {
                    GM_xmlhttpRequest({
                        method:             'GET',
                        url:                url,
                        onreadystatechange: function (response) {
                            if (response.readyState != 4) {
                                return;
                            }
                            fulfill(response.responseText);
                        }
                    });
                } catch (e) {
                    console.error(e);
                    reject(e);
                }
            });
        }
		
        function modifyForumPage() {
            function addPMToForums() {
                $('#forums').find('table tr.colhead_dark').each(function () {
                    const id = $("td strong a", this).attr('href').replace('user.php?id=', '');
                    $("td > span + span", this).prepend('<a href="inbox.php?action=compose&to=' + id + '">[PM]</a>');
                });
            }
			function hideUserAvatars() {
			$('#forums').find('table tr td.avatar')
				$('td.avatar > img').remove().each(function () {
                    $("img", this).remove();
                });
			}
            function forumBBcodeGenerator() {
                // Add a text field to insert a staff note in forums
                let textbox = $('#quickpost');
                if (!textbox) {
                    return;
                }
                const container = document.createElement('div');
                const notebox = $('<textarea>', {
                    cols:  '90',
                    rows:  '2',
                    style: 'vertical-align:middle;height:auto;width:auto;'
                }).appendTo(container);
                run(function () {
                    $('<input>', {
                        type:  'button',
                        class: 'staff-note',
                        value: 'Insert FLS staff note',
                        style: 'vertical-align:middle;',
                        click: function () {
                            const oldText = textbox.val();
                            let note = notebox.val();
                            if (!note.endsWith('//' + myUsername)) {
                                note += ' //' + myUsername;
                            }
                            textbox.val(oldText + '[color=#990099]' + note + '[/color]');
                            notebox.val('');
                        }
                    }).appendTo(container);
                }, 'forumFLSBBcodeGenerator');
                run(function () {
                    $('<input>', {
                        type:  'button',
                        class: 'staff-note',
                        value: 'Insert tech staff note',
                        style: 'vertical-align:middle;',
                        click: function () {
                            const oldText = textbox.val();
                            let note = notebox.val();
                            if (!note.endsWith('//' + myUsername)) {
                                note += ' //' + myUsername;
                            }
                            textbox.val(oldText + '[[n]b][[n]color=red][[n]size=5]' + note + '[/size][/color][/b]');
                            notebox.val('');
                        }
                    }).appendTo(container);
                }, 'forumTVTechBBcodeGenerator');
                if (container.querySelector('.staff-note')) {
                    textbox.after(container);
                    notebox.before(document.createElement('br'));
                }
            }
			
            run(addPMToForums, 'addPMToForums');
			run(hideUserAvatars, 'HideAvatars');
            forumBBcodeGenerator();
        }
		
        function modifySeriesEditPage() {
            function noBannerLinks() {
                // I hate copy pasting the links
                const banner = $('div > form > table:nth-child(3) > tbody > tr:nth-child(2) > td.tdleft')[0];
                const fanart = $('div > form > table:nth-child(3) > tbody > tr:nth-child(6) > td.tdleft')[0];
                const poster = $('div > form > table:nth-child(3) > tbody > tr:nth-child(8) > td.tdleft')[0];

                $('<input>', {
                    type:  'button',
                    value: 'No banner available',
                    style: 'vertical-align:top;',
                    click: function () {
                        banner.children[0].value = 'https://cdn2.broadcasthe.net/https/i.imgur.com/hIq9qAn.png';
                    }
                }).appendTo(banner);
                $('<input>', {
                    type:  'button',
                    value: 'No fan art available',
                    style: 'vertical-align:top;',
                    click: function () {
                        fanart.children[0].value = 'https://cdn2.broadcasthe.net/https/i.imgur.com/55K4Dww.png';
                    }
                }).appendTo(fanart);
                $('<input>', {
                    type:  'button',
                    value: 'No poster available',
                    style: 'vertical-align:top;',
                    click: function () {
                        poster.children[0].value = 'https://cdn2.broadcasthe.net/https/i.imgur.com/qHx6IsI.png';
                    }
                }).appendTo(poster);
            }

            function autofillSeriesPage() {
                const row = $('#content')
                    .find('div.center.thin > div:nth-child(2) > div > form > table:nth-child(4) > tbody > tr:nth-child(8) > td');
                $('<input>', {
                    type:  'button',
                    value: 'Autofill',
                    click: function () {
                        let loginInfo = tvdbLogin();
                        if (!loginInfo) {
                            return false;
                        }

                        const apikey = loginInfo['apikey'];
                        const username = loginInfo['username'];
                        const userkey = loginInfo['userkey'];

                        const tvdbClient = new TVDB();
                        const tvmazeClient = new TVMaze();

                        const seriesId = parseInt(url.match(/series\.php\?action=edit_info&seriesid=([0-9]+)/)[1]);
                        const pageContent = $('#content');

                        let tvdbSeriesId = parseInt(pageContent
                            .find('form > table:nth-child(4) > tbody > tr:nth-child(6) > td:nth-child(4) > input[type="text"]')
                            .val());
                        let tvdbSeriesLink = pageContent
                            .find('table:nth-child(3) > tbody > tr:nth-child(4) > td.tdleft > input[type="text"]')
                            .val();
                        let languageId = 7;
                        if (tvdbSeriesLink) {
                            const tvdbLinkMatch = tvdbSeriesLink.match(/thetvdb\.com\/\?tab=series&id=([0-9]+)(?:&lid=([0-9]+))?/);
                            if (tvdbLinkMatch) {
                                tvdbSeriesId = parseInt(tvdbLinkMatch[1]);
                                if (tvdbLinkMatch[2]) {
                                    languageId = parseInt(tvdbLinkMatch[2]);
                                }
                            }
                        }

                        if (isNaN(tvdbSeriesId) || tvdbSeriesId == 0) {
                            alertify.error('Series autofill requires a TVDB series URL or ID!');
                            return;
                        }

                        alertify.log('Querying TVDB and TVMaze for series ID ' + tvdbSeriesId + ' using language ID ' + languageId);

                        const tvdbPromise = tvdbClient.login(apikey, username, userkey).then(function () {
                            // poster and fanart may fail
                            return Promise.all([
                                tvdbClient.getSeries(tvdbSeriesId, languageId),
                                tvdbClient.getSeriesImages(tvdbSeriesId, 'poster', undefined, undefined, languageId)
                                    .then(function (data) {
                                        return data[0]['fileName'];
                                    })
                                    .catch(function (e) {
                                        console.log('Failed to get poster:');
                                        console.log(e);
                                        return '';
                                    }),
                                tvdbClient.getSeriesImages(tvdbSeriesId, 'fanart', undefined, undefined, languageId)
                                    .then(function (data) {
                                        return data[0]['fileName'];
                                    })
                                    .catch(function (e) {
                                        console.log('Failed to get fanart:');
                                        console.log(e);
                                        return '';
                                    }),
                                tvdbClient.getSeriesEpisode(tvdbSeriesId, 1, 1, languageId)
								    .then(function (data) {
									    return data['firstAired'];
									})
                                    .catch(function (e) {
                                        console.log('Failed to get first episode data:');
                                        console.log(e);
                                        return '';
                                    }),
                                tvdbClient.getSeriesSummaryById(tvdbSeriesId, languageId)
                                    .then(function (data) {
                                        if (data['airedSeasons'].includes("0")) {
                                            return data['airedSeasons'].length - 1;
                                        } else {
                                            return data['airedSeasons'].length;
                                        }

                                    })
                                    .catch(function (e) {
                                        console.log('Failed to get series season summary data:');
                                        console.log(e);
                                        return '';
                                    })
                            ]).then(function (data) {
                                data[0]['poster'] = data[1];
                                data[0]['fanart'] = data[2];
                                data[0]['firstEpisodeDate'] = data[3];
                                data[0]['airedSeasons'] = data[4];
                                return data[0];
                            });
                        });

                        const tvmazePromise = tvmazeClient.getSeriesByTVDBId(tvdbSeriesId).then(function (data) {
                            return tvmazeClient.getSeasons(data['id']).then(function (seasons) {
                                data['seasons'] = seasons;
                                return data;
                            });
                        }).catch(function () {
                            return {};
                        });

                        var countryCodeDict = {
                            "+Globosat": "BR",
                            "1+1": "UA",
                            "13ème rue": "FR",
                            "13th Street Germany": "DE",
                            "2BE": "BE",
                            "2×2": "UA",
                            "7mate": "AU",
                            "A&E": "US",
                            "ABC (AU)": "AU",
                            "ABC (US)": "US",
                            "ABC Family": "US",
                            "ABC iview": "AU",
                            "ABC1": "UK",
                            "ABC2": "AU",
                            "ABC3": "AU",
                            "Adult Swim": "US",
                            "AHC": "US",
                            "Al Jazeera America": "US",
                            "Amazon": "US",
                            "Amazon (Japan)": "JP",
                            "AMC": "US",
                            "Animal Planet": "US",
                            "Animax": "JP",
                            "Anime Network": "US",
                            "Apple Music": "US",
                            "Arena": "AU",
                            "AT-X": "JP",
                            "AXS TV": "US",
                            "Bandai Channel": "JP",
                            "BBC": "UK",
                            "BBC ALBA": "UK",
                            "BBC News": "UK",
                            "BBC One": "UK",
                            "BBC Parliament": "UK",
                            "BBC Prime": "UK",
                            "BBC Three": "UK",
                            "BBC Two": "UK",
                            "BBC World News": "UK",
                            "BET": "US",
                            "Big Ten Network": "US",
                            "Blackpills": "US",
                            "Bloomberg Television": "US",
                            "Boomerang": "US",
                            "Bravo (CA)": "CA",
                            "Bravo (UK)": "UK",
                            "Bravo (US)": "US",
                            "BYU Television": "US",
                            "C-Span": "US",
                            "Canadian Learning Television": "CA",
                            "Cartoon Network": "US",
                            "Cartoon Network (UK)": "UK",
                            "Cartoon Network Australia": "AU",
                            "CBeebies": "UK",
                            "CBS": "US",
                            "CBS All Access": "US",
                            "Channel 4": "UK",
                            "Channel 5 (UK)": "UK",
                            "Cinemax": "US",
                            "CMT": "US",
                            "CNBC": "US",
                            "CNN": "US",
                            "Comedy Central (UK)": "UK",
                            "Comedy Central (US)": "US",
                            "Comic-Con HQ": "US",
                            "Cooking Channel": "US",
                            "Crackle": "US",
                            "CraveTV": "CA",
                            "Crime & Investigation Network (AU)": "AU",
                            "Crime & Investigation Network (UK)": "UK",
                            "Crime & Investigation Network (US)": "US",
                            "CuriosityStream": "US",
                            "CW Seed": "US",
                            "Dave": "UK",
                            "Destination America": "US",
                            "Discovery (US)": "US",
                            "Discovery Channel (AU)": "AU",
                            "Discovery Channel (CA)": "CA",
                            "Discovery Kids": "US",
                            "Disney Channel (UK)": "UK",
                            "Disney Channel (US)": "US",
                            "Disney Junior": "US",
                            "Disney Junior (UK)": "UK",
                            "Disney XD": "US",
                            "DIY Network": "US",
                            "DIY Network Canada": "CA",
                            "E!": "US",
                            "E! (CA)": "CA",
                            "EPIX": "US",
                            "ESPN": "US",
                            "Esquire Network": "US",
                            "Family (CA)": "CA",
                            "Food Network": "US",
                            "Food Network Canada": "CA",
                            "FOX (US)": "US",
                            "Fox Business": "US",
                            "Fox Channel (UK)": "UK",
                            "Freeform": "US",
                            "Fuji TV": "JP",
                            "FUNimation Channel": "US",
                            "fuse": "US",
                            "FX (US)": "US",
                            "FXX": "US",
                            "FYI": "US",
                            "go90": "US",
                            "Hallmark Channel": "US",
                            "HBO": "US",
                            "HBO Canada": "CA",
                            "HGTV": "US",
                            "HGTV Canada": "CA",
                            "History": "US",
                            "History (CA)": "CA",
                            "History (UK)": "UK",
                            "HLN": "US",
                            "Hulu": "US",
                            "IFC": "US",
                            "Investigation Discovery": "US",
                            "Ion Television": "US",
                            "ITV": "UK",
                            "ITV Encore": "UK",
                            "ITV Granada": "UK",
                            "ITV Wales": "UK",
                            "ITV1": "UK",
                            "ITV2": "UK",
                            "ITV3": "UK",
                            "ITV4": "UK",
                            "KBS": "KR",
                            "KBS Joy": "KR",
                            "KBS TV1": "KR",
                            "KBS TV2": "KR",
                            "KBS World": "KR",
                            "Lifetime (UK)": "UK",
                            "Lifetime (US)": "US",
                            "More4": "UK",
                            "MSNBC": "US",
                            "MTV (CA)": "CA",
                            "MTV (UK)": "UK",
                            "MTV (US)": "US",
                            "MTV2": "US",
                            "MTV3": "US",
                            "Nat Geo Wild": "US",
                            "National Geographic (AU)": "AU",
                            "National Geographic (UK)": "UK",
                            "National Geographic (US)": "US",
                            "NBA TV": "US",
                            "NBC": "US",
                            "NBCSN": "US",
                            "Netflix": "US",
                            "Network Ten": "AU",
                            "NFL Network": "US",
                            "NHK": "JP",
                            "Nick at Nite": "US",
                            "Nick Jr.": "US",
                            "Nickelodeon": "US",
                            "NickToons": "US",
                            "Nine Network": "AU",
                            "Noggin": "US",
                            "Oprah Winfrey Network": "US",
                            "Oxygen": "US",
                            "Paramount Network": "US",
                            "PBS": "US",
                            "PBS Kids Sprout": "US",
                            "Red Bull TV": "US",
                            "Rooster Teeth": "US",
                            "RTÉ One": "IE",
                            "RTÉ Two": "IE",
                            "RTÉjr": "IE",
                            "SBS (AU)": "AU",
                            "SBS (KR)": "KR",
                            "Science Channel": "US",
                            "SciFi": "US",
                            "Seeso": "US",
                            "Seven Network": "AU",
                            "Showtime": "US",
                            "Sky Arts": "UK",
                            "Sky Atlantic (UK)": "UK",
                            "Sky Cinema (UK)": "UK",
                            "Smithsonian Channel": "US",
                            "Smithsonian Channel (CA)": "CA",
                            "Sony Crackle": "US",
                            "Space": "US",
                            "Spike TV": "US",
                            "Stan": "CA",
                            "Starz!": "US",
                            "SundanceTV": "US",
                            "TBS": "US",
                            "TeenNick": "US",
                            "The CW": "US",
                            "The Weather Channel": "US",
                            "TLC": "US",
                            "TNT (US)": "US",
                            "Toei Channel": "JP",
                            "Tokyo Broadcasting System": "JP",
                            "Tokyo MX": "JP",
                            "Toon Disney": "US",
                            "Travel Channel": "US",
                            "truTV": "US",
                            "TV Land": "US",
                            "UK Entertainment Channel": "UK",
                            "UKTV Drama": "UK",
                            "UKTV Food": "UK",
                            "UKTV Gold": "UK",
                            "UKTV History": "UK",
                            "UKTV Style": "UK",
                            "USA Network": "US",
                            "Velocity": "US",
                            "VH1": "US",
                            "Viceland (CA)": "CA",
                            "Viceland (UK)": "UK",
                            "Viceland (US)": "US",
                            "VRV": "US",
                            "WE tv": "US",
                            "WWE Network": "US",
                            "YouTube": "US",
                            "YouTube Red": "US"
                        };

                        Promise.all([tvdbPromise, tvmazePromise]).then(function (data) {
                            const tvdbData = data[0];
                            const tvmazeData = data[1];

                            const form = pageContent.find('div.center.thin > div:nth-child(2) > div > form');
                            // first table elements go straight down
                            const firstTable = form.find('table:nth-child(3) > tbody > tr > td > input[type="text"]');
                            if (tvdbData['seriesName']) {
                                firstTable[0].value = tvdbData['seriesName'];
                            } else {
                                throw 'Series name is empty on TVDB; please fix the language ID.';
                            }
                            if (tvdbData['banner']) {
                                firstTable[1].value = '//cdn2.broadcasthe.net/tvdb/banners/' + tvdbData['banner'];
                            } else if (!firstTable[1].value) {
                                firstTable[1].value = 'https://cdn2.broadcasthe.net/https/i.imgur.com/hIq9qAn.png';
                            }
                            if (tvdbData['imdbId']) {
                                firstTable[2].value = 'http://imdb.com/title/' + tvdbData['imdbId'];
                            }
                            if (!tvdbSeriesLink || languageId == 7) {
                                firstTable[3].value = 'http://thetvdb.com/?tab=series&id=' + tvdbSeriesId + '&lid=7';
                            }
                            //firstTable[4].value = tvrage;
                            if (tvdbData['fanart']) {
                                firstTable[5].value = '//cdn2.broadcasthe.net/tvdb/banners/' + tvdbData['fanart'];
                            } else if (!firstTable[5].value) {
                                firstTable[5].value = 'https://cdn2.broadcasthe.net/https/i.imgur.com/55K4Dww.png';
                            }
                            //firstTable[6].value = youtube;
                            if (tvdbData['poster']) {
                                firstTable[7].value = '//cdn2.broadcasthe.net/tvdb/banners/' + tvdbData['poster'];
                            } else if (!firstTable[7].value) {
                                firstTable[7].value = 'https://cdn2.broadcasthe.net/https/i.imgur.com/qHx6IsI.png';
                            }

                            // second table elements go across then down
                            const secondTable = form.find('table:nth-child(4) > tbody > tr > td > input[type="text"]');
                            if (tvdbData['airsDayOfWeek']) {
                                secondTable[0].value = tvdbData['airsDayOfWeek'];
                            }
                            if (tvdbData['airsTime']) {
                                secondTable[1].value = tvdbData['airsTime'];
                            }
                            if (tvdbData['rating']) {
                                secondTable[2].value = tvdbData['rating'];
                            }
                            if (tvmazeData['network'] && tvmazeData['network']['country'] && tvmazeData['network']['country']['code']) {
                                secondTable[3].value = tvmazeData['network']['country']['code'];
                            } else if (!secondTable[3].value && countryCodeDict[tvdbData['network']]) {
                                secondTable[3].value = countryCodeDict[tvdbData['network']];
                            }
                            if (tvdbData['firstAired']) {
                                secondTable[4].value = tvdbData['firstAired'];
                            } else if (!secondTable[4].value && tvdbData['firstEpisodeDate']) {
                                secondTable[4].value = tvdbData['firstEpisodeDate'];
                            }
                            if (tvdbData['network']) {
                                secondTable[5].value = tvdbData['network'];
                            }
                            if (tvdbData['runtime']) {
                                secondTable[6].value = tvdbData['runtime'];
                            }
                            if (tvdbData['siteRating']) {
                                secondTable[7].value = tvdbData['siteRating'];
                            }
                            if (tvdbData['airedSeasons']) {
                                secondTable[8].value = tvdbData['airedSeasons'];
                            }
                            if (tvdbData['status']) {
                                secondTable[9].value = tvdbData['status'];
                            }
                            if (tvmazeData['type']) {
                                secondTable[10].value = tvmazeData['type'];
                            }
                            secondTable[11].value = tvdbSeriesId;
                            if (tvmazeData['externals'] && tvmazeData['externals']['tvrage']) {
                                secondTable[12].value = tvmazeData['externals']['tvrage'];
                            }
                            return tvdbData['overview'];
                        }).then(function (overview) {
                            if (overview) {
                                alertify.log('Adding series overview for series ID ' + tvdbSeriesId);
                                return Promise.resolve($.post('https://broadcasthe.net/series.php', {
                                    seriesid: seriesId,
                                    action:   'edit_summary',
                                    btn_body: overview
                                }));
                            } else {
                                alertify.error('No overview available on TVDB for series ID ' + tvdbSeriesId);
                                return false;
                            }
                        }).then(function () {
                            alertify.success('Autofill for series ID ' + tvdbSeriesId + ' complete.');
                        }).catch(function (err) {
                            errorHandler('Autofill for series ID ' + tvdbSeriesId + ' failed', err);
                        });

                        return false;
                    }
                }).prependTo(row);
            }

            run(noBannerLinks, 'noBannerLinks');
            run(autofillSeriesPage, 'autofillSeriesPage');
        }

        function modifyTorrentEditPage() {
            let currentOrigin;

            function internalModifier() {
                if (!currentOrigin) {
                    const l = document.getElementById('origin');
                    currentOrigin = l.options[l.selectedIndex].value;
                }
                window.Origin = exportFunction(function () {
                    const l = document.getElementById('origin');
                    const origin = l.options[l.selectedIndex].value;
                    let outputStr = '';
                    if (origin === 'Mixed') {
                        outputStr = 'You can\'t upload a season pack with mixed origin without prior staff consent!';
                    } else if (origin === 'Internal') {
                        outputStr = 'Multiplier set to 2.0x.';
                    }

                    if (origin !== window.currentOrigin && origin === 'Internal') {
                        $('#internal').prop('checked', true);
                        const torrentId = url.match(/(?!torrent)id=([0-9]+)/)[1];
                        $.post('https://broadcasthe.net/torrents.php', {
                            action:    'takemulti',
                            torrentid: torrentId,
                            upload:    2
                        });
                        $('select[name="upload"]').val('2');
                    }
                    document.getElementById('mixedOriginWarning').innerHTML = outputStr;
                    currentOrigin = origin;
                }, window);
            }
			
            modifyTorrentUploadPage();
            run(internalModifier, 'internalModifier');
        }

        function modifyTorrentUploadPage() {

            function autoTorrentFix() {
                $('#post').before($('<input>', {
                    id:    'autofix',
                    type:  'button',
                    value: 'Auto fix torrent',
                    click: function () {
                        try {
                            const containerSelect = document.querySelector('#format');
                            const codecSelect = document.querySelector('#bitrate');
                            const sourceSelect = document.querySelector('#media');
                            const resolutionSelect = document.querySelector('#resolution');
                            const originSelect = document.querySelector('#origin');

                            const log = document.querySelector('#release_desc').value;
                            const mediaInfos = MediaInfo.getAllMediaInfos(log);
                            const mi = mediaInfos[0];

                            let container = mi.getContainer();
                            let videoCodec = mi.getVideoCodec();
                            const audioCodec = mi.getPrimaryAudioCodec();
                            let resolution = mi.getVideoResolution();
                            const origin = originSelect.options[originSelect.selectedIndex].text;
                            let source = '';

                            const releaseNameBox = document.querySelector('#scenename');
                            let releaseName = releaseNameBox.value;

                            const oldData = {};
                            oldData['Release name'] = releaseName;
                            oldData['Container'] = containerSelect.options[containerSelect.selectedIndex].text;
                            oldData['Codec'] = codecSelect.options[codecSelect.selectedIndex].text;
                            oldData['Source'] = sourceSelect.options[sourceSelect.selectedIndex].text;
                            oldData['Origin'] = origin;
                            oldData['Resolution'] = resolutionSelect.options[resolutionSelect.selectedIndex].text;

                            releaseName = releaseName.replace(/^\.*/, '');
                            releaseName = releaseName.replace(/^F1/, 'Formula.1');
                            releaseName = releaseName.replace(/Prelims/i, 'Preliminaries');
                            releaseName = releaseName.replace(/\(|\)|:|SD/g, '.');
                            releaseName = releaseName.replace(/&/g, '.and.');
                            releaseName = releaseName.replace(/BDRip|Bluray/, 'BluRay');
                            releaseName = releaseName.replace(/\.s([0-9]+)e([0-9]+)\./, ".S$1E$2.");
                            releaseName = Latinise.latinise(releaseName);

                            const releaseNameRes = releaseName.match(/\.([0-9]+)(p|i)\./);
                            if (releaseNameRes) {
                                if (resolution == 'SD') {
                                    releaseName = releaseName.replace(releaseNameRes[1] + releaseNameRes[2], '');
                                } else {
                                    releaseName = releaseName.replace(releaseNameRes[1] + releaseNameRes[2], resolution);
                                }
                            }

                            const sourceCasing = {
                                'HDTV':    /hdtv/i,
                                'PDTV':    /pdtv/i,
                                'DSR':     /dsr/i,
                                'DVDRip':  /dvdrip/i,
                                'TVRip':   /tvrip/i,
                                'VHSRip':  /vhsrip/i,
                                'BluRay':  /bluray/i,
                                'BDRip':   /bdrip/i,
                                'DVD5':    /dvd5/i,
                                'DVD9':    /dvd9/i,
                                'WEBRip':  /webrip/i,
                                'WEB-DL':  /web.?dl/i,
                                'WEB':     /web/i,
                                'BD25':    /bd25/i,
                                'BD50':    /bd50/i,
                                'Mixed':   /mixed/i,
                                'Unknown': /unknown/i,
                                'Remux':   /remux/i,
                            };

                            const sourceCases = Object.keys(sourceCasing);
                            for (let i = 0; i < sourceCases.length; i++) {
                                releaseName = releaseName.replace(sourceCasing[sourceCases[i]], sourceCases[i]);
                            }

                            releaseName = releaseName.replace(/\\|'|"/g, '');
                            releaseName = releaseName.replace(/\s|\.-\./g, '.');
                            releaseName = releaseName.replace(/\.{2,}/g, '.');

                            const releaseRegex = /(.*)(HDTV|PDTV|DSR|DVDRip|TVRip|VHSRip|BluRay\.Remux|BluRay|BDRip|WEBRip|WEB-DL|WEB|BD25|BD50|Mixed|Unknown)(?:\.(.*))?\.(.*?(?:-Hi10P)?)(?:[-.]|$)(.*)/i;
                            const releaseMatch = releaseName.match(releaseRegex);

                            let tvTitle = document.querySelector('#title').value;
                            tvTitle = tvTitle.replace(/\\/g, '');
                            document.querySelector('#title').value = tvTitle;

                            // Don't modify scene release names or release names from P2P that are Japanese (likely to be anime) unless in a season pack
                            const shouldModifyReleaseName = ((origin != "Scene" && !(origin == "P2P" && document.querySelector('#country').selectedIndex == 52)) || /^Season [0-9]+$/.test(tvTitle));
                            
                            if (releaseMatch) {
                                source = releaseMatch[2];
                                if (source == 'WEB') {
                                    source = 'WEB-DL';
                                } else if (source == 'BDRip') {
                                    source = 'BluRay';
                                }
                                // Check if scene
                                if (shouldModifyReleaseName) {
                                    const group = releaseMatch[5] || 'NOGRP';
                                    let beginningOfReleaseName = releaseMatch[1];
                                    if (resolution != 'SD' && beginningOfReleaseName.indexOf(resolution) == -1) {
                                        beginningOfReleaseName += resolution + '.';
                                    }

                                    releaseName = beginningOfReleaseName + source + '.';

                                    if (audioCodec != 'MP3' && audioCodec != 'MP2') {
                                        releaseName += audioCodec + '.';
                                    }

                                    releaseName += videoCodec + '-' + group;
                                    releaseNameBox.value = releaseName;
                                }
                            }
                            if (resolution == '576p') {
                                resolution = 'SD';
                            }
                            if (videoCodec == 'x264') {
                                videoCodec = 'H.264';
                            } else if (videoCodec == 'DivX') {
                                videoCodec = 'DiVX';
                            } else if (videoCodec == 'Xvid') {
                                videoCodec = 'XViD';
                            } else if (videoCodec == 'x265') {
                                videoCodec = 'H.265';
                            } else if (container == 'VOB') {
                                videoCodec = 'DVD';
                            } else if (oldData['container'] == 'ISO') {
                                container = 'ISO';
                            }

                            if (source.includes('BluRay') && container != 'M2TS') {
                                source = 'Bluray';
                            }

                            selectDropdown('#format', container);
                            selectDropdown('#bitrate', videoCodec);
                            selectDropdown('#media', source);
                            selectDropdown('#resolution', resolution);

                            const newData = {};
                            newData['Release name'] = releaseNameBox.value;
                            newData['Container'] = containerSelect.options[containerSelect.selectedIndex].text;
                            newData['Codec'] = codecSelect.options[codecSelect.selectedIndex].text;
                            newData['Source'] = sourceSelect.options[sourceSelect.selectedIndex].text;
                            newData['Resolution'] = resolutionSelect.options[resolutionSelect.selectedIndex].text;

                            const differences = [];
                            const differenceKeys = Object.keys(newData);

                            for (let i = 0; i < differenceKeys.length; i++) {
                                const key = differenceKeys[i];
                                const newValue = newData[key];
                                const oldValue = oldData[key];
                                if (oldValue != newValue) {
                                    if (key == 'Release name') {
                                        const diffTexts = Utils.getHTMLDiff(oldValue, newValue);
                                        differences.push('<div class="tab1">&#8226; ' + key + ':<br><div class="tab2">' + diffTexts[0] + '</div><br><div class="tab2">' + diffTexts[1] + '</div></div>');
                                    } else {
                                        differences.push('<div class="tab1">&#8226; ' + key + ':<br><div class="tab2">' + oldData[key] + ' &#8594; ' + newData[key] + '</div></div>');
                                    }
                                }
                            }
                            if (differences.length > 0) {
                                alertify.success('Changed the following items:<br>' + differences.join('<br>'));
                            } else {
                                alertify.error('No changes made.');
                            }
                            return false;
                        } catch (err) {
                            errorHandler('Failed to automatically fix torrent', err);
                            return false;
                        }
                    }
                }));
            }

            function torrentBBcodeGenerator() {
                // Same thing but for torrents
                const tech_specs_table = $('#table_manual_upload_2').find('tbody');
                const textbox = $('#release_desc');
                const row = document.createElement('tr');
                let label = $('<td>', {
                    class: 'label',
                    text:  'Add Staff Note'
                }).appendTo(row);
                const container = document.createElement('td');
                row.appendChild(container);
                const notebox = $('<textarea>', {
                    cols:  '60',
                    rows:  '2',
                    style: 'vertical-align:middle;height:auto;width:auto;'
                }).appendTo(container);
                $('<input>', {
                    type:  'button',
                    value: 'Insert staff note',
                    style: 'vertical-align:middle;',
                    click: function () {
                        const oldText = textbox.val();
                        let note = notebox.val();
                        if (!note.endsWith('//' + myUsername)) {
                            note += ' //' + myUsername;
                        }
                        textbox.val('[b][color=red][size=5]' + note + '[/size][/color][/b]\n\n' + oldText);
                        notebox.val('');
                    }
                }).appendTo(container);
                tech_specs_table.append(row);
            }

            function blockContainerChange() {
                // Prevent autofill from changing MP4 -> SD or MKV -> 720p
                window.Format = exportFunction(function () {
                    console.log('Blocked format/resolution auto change');
                }, window);
            }

            function checkReleaseNameLength() {
                const releaseNameBox = $('#scenename');
                const lengthWarning = $('<div>', {
                    style: 'color: red; font-weight: bold'
                });
                lengthWarning.hide();
                releaseNameBox.after(lengthWarning);
                releaseNameBox.on('input', function (e) {
                    if (e.target.value.length > 100) {
                        lengthWarning.text('Warning: Release name is ' + e.target.value.length + ' characters and will be truncated to 100.');
                        lengthWarning.show();
                    } else {
                        lengthWarning.hide();
                    }
                });
            }

			run(autoTorrentFix, 'autoTorrentFix');
            run(checkReleaseNameLength, 'checkReleaseNameLength');
            run(torrentBBcodeGenerator, 'torrentBBcodeGenerator');
            run(blockContainerChange, 'blockContainerChange');
        }

        function modifyTorrentDetailPage() {
            function autofillTorrentGroupDescription() {
                function generateTorrentDescription(episodeName, season, episode, firstAired, overview, episodeImage) {
                    let desc = '[b]Episode Name:[/b] ' + (episodeName || '');
                    desc += '\n[b]Season:[/b] ' + season;
                    desc += '\n[b]Episode:[/b] ' + episode;
                    desc += '\n[b]Aired:[/b] ' + firstAired || '';
                    desc += '\n\n[b]Episode Overview:[/b] ';
                    if (overview) {
                        desc += '\n' + overview;
                    }
                    if (episodeImage) {
                        desc += '\n\n[b]Episode Image[/b] \n[img=https://cdn2.broadcasthe.net/tvdb/banners/' + episodeImage + ']';
                    }
                    return desc;
                }


                const linkbox = $('div.linkbox')[0];
                $('<a>', {
                    text:  '[Autofill description]',
                    href:  '#',
                    click: function () {
                        if (/^BroadcasTheNet$/.test(document.title)) {
                            alertify.error('Fix this torrent before autofilling!');
                            return false;
                        }
                        const groupId = url.match(/(?:&|\?)(?!torrent)id=([0-9]+)/)[1];
                        let episodeName = document.querySelector('#content > div > h2 > a > img').title || '';
                        let episodeSE = episodeName.match(/.*S([0-9]+)E([0-9]+).*/);
                        let doubleEpisodeSE = episodeName.match(/.*S([0-9]+)E([0-9]+)E([0-9]+).*/);
                        let seasonPackNum = episodeName.match(/.*Season (\d*)$/);
                        let episodePromptPromise;
                        if (episodeSE == undefined && seasonPackNum == undefined && doubleEpisodeSE == undefined) {
                            episodePromptPromise = alertify.okBtn('OK')
                                .cancelBtn('Cancel')
                                .defaultValue('S00E01')
                                .prompt('Could not find a season/episode in the episode name. Please provide one manually.')
                                .then(function (resolvedValue) {
                                    resolvedValue.event.preventDefault();
                                    if (resolvedValue.buttonClicked == 'ok') {
                                        episodeName = resolvedValue.inputValue || '';
                                        episodeSE = episodeName.match(/.*S([0-9]+)E([0-9]+).*/);
                                        doubleEpisodeSE = episodeName.match(/.*S([0-9]+)E([0-9]+)E([0-9]+).*/);
                                        seasonPackNum = episodeName.match(/.*Season (\d*)$/);
                                    }
                                    if (episodeSE == undefined && seasonPackNum == undefined && doubleEpisodeSE == undefined) {
                                        throw 'Autofill only supports numbered episodes or season packs.';
                                    }
                                });
                        } else {
                            episodePromptPromise = Promise.resolve();
                        }

                        episodePromptPromise.then(function () {
                            let torrentSeason = -1;
                            let torrentEpisode = -1;
                            let torrentSecondEpisode = -1;
                            if (doubleEpisodeSE) {
                                doubleEpisodeSE = doubleEpisodeSE.map(function (num) {
                                    if (num == null || num == undefined) {
                                        return -1;
                                    }
                                    return parseInt(num);
                                });
                                torrentSeason = doubleEpisodeSE[1];
                                torrentEpisode = doubleEpisodeSE[2];
                                torrentSecondEpisode = doubleEpisodeSE[3];
                            } else if (seasonPackNum) {
                                seasonPackNum = parseInt(seasonPackNum[1]);
                                torrentSeason = seasonPackNum;
                            } else {
                                episodeSE = episodeSE.map(function (num) {
                                    if (num == null || num == undefined) {
                                        return -1;
                                    }
                                    return parseInt(num);
                                });
                                torrentSeason = episodeSE[1];
                                torrentEpisode = episodeSE[2];
                            }

                            let loginInfo = tvdbLogin();
                            if (!loginInfo) {
                                return false;
                            }

                            const apikey = loginInfo.apikey;
                            const username = loginInfo.username;
                            const userkey = loginInfo.userkey;

                            const client = new TVDB();

                            const getYearRegex = /([0-9]+)\-[0-9]+\-[0-9]+/;

                            const btnSeriesId = document.querySelector('#content > div > h2 > a').href.split('?id=')[1];
                            let tvdbSeriesId = 0;
                            let languageId = 7;
                            let p = Promise.resolve($.get('https://broadcasthe.net/series.php?action=edit_info&seriesid=' + btnSeriesId))
                                .then(function (data) {
                                    const pageContent = $(data).find('#content');
                                    tvdbSeriesId = parseInt(pageContent
                                        .find('form > table:nth-child(4) > tbody > tr:nth-child(6) > td:nth-child(4) > input[type="text"]')
                                        .val());
                                    const tvdbSeriesLink = pageContent
                                        .find('table:nth-child(3) > tbody > tr:nth-child(4) > td.tdleft > input[type="text"]')
                                        .val();
                                    if (tvdbSeriesLink) {
                                        const tvdbLinkMatch = tvdbSeriesLink.match(/thetvdb\.com\/\?tab=series&id=([0-9]+)(?:&lid=([0-9]+))?/);
                                        if (tvdbLinkMatch) {
                                            tvdbSeriesId = parseInt(tvdbLinkMatch[1]);
                                            if (tvdbLinkMatch[2]) {
                                                languageId = parseInt(tvdbLinkMatch[2]);
                                            }
                                        }
                                    }

                                    if (isNaN(tvdbSeriesId) || tvdbSeriesId == 0) {
                                        throw 'Cannot autofill - series does not have a TVDB ID.';
                                    }

                                    alertify.log('Torrent autofill is currently working...');

                                    return tvdbSeriesId;
                                });

                            if (torrentSecondEpisode !== -1) {
                                // double episode
                                p = p.then(function () {
                                    return client.login(apikey, username, userkey)
                                        .then(function () {
                                            return Promise.all([
                                                client.getSeasonPoster(tvdbSeriesId, torrentSeason, 'http://cdn.broadcasthe.net/common/posters/noposter.png'),
                                                client.getSeriesEpisode(tvdbSeriesId, torrentSeason, torrentEpisode),
                                                client.getSeriesEpisode(tvdbSeriesId, torrentSeason, torrentSecondEpisode)
                                            ]);
                                        }).then(function (data) {
                                            let desc = generateTorrentDescription(data[1]['episodeName'], torrentSeason, torrentEpisode, data[1]['firstAired'], data[1]['overview'], data[1]['filename']);
                                            desc += '\n\n' + generateTorrentDescription(data[2]['episodeName'], torrentSeason, torrentSecondEpisode, data[2]['firstAired'], data[2]['overview'], data[2]['filename']);
                                            const ret = [{
                                                action:  'takegroupedit',
                                                groupid: groupId,
                                                image:   data[0],
                                                body:    desc,
                                                summary: ''
                                            }, null];
                                            if (data[1]['firstAired']) {
                                                ret[1] = {
                                                    action:   'nonwikiedit',
                                                    groupid:  groupId,
                                                    year:     data[1]['firstAired'].match(getYearRegex)[1],
                                                    artistid: btnSeriesId
                                                };
                                            }
                                            return ret;
                                        });
                                });
                            } else if (torrentEpisode !== -1) {
                                // episode
                                p = p.then(function () {
                                    return client.login(apikey, username, userkey)
                                        .then(function () {
                                            return Promise.all([
                                                client.getSeasonPoster(tvdbSeriesId, torrentSeason, 'http://cdn.broadcasthe.net/common/posters/noposter.png'),
                                                client.getSeriesEpisode(tvdbSeriesId, torrentSeason, torrentEpisode)
                                            ]);
                                        }).then(function (data) {
                                            const desc = generateTorrentDescription(data[1]['episodeName'], torrentSeason, torrentEpisode, data[1]['firstAired'], data[1]['overview'], data[1]['filename']);
                                            const ret = [{
                                                action:  'takegroupedit',
                                                groupid: groupId,
                                                image:   data[0],
                                                body:    desc,
                                                summary: ''
                                            }, null];
                                            if (data[1]['firstAired']) {
                                                ret[1] = {
                                                    action:   'nonwikiedit',
                                                    groupid:  groupId,
                                                    year:     data[1]['firstAired'].match(getYearRegex)[1],
                                                    artistid: btnSeriesId
                                                };
                                            }
                                            return ret;
                                        });
                                });
                            } else {
                                // season pack
                                p = p.then(function () {
                                    return client.login(apikey, username, userkey)
                                        .then(function () {
                                            return Promise.all([
                                                client.getSeasonPoster(tvdbSeriesId, torrentSeason, 'http://cdn.broadcasthe.net/common/posters/noposter.png'),
                                                client.getSeriesSeason(tvdbSeriesId, torrentSeason)
                                            ]);
                                        }).then(function (data) {
                                            data[1].sort(function (e1, e2) {
                                                return e1['airedEpisodeNumber'] - e2['airedEpisodeNumber'];
                                            });
                                            const desc = data[1].map(function (episode) {
                                                return generateTorrentDescription(episode['episodeName'], episode['airedSeason'], episode['airedEpisodeNumber'], episode['firstAired'], episode['overview'], episode['filename']);
                                            }).reduce(function (previousValue, currentValue) {
                                                return previousValue + currentValue + '\n\n';
                                            }, '');
                                            const ret = [{
                                                action:  'takegroupedit',
                                                groupid: groupId,
                                                image:   data[0],
                                                body:    desc,
                                                summary: ''
                                            }, null];
                                            if (data[1][0]['firstAired']) {
                                                ret[1] = {
                                                    action:   'nonwikiedit',
                                                    groupid:  groupId,
                                                    year:     data[1][0]['firstAired'].match(getYearRegex)[1],
                                                    artistid: btnSeriesId
                                                };
                                            }
                                            return ret;
                                        });
                                });
                            }
                            return p.then(function (data) {
                                return Promise.resolve($.post('https://broadcasthe.net/torrents.php', data[0]))
                                    .then(function () {
                                        return data[1];
                                    });
                            }).then(function (data) {
                                if (data) {
                                    return Promise.resolve($.post('https://broadcasthe.net/torrents.php', data));
                                } else {
                                    alertify.error('Could not autofill aired year since it was unavailable on TVDB.');
                                    return true;
                                }
                            }).then(function () {
                                return Promise.resolve($.get(url));
                            }).then(function (data) {
                                const dom = $(data);
                                $('div.body:eq(1)').replaceWith(dom.find('div.body:eq(1)'));
                                $('#content').find('> div > h2').replaceWith(dom.find('#content > div > h2'));
                                alertify.success('Torrent autofill complete - changes are now visible.');
                            });
                        }).catch(function (err) {
                            errorHandler('Autofill for torrent group ' + groupId + ' failed', err);
                        });
                        return false;
                    }
                }).prependTo(linkbox);
            }

            run(autofillTorrentGroupDescription, 'autofillTorrentGroup');

            $('tr.pad > td > div.linkbox').each(function () {
                // Hide obnoxious file list
                const $this = $(this);
                const filelist = $this.next().next();
                $('<a>', {
                    text:  '[File List]',
                    href:  '#',
                    click: function () {
                        filelist.toggle();
                        return false;
                    }
                }).prependTo($this);

                // Count number of files in torrents
                const filenum = filelist.find('tbody').children().length - 1;
                let output = ' (' + filenum + ' file';
                if (filenum > 1) {
                    output += 's';
                }
                output += ')';
                $this.parent().parent().prev().prev().children()[1].innerHTML += output;

                if (filenum > 50) {
                    filelist.hide();
                }
            });

            // Add search links for torrents
            $('table.torrent_table > tbody > tr > td:nth-child(1) > a').each(function () {
                const releaseNode = this.parentNode.parentNode.nextSibling.nextSibling.children[0];
                const relQueryObj = $(releaseNode.children[0]);
                run(function () {
                    relQueryObj.append($('<br>'));
                }, 'torrentSearchesNewLine');

                // Only add log button if this user has log access
                if ($('input[placeholder=\'Log\']').length > 0) {
                    run(function (that) {
                        const linkbox = that.previousSibling.previousSibling.children;
                        const reqLink = linkbox[linkbox.length - 1].href.split('=');
                        const torrentId = reqLink[reqLink.length - 1];
                        relQueryObj.append($('<a>', {
                            href: 'https://broadcasthe.net/log.php?search=' + torrentId,
                            text: '[Log] '
                        }));
                    }, 'searchLog', this);
                }

                if (this.innerHTML.endsWith('Scene')) {
                    const releaseName = Utils.cleanReleaseName(releaseNode.childNodes[0].nodeValue);

                    const providerNames = Object.keys(searchProviders);

                    for (let i = 0; i < providerNames.length; i++) {
                        const searchName = providerNames[i];
                        const searchUrl = searchProviders[searchName];
                        run(function (searchUrl, searchName, releaseName) {
                            let newSearchUrl = searchUrl.replace(/__RELEASE_NAME__/, releaseName);
                            newSearchUrl = newSearchUrl.replace(/__ENCODED_RELEASE_NAME__/, encodeURIComponent(releaseName));
                            relQueryObj.append($('<a>', {
                                href: newSearchUrl,
                                text: '[' + searchName + '] '
                            }));
                        }, 'search' + searchName, searchUrl, searchName, releaseName);
                    }
                }
            });

            // Add reseed button to all torrents
            run(function () {
                $('td > blockquote').prev().filter(function () {
                    const prevId = $(this).attr('id');
                    return prevId && prevId.indexOf('subtitle') > -1;
                }).each(function () {
                    const $this = $(this);
                    const torrentId = $this.attr('id').match('subtitle_dialog_([0-9]+)')[1];
                    const groupId = url.match(/(?:&|\?)(?!torrent)id=([0-9]+)/)[1];
                    $this.after($('<div id="linkbox" style="text-align: center"><form method="post" action=""><input type="hidden" id="action" name="action" value="reseed"><input type="hidden" id="gid" name="gid" value="' + groupId + '"><input type="hidden" id="tid" name="tid" value="' + torrentId + '"><input type="submit" id="reseed" name="reseed" value="Request a reseed"></form></div>'));
                });
            }, 'addReseedButton');

            // Copy release link to clipboard
            $('a[title="RequestLink"]').on('click', function(event) {
                event.stopPropagation();
                copyToClipboard(this.href);
                return false;
            });
        }

        function modifyRequestPage() {
            function requestBBcodeGenerator() {
                // Same thing but for requests
                const submit_button = $('tr').last();
                const textbox = $('textarea[name="description"]');
                const row = document.createElement('tr');
                let label = $('<td>', {
                    class: 'label',
                    text:  'Add Staff Note'
                }).appendTo(row);
                const container = document.createElement('td');
                row.appendChild(container);
                const notebox = $('<textarea>', {
                    cols:  '74',
                    rows:  '2',
                    style: 'vertical-align:middle;height:auto;width:auto;'
                }).appendTo(container);
                $('<input>', {
                    type:  'button',
                    value: 'Insert staff note',
                    style: 'vertical-align:middle;',
                    click: function () {
                        const oldText = textbox.val();
                        let note = notebox.val();
                        if (!note.endsWith('//' + myUsername)) {
                            note += ' //' + myUsername;
                        }
                        textbox.val('[b][color=red][size=5]' + note + '[/size][/color][/b]\n\n' + oldText);
                        notebox.val('');
                    }
                }).appendTo(container);
                submit_button.before(row);
            }

            run(requestBBcodeGenerator, 'requestBBcodeGenerator');
        }

        function addPMToUsernames() {
            $('#user .box.pad.center tbody tr > td a,' +
                '#torrents .torrent_table table ~ div + blockquote > a,' +
                '#torrents .torrent_table table ~ table + blockquote > a,' +
                '#torrents .torrent_table .torrent > td:nth-child(3) > .nobr > a')
                .each(function () {
                    const $this = $(this);
                    const id = $this.attr('href').replace('user.php?id=', '');
                    $this.after(' <a href="inbox.php?action=compose&to=' + id + '">[PM]</a> ');
                });
        }

        function modifyMassDeletePage() {
            // cache series data when mass deleting
            let seriesData;

            function expandMassDeleteReleaseNames() {
                $('#content').find('> div.thin.center > table > tbody > tr > td:nth-child(5)').each(function () {
                    const $this = $(this);
                    $this.addClass('mass-delete-releasename');
                    const span = $this.find('span');
                    const releaseName = span.attr('title');
                    span.remove();
                    $this.text(releaseName);
                });
            }

            function seasonPackMassDelete() {

                const row = document.createElement('tr');
                const container = document.createElement('td');
                container.setAttribute('colspan', '7');
                row.appendChild(container);
                $('<span>', {
                    class: 'label',
                    text:  'URL of season pack to autoclean: ',
                    style: 'vertical-align:middle;'
                }).appendTo(container);
                const urlbox = $('<input>', {
                    id:    'season_pack_url',
                    style: 'vertical-align:middle;'
                });
                urlbox.attr('size', 60);
                urlbox.appendTo(container);
                $('<input>', {
                    type:  'button',
                    value: 'Autoclean Season Pack',
                    style: 'vertical-align:middle;margin-left:5px',
                    click: function () {
                        const seasonPackUrl = urlbox.val();
                        const seriesId = parseInt(url.match(/seriesid=([0-9]+)&auth=/)[1]);
                        const seasonTorrentId = parseInt(seasonPackUrl.split('action=reqlink&id=')[1]);
                        if (isNaN(seasonTorrentId)) {
                            alertify.error('Not a valid link.');
                            urlbox.val('');
                            return false;
                        }

                        alertify.log('Trumping in progress...');

                        let p;
                        if (seriesData) {
                            console.log('Using cached data');
                            p = Promise.resolve(seriesData);
                        } else {
                            p = Promise.resolve($.get('https://broadcasthe.net/series.php?id=' + seriesId))
                                .then(function (data) {
                                    const dom = $(data);
                                    return $.makeArray(dom.find('#discog_table > tbody > tr > td.group.discog > div > strong > a.episode'))
                                        .filter(function (link) {
                                            return link.text.trim() !== 'S01E01' && link.text.trim() !== 'S01E01E02' && link.text.trim() !== 'S01E00';
                                        }).map(function (link) {
                                            return link.href;
                                        });
                                }).then(function (links) {
                                    alertify.log('Fetching torrent data. Please wait...');
                                    console.log('Grabbing info for ' + links.length + ' torrents');
                                    return Promise.all(links.map(function (link) {
                                        return Promise.resolve($.get(link))
                                            .reflect()
                                            .then(function (inspection) {
                                                if (inspection.isFulfilled()) {
                                                    console.log('Grabbed ' + link);
                                                    return inspection.value();
                                                } else {
                                                    console.error('Grabbing ' + link + ' failed: ' + inspection.reason());
                                                    throw 'Grabbing ' + link + ' failed: ' + inspection.reason();
                                                }
                                            }).catch(function () {
                                                return Promise.resolve($.get(link))
                                                    .reflect()
                                                    .then(function (inspection) {
                                                        if (inspection.isFulfilled()) {
                                                            console.log('Grabbed ' + link);
                                                            return inspection.value();
                                                        } else {
                                                            console.error('Grabbing ' + link + ' failed: ' + inspection.reason());
                                                            throw 'Grabbing ' + link + ' failed: ' + inspection.reason();
                                                        }
                                                    })
                                            });
                                    }));
                                }).then(function (data) {
                                    seriesData = data.reduce(function (previousValue, currentValue) {
                                        const dom = $(currentValue);
                                        dom.find('tr.pad > td > table:nth-child(3) > tbody > tr:nth-child(2) > td:nth-child(1)')
                                            .each(function (index, elem) {
                                                const enclosingTable = elem.parentNode.parentNode.parentNode.parentNode.parentNode;
                                                const torrentId = parseInt($(enclosingTable)
                                                    .attr('id')
                                                    .replace('torrent_', ''));
                                                previousValue[elem.innerHTML] = {
                                                    torrentId:   parseInt(torrentId),
                                                    releaseName: Utils.cleanReleaseName(enclosingTable.previousSibling.previousSibling.children[0].childNodes[0].nodeValue)
                                                };
                                            });
                                        return previousValue;
                                    }, {});
                                    return seriesData;
                                });
                        }
                        p.then(function (torrentData) {
                            return Promise.resolve($.get(seasonPackUrl))
                                .then(function (data) {
                                    const torrentGroupId = parseInt(data.match(/torrents\.php\?action=editgroup&(?:amp;)?groupid=([0-9]+)/)[1]);
                                    const dom = $(data).find('#torrent_' + seasonTorrentId);
                                    const filenames = $.makeArray(dom.find('td > table:nth-child(3) > tbody > tr')
                                        .not('.colhead_dark'))
                                        .map(function (row) {
                                            return row.querySelector('td').innerHTML;
                                        });
                                    const releaseName = Utils.cleanReleaseName(dom.prev()[0].children[0].childNodes[0].nodeValue);
                                    return {releaseName: releaseName, fileNames: filenames, groupId: torrentGroupId};
                                }).then(function (data) {
                                    return Promise.resolve($.get('https://broadcasthe.net/torrents.php?action=editgroup&groupid=' + data['groupId']))
                                        .then(function (groupData) {
                                            const dom = $(groupData);
                                            const torrentCategory = dom.find('#content > div.center.thin > div:nth-child(4) > form > table > tbody > tr:nth-child(1) > td:nth-child(2) > select > option:nth-child(2)');
                                            if (!torrentCategory.attr('selected')) {
                                                throw 'The provided link was not for a season.\n<a href="' + seasonPackUrl + '">' + seasonPackUrl + '</a>';
                                            }
                                            const torrentGroupName = dom.find('#content > div.center.thin > div:nth-child(8) > form > div > input[type="text"]:nth-child(4)')
                                                .attr('value');
                                            const seasonNum = parseInt(torrentGroupName.match(/Season ([0-9]+)/)[1]);
                                            let isSpecial = torrentGroupName.match(/Season [0-9]+ - (.+)|.*(Extras).*|.*(Specials).*/);
                                            if (isSpecial) {
                                                isSpecial = isSpecial[1] || isSpecial[2] || isSpecial[3];
                                            }
                                            data['seasonNum'] = seasonNum;
                                            data['isSpecial'] = isSpecial;
                                            return data;
                                        });
                                }).then(function (data) {
                                    const seasonNum = data['seasonNum'];
                                    const packFileNames = data['fileNames'];
                                    let isSpecial = data['isSpecial'];
                                    const torrentFileNamesArr = Object.keys(torrentData);
                                    const foundFileNames = torrentFileNamesArr.filter(function (fileName) {
                                        return packFileNames.indexOf(fileName) > -1;
                                    });
                                    const numDirfixesInPack = packFileNames.filter(function (fileName) {
                                        return /.*?\.DIRFIX\..*/.test(fileName);
                                    }).length;
                                    const numDirfixesFound = torrentFileNamesArr.filter(function (fileName) {
                                        return /.*?\.DIRFIX\..*/.test(torrentData[fileName]['releaseName']);
                                    }).length;
                                    if (foundFileNames.length == 0 && numDirfixesFound == 0 && numDirfixesInPack == 0) {
                                        throw 'No matching single episodes found.';
                                    } else if ((seasonNum > 1 && foundFileNames.length != packFileNames.length) || (!isSpecial && seasonNum == 1 && foundFileNames.length != packFileNames.length - 1)) {
                                        let confirmMessage = 'Warning: I found ' + foundFileNames.length + ' matching single episodes but there are ' + packFileNames.length + ' in the pack.';
                                        if (numDirfixesInPack > 0) {
                                            confirmMessage += ' However, there are ' + numDirfixesInPack + ' DIRFIXed single episodes in the pack (these will have to be deleted manually).';
                                        }
                                        confirmMessage += ' Delete anyway?';
                                        return alertify.okBtn('Yes, delete anyway')
                                            .cancelBtn('No, don\'t delete')
                                            .confirm(confirmMessage)
                                            .then(function (resolvedValue) {
                                                resolvedValue.event.preventDefault();
                                                console.log(resolvedValue);
                                                if (resolvedValue.buttonClicked == 'ok') {
                                                    return foundFileNames;
                                                } else {
                                                    throw 'Number of single episodes found differs from number of files in pack.';
                                                }
                                            })
                                    }
                                    return foundFileNames;
                                });
                        }).then(function (selectedFileNames) {
                            let trumpData = selectedFileNames.map(function (fileName) {
                                console.log('Trumping torrent ' + seriesData[fileName]['torrentId'] + ': ' + fileName);
                                return '&' + encodeURIComponent('torrentid[]') + '=' + seriesData[fileName]['torrentId'];
                            }).reduce(function (previousValue, currentValue) {
                                return previousValue + currentValue;
                            }, 'action=trump');
                            trumpData += '&reason=' + encodeURIComponent('Season Pack Release') + '&extra=' + encodeURIComponent(seasonPackUrl + ' //' + myUsername);
                            return alertify.okBtn('Yes, delete')
                                .cancelBtn('No, don\'t delete')
                                .confirm('Going to delete ' + selectedFileNames.length + ' torrents. Continue?')
                                .then(function (resolvedValue) {
                                    resolvedValue.event.preventDefault();
                                    if (resolvedValue.buttonClicked == 'ok') {
                                        return Promise.resolve($.post('https://broadcasthe.net/series.php', trumpData))
                                            .then(function () {
                                                return Promise.resolve($.get(url));
                                            }).then(function (data) {
                                                const dom = $(data);
                                                $('table').replaceWith(dom.find('table'));
                                                run(expandMassDeleteReleaseNames, 'expandMassDeleteReleaseNames');
                                                run(seasonPackMassDelete, 'seasonPackMassDelete');
                                                return selectedFileNames.length;
                                            });
                                    } else {
                                        throw 'Cancelled by user.';
                                    }
                                });
                        }).then(function (num) {
                            alertify.success('' + num + ' single episodes trumped.');
                        }).catch(function (err) {
                            errorHandler('Error trumping single episodes', err);
                        });
                        return false;
                    }
                }).appendTo(container);
                $('tr').last().after(row);

                $('#content').find('> div.thin.center > table > tbody > tr:nth-child(8) > td > input[type="submit"]')
                    .attr('onclick', 'return false')
                    .on('click', function () {
                        const checked = document.querySelectorAll('input[name="torrentid[]"]:checked');
                        let trumpData = Array.prototype.slice.call(checked).map(function (checkbox) {
                            return '&' + encodeURIComponent('torrentid[]') + '=' + checkbox.value;
                        }).reduce(function (previousValue, currentValue) {
                            return previousValue + currentValue;
                        }, 'action=trump');
                        const reasonSelector = document.querySelector('#content > div.thin.center > table > tbody > tr:nth-child(7) > td > select');
                        trumpData += '&reason=' + encodeURIComponent(reasonSelector.options[reasonSelector.selectedIndex].value) + '&extra=' + encodeURIComponent($('#content').find('> div.thin.center > table > tbody > tr:nth-child(7) > td > input')
                                .val());
                        alertify.okBtn('Yes').cancelBtn('No').confirm('Are you sure?').then(function (resolvedValue) {
                            resolvedValue.event.preventDefault();
                            if (resolvedValue.buttonClicked == 'ok') {
                                return Promise.resolve($.post('https://broadcasthe.net/series.php', trumpData))
                                    .then(function () {
                                        return Promise.resolve($.get(url));
                                    }).then(function (data) {
                                        const dom = $(data);
                                        $('table').replaceWith(dom.find('table'));
                                        run(expandMassDeleteReleaseNames, 'expandMassDeleteReleaseNames');
                                        run(seasonPackMassDelete, 'seasonPackMassDelete');
                                        return true;
                                    });
                            }
                        });
                        return false;
                    });
            }

            run(expandMassDeleteReleaseNames, 'expandMassDeleteReleaseNames');
            run(seasonPackMassDelete, 'seasonPackMassDelete');
        }

        function modifySnatchlistPage() {
            function snatchListFilter() {
                // pulled from https://github.com/Userscriptz/BTN-Snatchlist/blob/master/main.user.js

                function changeTable() {
                    const pref = Preferences.getInstance('Snatchlist');
                    const snatchView = pref.getItem('selectedmode', 'all');
                    const links = pref.getItem('dllinks', 'no');

                    if (links == 'yes') {
                        $('.download-btn').show();
                    } else {
                        $('.download-btn').hide();
                    }

                    $('#SnatchData').find('table:nth-child(9) > tbody tr').each(function () {
                        if ($(this).attr('class') != 'colhead_dark') {
                            const id = '#' + $(this).attr('id');
                            const status = $(id + ' > td:nth-child(7) > span').text();
                            const seedTimeLeft = $(id + ' > td:nth-child(5)').text();
                            let remove;

                            if (snatchView == 'showcomplete') {
                                remove = (seedTimeLeft != 'Complete' || status != 'Seeding');
                            }
                            else if (snatchView == 'showcomplete2') {
                                remove = (seedTimeLeft != 'Complete' || status != 'Complete');
                            }
                            else if (snatchView == 'seedtimeleft') {
                                remove = (seedTimeLeft == 'Complete' || status == 'Complete');
                            }
                            else if (snatchView == 'inactive') {
                                remove = (status != 'Inactive');
                            }
                            else if (snatchView == 'leeching') {
                                remove = (status != 'Leeching');
                            }
                            else {
                                remove = false;
                            }

                            if (remove) {
                                $(id).hide();
                            } else {
                                $(id).show();
                            }
                        }
                    });
                }

                const pref = Preferences.getInstance('Snatchlist');
                const selectedMode = pref.getItem('selectedmode', 'all');
                const dllinks = pref.getItem('dllinks', 'no');

                let cssStyle = '.mode-active {color:limegreen; font-weight:bold;} ';
                cssStyle += '#div-1 {position:relative; padding-top:40px;} ';
                cssStyle += '#div-1a {position:absolute; top:0; right:0; width:170px;} ';
                cssStyle += '#div-1b {position:absolute; top:0; left:0; width:600px;} ';
                cssStyle += '.mode-hidden {display: none} ';
                $("<style>")
                    .prop("type", "text/css")
                    .html(cssStyle)
                    .appendTo("head");

                const selectModeLink = function (text, key, mode, active) {
                    return $('<a>', {
                        text:  text,
                        href:  '#',
                        click: function (e) {
                            pref.setItem(key, mode);
                            $('a').filter('.mode-active.' + key).removeClass('mode-active');
                            $(this).addClass('mode-active');
                            changeTable();
                            e.preventDefault();
                        },
                        class: (active ? 'mode-active ' : '') + key
                    });
                };

                const div1b = $('<div>', {
                    id:   'div-1b',
                    text: 'Torrents: '
                });

                const div1a = $('<div>', {
                    id:   'div-1a',
                    text: 'Downloads: '
                });

                div1b.append(selectModeLink('All', 'selectedmode', 'all', selectedMode == 'all'), ' | ',
                    selectModeLink('Inactive', 'selectedmode', 'inactive', selectedMode == 'inactive'), ' | ',
                    selectModeLink('Leeching', 'selectedmode', 'leeching', selectedMode == 'leeching'), ' | ',
                    selectModeLink('Seed Time Left', 'selectedmode', 'seedtimeleft', selectedMode == 'seedtimeleft'), ' | ',
                    selectModeLink('Complete (Seeding)', 'selectedmode', 'showcomplete', selectedMode == 'showcomplete'), ' | ',
                    selectModeLink('Complete', 'selectedmode', 'showcomplete2', selectedMode == 'showcomplete2'));

                div1a.append(selectModeLink('Enable', 'dllinks', 'yes', dllinks == 'yes'), ' | ',
                    selectModeLink('Disable', 'dllinks', 'no', dllinks == 'no'));

                const div1 = $('<div>', {
                    id: 'div-1'
                });
                div1.append(div1b, div1a);
                div1.prependTo('#content');

                const modifySnatchlistElements = function () {
                    let authkey = pref.getItem('authkey');
                    let torrent_pass = pref.getItem('torrent_pass');
                    if (dllinks == 'yes' && (!authkey || !torrent_pass)) {
                        alertify.error('<div><h3>Need torrent passkey</h3><p>To generate torrent download links, your passkey is needed. It can be entered in the Options menu.</p><p>Optionally, you can also disable torrent download links to hide this notice.</p></div>');
                    } else {
                        $('#SnatchData').find('table:nth-child(9) > tbody tr').each(function () {
                            if ($(this).attr('class') != 'colhead_dark') {
                                const id = '#' + $(this).attr('id');
                                const torrentid = $(id + ' > td:nth-child(1) > span > a:nth-child(2)')
                                    .attr('href')
                                    .match(/torrentid=([0-9]+)/)[1];
                                const link = $('<a>', {
                                    href:  'http://broadcasthe.net/torrents.php?action=download&id=' + torrentid + '&authkey=' + authkey + '&torrent_pass=' + torrent_pass,
                                    class: 'download-btn'
                                });
                                $('<img>', {
                                    src: 'https://cdn.broadcasthe.net/common/browsetags/download.png'
                                }).appendTo(link);
                                link.appendTo(id + ' > td:nth-child(1) > span');
                            }
                        });
                    }
                    changeTable();
                };

                window.SortSnatch = exportFunction(function (sort, uid, page) {
                    let old = '';
                    let hnr = '';
                    if (window.location.href.search("old") !== -1) {
                        old = '&old=1';
                    }
                    if (window.location.href.search("hnr") !== -1) {
                        hnr = '&hnr=1';
                    }
                    console.log('Fetching page ' + page);
                    Promise.resolve($.get("https://broadcasthe.net/snatchlist.php?type=ajax&id=" + uid + "&sort=" + sort + "&page=" + page + old + hnr))
                        .then(function (data) {
                            data = $(data)[0];
                            console.log('Fetched page ' + page);
                            $('#SnatchData').replaceWith(data);
                            modifySnatchlistElements();
                        });
                    return false;
                }, window);

                modifySnatchlistElements();
            }

            snatchListFilter();
        }

        function modifyTorrentListPage() {

            function getWatchedSeries() {
                const postUrl = 'https://broadcasthe.net/forums.php?action=get_post&post=1095026';
                return Promise.resolve($.get(postUrl))
                    .then(function (data) {
                        return data.split('\n').slice(1).map(function (value) {
                            value = value.trim();
                            const ret = {};
                            const matched = value.match(/\[url=https:\/\/broadcasthe.net\/series.php\?id=([0-9]+)](.*?)\[\/url](.*)/);
                            if (matched) {
                                ret.url = 'https://broadcasthe.net/series.php?id=' + matched[1];
                                ret.name = matched[2];
                                if (matched[3].length > 0) {
                                    ret.reason = matched[3].replace(' - ', '');
                                } else {
                                    ret.reason = 'No reason specified';
                                }
                            } else {
                                ret.url = '';
                                if (value.indexOf(' - ') > -1) {
                                    const splits = value.split(' - ', 2);
                                    ret.name = splits[0];
                                    ret.reason = splits[1];
                                } else {
                                    ret.name = value;
                                    ret.reason = 'No reason specified';
                                }
                            }
                            return ret;
                        });
                    });
            }

            function getWatchedUsers() {
                const postUrl = 'https://broadcasthe.net/forums.php?action=get_post&post=1095024';
                return Promise.resolve($.get(postUrl))
                    .then(function (data) {
                        return data.split('\n').slice(1).map(function (value) {
                            value = value.trim();
                            const ret = {};
                            if (value.indexOf(' - ') > -1) {
                                const splits = value.split(' - ', 2);
                                ret.name = splits[0];
                                ret.reason = splits[1];
                            } else {
                                ret.name = value;
                                ret.reason = 'No reason specified';
                            }
                            return ret;
                        });
                    });
            }

            run(function () {
                $('#torrent_table').find('tbody > tr > td:nth-child(3)').slice(1).each(function () {
                    const $this = $(this);

                    // cleanup some crap
                    $this.find('br').remove();
                    $this.children('img').remove();
                    let contents = $this.contents();
                    if ($this.children('a')[0].title == 'View Series') {
                        contents.slice(3, 6).wrapAll('<div class="nobr">');
                    } else {
                        $this.children('a').slice(0, 1).wrapAll('<div class="nobr">');
                    }

                    const torrentId = $this.find('a[title="View Torrent"]')
                        .attr('href')
                        .match(/torrents\.php\?id=[0-9]+&torrentid=([0-9]+)/)[1];

                    contents = $this.contents();

                    // get rid of <strong> tag wrapping "Internal" if it exists
                    const torrentSpecsNode = contents.slice(5, 6);
                    let nextNode = torrentSpecsNode[0].nextSibling;
                    let torrentSpecsText = torrentSpecsNode[0].nodeValue;
                    while (nextNode.localName != 'div') {
                        torrentSpecsText += nextNode.innerHTML || nextNode.nodeValue;
                        nextNode = nextNode.nextSibling;
                        $(nextNode.previousSibling).remove();
                    }

                    // color code format, codec, source, resolution, origin, year
                    const torrentSpecs = torrentSpecsText.match(/\[([A-Za-z0-9.\-]+) \/ ([A-Za-z0-9.\-]+) \/ ([A-Za-z0-9.\-]+) \/ ([A-Za-z0-9.\-]+) \/ ([A-Za-z0-9.\-]+)]\s*\[([0-9]+)]/);
                    let newTorrentSpecsNode = '<div class="nobr">[';
                    for (let i = 1; i < 5; i++) {
                        newTorrentSpecsNode += '<div class="inline ' + torrentSpecs[i] + '">' + torrentSpecs[i];
                        newTorrentSpecsNode += '</div> / ';
                    }
                    newTorrentSpecsNode += '<div class="inline ' + torrentSpecs[5] + '">' + torrentSpecs[5];
                    newTorrentSpecsNode += '</div>] [<div class="inline year">' + torrentSpecs[6] + '</div>] [<a href="https://broadcasthe.net/torrents.php?action=edit&id=' + torrentId + '">Edit</a>]</div>';
                    torrentSpecsNode.replaceWith(newTorrentSpecsNode);
                });
            }, 'colorCodeTorrents');
            run(function () {
                // Disabled temporarily
                const tableSelector = $('#torrent_table');
                if (tableSelector.width() > 1000 && false) {
                    tableSelector.find('tbody > tr > td:nth-child(3) > div > span').each(function () {
                        const that = $(this);
                        that.text(' ' + that.attr('title'));
                    });
                } else {
                    tableSelector.find('tbody > tr > td:nth-child(3) > div > span').mouseover(function () {
                        const that = $(this);
                        const newTitle = that.text();
                        that.text(' ' + that.attr('title'));
                        that.attr('title', newTitle);
                    }).mouseout(function () {
                        const that = $(this);
                        const newTitle = that.text();
                        that.text(' ' + that.attr('title'));
                        that.attr('title', newTitle);
                    });
                }
            }, 'expandReleaseNames');
            const p1 = run(function () {
                getWatchedSeries().then(function (seriesList) {
                    const seriesLinks = $('#torrent_table')
                        .find('tbody > tr > td:nth-child(3) > div:nth-child(2) > a:nth-child(1)');
                    seriesList.forEach(function (series) {
                        seriesLinks.filter(function () {
                            const that = $(this);
                            return that.attr('href') === series.url || that.text() === series.name;
                        }).after('<div class="watched inline" title="' + series.reason + '"> [!]</div>');
                    });
                });
            }, 'getWatchedSeries');
            const p2 = run(function () {
                getWatchedUsers().then(function (userList) {
                    const userLinks = $('#torrent_table')
                        .find('tbody > tr > td:nth-child(3) > div:nth-child(4) > a:nth-child(2)');
                    userList.forEach(function (user) {
                        userLinks.filter(function () {
                            return $(this).text() === user.name;
                        }).after('<div class="watched inline" title="' + user.reason + '"> [!]</div>');
                    });
                });
            }, 'getWatchedUsers');
            Promise.all([p1, p2]).then(function () {
                $('.watched').mouseover(function () {
                    const that = $(this);
                    if (that.attr('title').length < 50) {
                        that.text(' ' + that.attr('title'));
                    }
                }).mouseout(function () {
                    $(this).text(' [!]');
                });
            });
        }

        function modifyReportsPage() {

            function refreshReportsPage() {
                return Promise.resolve($.get(window.location.href)).then(function (data) {
                    const dom = window.jQuery(data);
                    dom.find('span[class=date-utc]').map(function (i, e) {
                        const LocalDate = new Date($(e).attr('x-jqdate') * 1000);
                        $(e).html(LocalDate.toString());
                    });
                    $('#content').replaceWith(dom.find('#content'));
                    window.jQuery('span.expandable')
                        .expander({slicePoint: 35, expandText: '[show]', userCollapseText: '[hide]'});
                    modifyReportsPage();
                    //run(addLinksToReports, 'addLinksToReports');
                })
            }

            function addLinksToReports() {

                function getReport(id) {
                    return Promise.resolve($.post('https://broadcasthe.net/reports.php', {
                        id:     id,
                        action: 'view_report'
                    }));
                }

                // Highlight reports with the current date (UTC) in them
                if (url.indexOf('status=Resolved') == -1) {
                    const date = new Date();
                    $('span.expandable').filter(function () {
                        let matched = this.innerHTML.match(/([0-9]{4}).([0-9]{1,2}).([0-9]{1,2})/);
                        if (!matched) {
                            return false;
                        }
                        const cleanDate = new Date(Date.UTC(parseInt(matched[1]), parseInt(matched[2]) - 1, parseInt(matched[3]), 0, 0, 0, 0));
                        return date > cleanDate;
                    }).each(function (index, value) {
                        const $this = $(value);
                        const row = $this.closest('tr');
                        row.css('background-color', '#500050');
                        const viewLink = row.find('td:nth-child(7) > a');
                        if (viewLink.length === 1) {
                            viewLink.after($('<a>', {
                                href:  '#',
                                class: 'autoresolve cleaned',
                                text:  '[Cleaned]'
                            }));
                            viewLink.after($('<br>'));
                        }
                    });
                    $('#content').find('> table > tbody > tr > td:nth-child(2) > a').filter(function () {
                        return /log\.php\?search=[0-9]+/.test(this.href);
                    }).each(function (index, value) {
                        const $this = $(value);
                        const row = $this.closest('tr');
                        row.css('background-color', '#500050');
                        const viewLink = row.find('td:nth-child(7) > a');
                        if (viewLink.length === 1) {
                            // Add links to those
                            viewLink.after($('<a>', {
                                href:  '#',
                                class: 'autoresolve deleted',
                                text:  '[Deleted]'
                            }));
                            viewLink.after($('<br>'));
                            viewLink.after($('<a>', {
                                href:  '#',
                                class: 'autoresolve cleaned',
                                text:  '[Cleaned]'
                            }));
                            viewLink.after($('<br>'));
                        }
                    });
                }
                $('#content').find('> table > tbody').on('click', '.autoresolve', function (e) {
                    const className = e.target.className;
                    if (className.indexOf('autoresolve') == -1) {
                        return true;
                    }
                    const $this = $(this);
                    const reportId = $this.closest('td').find('a:nth-child(1)').attr('onclick').match(/showReport\(([0-9]+)\)/)[1];
                    getReport(reportId).then(function (data) {
                        const dom = $(data);
                        const formDataPreprocessed = dom.serializeArray();
                        const formData = {};
                        for (let i = 0; i < formDataPreprocessed.length; i++) {
                            const elem = formDataPreprocessed[i];
                            formData[elem['name']] = elem['value'];
                        }
                        let comment = '';
                        if (className.indexOf('cleaned') > -1) {
                            comment = 'Cleaned\n//' + myUsername;
                        } else if (className.indexOf('deleted') > -1) {
                            comment = 'Deleted\n//' + myUsername;
                        }
                        formData['status'] = 'Resolved';
                        formData['comment'] = comment;
                        formData['userMessage'] = comment;
                        if (comment == '') {
                            throw 'Empty report comment.';
                        }
                        return alertify.okBtn('Resolve')
                            .cancelBtn('Cancel')
                            .confirm('Resolve report ' + formData['reportid'] + '?')
                            .then(function (resolvedValue) {
                                if (resolvedValue.buttonClicked == 'ok') {
                                    return Promise.resolve($.post('https://broadcasthe.net/reports.php', formData))
                                        .then(function () {
                                            alertify.success('Resolved report ' + formData['reportid'] + ':\n\t' + comment);
                                            return refreshReportsPage();
                                        });
                                } else {
                                    alertify.error('Did not resolve report ' + formData['reportid'] + '.');
                                }
                            });
                    });
                    return false;
                })
            }

            function disableReportsAutorefresh() {
                // Hook form submit if not Firefox
                if (!isFirefox()) {
                    $(document).ready(function () {
                        (function ($window) {
                            $window('#reports_dialog').dialog('destroy');
                            $window('#reports_dialog').dialog({
                                autoOpen: false,
                                modal:    true,
                                show:     'fade',
                                hide:     'fade',
                                width:    585,
                                height:   400,
                                buttons:  {
                                    Close: function () {
                                        $window('#reports_dialog').dialog('close');
                                    },
                                    Save:  function () {
                                        $window('#reports_dialog').dialog('close');
                                        const formData = $('#repoform').serialize();
                                        $.post('https://broadcasthe.net/reports.php', formData)
                                            .then(function () {
                                                return refreshReportsPage();
                                            });
                                    }
                                }
                            });
                        })(window.jQuery);
                    })
                } else {
                    console.log('Warning: hooking reports.php dialogs does not work in Firefox.');
                }
            }

            run(disableReportsAutorefresh, 'disableReportsAutorefresh');
            run(addLinksToReports, 'addLinksToReports');
        }

        function modifyStaffPMPage() {

            function staffPMBBcodeGenerator() {
                // Add a text field to insert a staff note in PMs
                // This way I don't have to !bbcode2 all the time
                let textbox = $('#quickpost');
                if (!textbox) {
                    return;
                }
                const container = document.createElement('div');
                const notebox = $('<textarea>', {
                    cols:  '70',
                    rows:  '3',
                    style: 'vertical-align:middle;height:auto;width:auto;'
                }).appendTo(container);
                $('<input>', {
                    type:  'button',
                    value: 'Insert staff note',
                    style: 'vertical-align:middle;',
                    click: function () {
                        const oldText = textbox.val();
                        let note = notebox.val();
                        if (!note.endsWith('//' + myUsername)) {
                            note += ' //' + myUsername;
                        }
                        textbox.val(oldText + '[[n]b][[n]color=red][[n]size=5]' + note + '[/size][/color][/b]');
                        notebox.val('');
                    }
                }).appendTo(container);
                textbox.after(container);
                notebox.before(document.createElement('br'));
            }

            function selfAssignStaffPM() {
                const assignToMe = $('<input>', {
                    type:  'button',
                    value: 'Assign to me',
                    click: function () {
                        Promise.resolve($.post('https://broadcasthe.net/staffpm.php?action=assign', {
                            convid: url.match(/id=([0-9]+)/)[1],
                            assign: 'user_' + myUserId
                        })).then(function () {
                            location.reload();
                        });
                        return false;
                    }
                });

                $('#messageform').find('> input[type="button"]:nth-child(7)').after(assignToMe);
            }

            function autoOpenCommonAnswers() {
                $('#common_answers').show();
            }

            run(staffPMBBcodeGenerator, 'staffPMBBcodeGenerator');
            run(selfAssignStaffPM, 'selfAssignStaffPM');
            run(autoOpenCommonAnswers, 'autoOpenCommonAnswers');
        }

        function modifyUserDetailPage() {
            const userId = url.match(/user.php\?id=([0-9]+)/)[1];
			const userName = document.getElementsByClassName("user_profile")[0].children[0].children[0].innerText
			run(function () {
				$('div.leftbar')
					.find('img').remove();
			}, 'HideAvatars');
			run(function () {
				$('div.leftbar')
					.find('li:nth-child(4)')
					.after('<li>[<a href="https://broadcasthe.net/staffpm.php?username=' + userName + '&view=search">Staff PMs</a>]</li>');
            }, 'addStaffPMsLinkToProfile');
            run(function () {
                $('#section2')
                    .find('li:nth-child(14)')
                    .after('<li><a href="https://broadcasthe.net/snatchlist.php?id=' + userId + '">Snatchlist</a></li>');
            }, 'addSnatchlistLink');
            run(function () {
                let stampSelector = $('#section3').find('> div > div.pad.center');
                stampSelector.find('> img, a').remove();
            }, 'blockUserStamps');
            // stampObserver.disconnect();
        }

        function modifyUserProfileOptionsPage() {
            run(function () {
                $('#torpersnatch')
                    .replaceWith('<input name="torpersnatch" id="torpersnatch" type="number" min="10" max="100000" step="10" value="1000" size="6">');
                $('#torperpage')
                    .replaceWith('<input name="torperpage" id="torperpage" type="number" min="25" max="100000" step="5" value="100" size="6">');
            }, 'modifyUserProfileOptionsPage');
        }

		function modifySite() {
			$('#searchbars ul li').css({"margin-top":"5px","margin-right":"4px","margin-bottom":"12px","margin-left":"4px"});
			$('#content').css({"margin":"auto"});
			run(function () {
				$('#searchbars ul li:contains("Torrent")').remove();
			}, 'HideSearchTorrents');
			run(function () {
				$('#searchbars ul li:contains("Series")').remove();
			}, 'HideSearchSeries');
			run(function () {
				$('#searchbars ul li:contains("Actors")').remove();
			}, 'HideSearchActors');
			run(function () {
				$('#searchbars ul li:contains("Requests")').remove();
			}, 'HideSearchRequests');
			run(function () {
				$('#searchbars ul li:contains("Forums")').remove();
			}, 'HideSearchForums');
			run(function () {
				$('#searchbars ul li:contains("Log")').remove();
			}, 'HideSearchLog');
			run(function () {
				$('#searchbars ul li:contains("Users")').remove();
			}, 'HideSearchUsers');
		}
		
        addPolyfillsAndStyles();
        addOptionsToMenu();

        run(checkForUpdates, 'checkForUpdates');
        run(addPMToUsernames, 'addPMToUsernames');

        if (/^user\.php\?action=edit&userid=[0-9]+/.test(url)) {
            modifyUserProfileOptionsPage();
        }
        if (/^torrents.php\?(?:page=[0-9]+&)?(?:(?:id=[0-9]+)|(?:torrentid=[0-9]+))/.test(url)) {
            modifyTorrentDetailPage();
        }
        if (/^staffpm.php\?action=viewconv/.test(url)) {
            modifyStaffPMPage();
        }
        if (/^torrents.php\?action=edit&id=[0-9]+/.test(url)) {
            modifyTorrentEditPage();
        }
        if (/^upload.php/.test(url)) {
            modifyTorrentUploadPage();
        }
        if (/^series.php\?action=edit_info/.test(url)) {
            modifySeriesEditPage();
        }
        if (/^requests.php\?action=edit/.test(url)) {
            modifyRequestPage();
        }
        if (/^series.php\?action=trump&seriesid=[0-9]+&auth=[0-9a-f]+/.test(url)) {
            modifyMassDeletePage();
        }
        if (/^snatchlist.php/.test(url)) {
            modifySnatchlistPage();
        }
        if (/^torrents.php.*(?!id)/.test(url)) {
            modifyTorrentListPage();
        }
        if (/^reports.php/.test(url)) {
            modifyReportsPage();
        }
        if (/^user.php\?id=[0-9]+/.test(url)) {
            modifyUserDetailPage();
        }
        if (/^forums.php/.test(url)) {
            modifyForumPage();
        }
		if (/^/.test(url)) {
			modifySite();
		}
	});
}($));

},{"./MediaInfo.js":1,"./Preferences.js":2,"./TVDBv2.js":3,"./TVMaze.js":4,"./Utils.js":5,"./latinise.js":6,"bluebird":7}],9:[function(require,module,exports){
// shim for using process in browser

var process = module.exports = {};
var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
    if (!draining || !currentQueue) {
        return;
    }
    draining = false;
    if (currentQueue.length) {
        queue = currentQueue.concat(queue);
    } else {
        queueIndex = -1;
    }
    if (queue.length) {
        drainQueue();
    }
}

function drainQueue() {
    if (draining) {
        return;
    }
    var timeout = setTimeout(cleanUpNextTick);
    draining = true;

    var len = queue.length;
    while(len) {
        currentQueue = queue;
        queue = [];
        while (++queueIndex < len) {
            if (currentQueue) {
                currentQueue[queueIndex].run();
            }
        }
        queueIndex = -1;
        len = queue.length;
    }
    currentQueue = null;
    draining = false;
    clearTimeout(timeout);
}

process.nextTick = function (fun) {
    var args = new Array(arguments.length - 1);
    if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
        }
    }
    queue.push(new Item(fun, args));
    if (queue.length === 1 && !draining) {
        setTimeout(drainQueue, 0);
    }
};

// v8 likes predictible objects
function Item(fun, array) {
    this.fun = fun;
    this.array = array;
}
Item.prototype.run = function () {
    this.fun.apply(null, this.array);
};
process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function() { return 0; };

},{}]},{},[8]);
